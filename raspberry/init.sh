#!/sbin/openrc-run

command="/usr/bin/go4matic"
pidfile="/var/run/go4matic.pid"
command_args="--raspberry --port 80 --config /data/go4matic/config.yml --log_config /data/go4matic/log_config.yml"
command_background=true


depend() {
	use logger dns
	need net
	after firewall
}
