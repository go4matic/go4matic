#!/bin/sh

docker run --rm -v "$PWD"/../:/work -w /work -e GOOS=linux -e GOARCH=arm -e GOARM=5 golang:1.13-alpine go build -v -o ./out/go4matic_linux_arm -tags 'prod' ./cmd/go4matic

docker run --rm -it -v "$PWD"/../:/input -v $PWD:/output -e UBOOT_COUNTER_RESET_ENABLED=false bboehmke/raspi-alpine-builder