// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestField_TypeName(t *testing.T) {
	ass := assert.New(t)

	field := Field{
		Type: Int,
	}
	ass.Equal(Int.TypeName(), field.TypeName())

	field = Field{}
	ass.Equal(Any.TypeName(), field.TypeName())
}

func TestField_MarshalJSON(t *testing.T) {
	ass := assert.New(t)

	field := Field{
		Type:         Int,
		Required:     true,
		Description:  "test",
		DefaultValue: 42,
	}

	bytes, err := field.MarshalJSON()
	ass.NoError(err)
	ass.Equal("{\"type\":\"int\",\"base_type\":\"integer\",\"description\":\"test\",\"required\":true,\"default_value\":42}", string(bytes))

	field = Field{
		Required:     true,
		Description:  "test",
		DefaultValue: 42,
	}

	bytes, err = field.MarshalJSON()
	ass.NoError(err)
	ass.Equal("{\"type\":\"*\",\"base_type\":\"AnyValue\",\"description\":\"test\",\"required\":true,\"default_value\":42}", string(bytes))
}

func TestFunction_ValidateArguments(t *testing.T) {
	ass := assert.New(t)

	funcDef := Function{
		Arguments: FieldMap{
			"aaa": {
				Type: Int,
			},
		},
	}

	data := []interface{}{42}

	dataOut, err := funcDef.ValidateArguments(data)
	ass.NoError(err)
	ass.Equal(data, dataOut)

	funcDef2 := Function{}
	dataOut, err = funcDef2.ValidateArguments(data)
	ass.NoError(err)
	ass.Equal(data, dataOut)

	ass.True(funcDef2.Check(func([]interface{}) []interface{} { return nil }))
	ass.False(funcDef2.Check("test"))
	ass.Equal("func", funcDef.TypeName())
	ass.Equal("Function", funcDef.BaseName())
}

func TestSimpleHandler_ValidateParameters(t *testing.T) {
	ass := assert.New(t)

	funcDef := SimpleHandler{
		Parameters: FieldMap{
			"test": {
				Type: Int,
			},
		},
	}

	data := map[string]interface{}{
		"test": 42,
	}

	dataOut, err := funcDef.ValidateParameters(data)
	ass.NoError(err)
	ass.Equal(data, dataOut)

	funcDef2 := SimpleHandler{}
	dataOut, err = funcDef2.ValidateParameters(data)
	ass.NoError(err)
	ass.Equal(data, dataOut)
}

func TestExtendedHandler_ValidateParameters(t *testing.T) {
	ass := assert.New(t)

	funcDef := ExtendedHandler{
		Parameters: FieldMap{
			"test": {
				Type: Int,
			},
		},
	}

	data := map[string]interface{}{
		"test": 42,
	}

	dataOut, err := funcDef.ValidateParameters(data)
	ass.NoError(err)
	ass.Equal(data, dataOut)

	funcDef2 := ExtendedHandler{}
	dataOut, err = funcDef2.ValidateParameters(data)
	ass.NoError(err)
	ass.Equal(data, dataOut)
}
