// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFieldMap(t *testing.T) {
	ass := assert.New(t)

	m := FieldMap{
		"aaa": {
			Type:         Int,
			Required:     true,
			DefaultValue: 42,
			Order:        2,
		},
		"bbb": {
			Type:  String,
			Order: 1,
		},
		"ccc": {
			Order: 1,
		},
	}
	ass.Equal("FieldMap[3]", m.TypeName())
	ass.Equal("object", m.BaseName())
	ass.Equal([]string{"bbb", "ccc", "aaa"}, m.SortedKeys())

	ass.True(m.Check(map[string]interface{}{
		"aaa": 42,
		"bbb": "test",
	}))
	ass.True(m.Check(map[string]interface{}{
		"aaa": 42,
	}))

	ass.False(m.Check(map[string]interface{}{
		"aaa": 42,
		"bbb": "test",
		"ddd": "test",
	}))
	ass.False(m.Check(map[string]interface{}{
		"bbb": "test",
	}))
	ass.False(m.Check(map[string]interface{}{}))

	ass.Equal(map[string]interface{}{
		"aaa": 42,
		"bbb": "test",
		"ccc": nil,
	}, m.Load(map[string]interface{}{
		"bbb": "test",
	}))
}

// test data for TestCheckFields
var checkFieldsDataSpecial = []struct {
	name    string
	fields  FieldMap
	dataIn  map[string]interface{}
	dataOut map[string]interface{}
	error   string
}{
	{
		"empty_fields_and_data",
		FieldMap{},
		map[string]interface{}{},
		map[string]interface{}{},
		"",
	},
	{
		"nil_data",
		FieldMap{},
		nil,
		map[string]interface{}{},
		"",
	},
	{
		"nil_fields",
		nil,
		nil,
		map[string]interface{}{},
		"",
	},
	{
		"required_field_missing",
		FieldMap{
			"test": {
				Required: true,
			},
		},
		map[string]interface{}{},
		nil,
		"required field \"test\" missing",
	},
	{
		"default_value_field",
		FieldMap{
			"test": {
				DefaultValue: 42,
			},
		},
		map[string]interface{}{},
		map[string]interface{}{
			"test": 42,
		},
		"",
	},
	{
		"default_value_field2",
		FieldMap{
			"test": {
				DefaultValue: 42,
			},
		},
		map[string]interface{}{
			"test": nil,
		},
		map[string]interface{}{
			"test": 42,
		},
		"",
	},
	{
		"unknown_field",
		FieldMap{},
		map[string]interface{}{
			"test": 42,
		},
		nil,
		"unknown field \"test\" given",
	},
	{
		"wildcard_field",
		FieldMap{
			"{name}": {},
		},
		map[string]interface{}{
			"test": 42,
		},
		map[string]interface{}{
			"test": 42,
		},
		"",
	},
	{
		"invalid_field_type",
		FieldMap{
			"test": {
				Type: Int,
			},
		},
		map[string]interface{}{
			"test": "aaa",
		},
		nil,
		"invalid value type for field \"test\"",
	},
}

// TestValidateFields checks validateFields function
func TestFieldMapValidate(t *testing.T) {
	// special cases
	for _, data := range checkFieldsDataSpecial {
		t.Run(data.name, func(st *testing.T) {
			ass := assert.New(st)

			dataOut, err := data.fields.Validate(data.dataIn)

			if data.error != "" {
				if ass.Error(err) {
					ass.Equal(data.error, err.Error())
				}
			} else {
				ass.NoError(err)
			}

			ass.Equal(data.dataOut, dataOut)
		})
	}
}

func TestFieldList(t *testing.T) {
	ass := assert.New(t)

	m := FieldList{
		{
			Type:     Int,
			Required: true,
		}, {
			Type: String,
		},
	}
	ass.Equal("FieldList[2]", m.TypeName())
	ass.Equal("array", m.BaseName())

	ass.True(m.Check([]interface{}{
		42, "test",
	}))
	ass.True(m.Check([]interface{}{
		42,
	}))

	ass.False(m.Check([]interface{}{
		42, "test", "test",
	}))
	ass.False(m.Check([]interface{}{
		"test",
	}))
	ass.False(m.Check([]interface{}{}))
}

// test data for TestFieldListValidate
var checkFieldListDataSpecial = []struct {
	name    string
	fields  FieldList
	dataIn  []interface{}
	dataOut []interface{}
	error   string
}{
	{
		"empty_fields_and_data",
		FieldList{},
		[]interface{}{},
		[]interface{}{},
		"",
	},
	{
		"nil_data",
		FieldList{},
		nil,
		[]interface{}{},
		"",
	},
	{
		"nil_fields",
		nil,
		nil,
		[]interface{}{},
		"",
	},
	{
		"required_field_missing",
		FieldList{
			{
				Required: true,
			},
		},
		[]interface{}{},
		nil,
		"required field 0 missing",
	},
	{
		"default_value_field",
		FieldList{
			{
				DefaultValue: 42,
			},
		},
		[]interface{}{},
		[]interface{}{
			42,
		},
		"",
	},
	{
		"unknown_field",
		FieldList{},
		[]interface{}{
			42,
		},
		nil,
		"expected 0 fields but get 1",
	},
	{
		"invalid_field_type",
		FieldList{
			{
				Type: Int,
			},
		},
		[]interface{}{
			"aaa",
		},
		nil,
		"invalid value type for field 0",
	},
}

// TestFieldListValidate checks validateFields function
func TestFieldListValidate(t *testing.T) {
	// special cases
	for _, data := range checkFieldListDataSpecial {
		t.Run(data.name, func(st *testing.T) {
			ass := assert.New(st)

			dataOut, err := data.fields.Validate(data.dataIn)

			if data.error != "" {
				if ass.Error(err) {
					ass.Equal(data.error, err.Error())
				}
			} else {
				ass.NoError(err)
			}

			ass.Equal(data.dataOut, dataOut)
		})
	}
}
