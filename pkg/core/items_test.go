// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package core

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/go4matic/go4matic/pkg/api"
)

type testValuesItem struct {
	*testItem
}

func (i *testValuesItem) GetValueTypes() api.FieldMap {
	return nil
}

type testSensorItem struct {
	*testValuesItem
}

func (i *testSensorItem) GetValues() map[string]interface{} {
	return nil
}

type testSensorItemChecker struct {
	*testSensorItem

	checker bool
}

func (i *testSensorItemChecker) IsSensorItem() bool {
	return i.checker
}

func TestGetSensorItem(t *testing.T) {
	ass := assert.New(t)

	sensor, ok := GetSensorItem(new(testValuesItem))
	ass.False(ok)
	ass.Nil(sensor)

	item := new(testSensorItem)
	sensor, ok = GetSensorItem(item)
	ass.True(ok)
	ass.Equal(item, sensor)

	item2 := new(testSensorItemChecker)
	sensor, ok = GetSensorItem(item2)
	ass.False(ok)
	ass.Nil(sensor)

	item2.checker = true
	sensor, ok = GetSensorItem(item2)
	ass.True(ok)
	ass.Equal(item2, sensor)
}

type testActorItem struct {
	*testValuesItem
}

func (i *testActorItem) SetValues(values map[string]interface{}) error {
	return nil
}

type testActorItemChecker struct {
	*testActorItem

	checker bool
}

func (i *testActorItemChecker) IsActorItem() bool {
	return i.checker
}

func TestGetActorItem(t *testing.T) {
	ass := assert.New(t)

	actor, ok := GetActorItem(new(testValuesItem))
	ass.False(ok)
	ass.Nil(actor)

	item := new(testActorItem)
	actor, ok = GetActorItem(item)
	ass.True(ok)
	ass.Equal(item, actor)

	item2 := new(testActorItemChecker)
	actor, ok = GetActorItem(item2)
	ass.False(ok)
	ass.Nil(actor)

	item2.checker = true
	actor, ok = GetActorItem(item2)
	ass.True(ok)
	ass.Equal(item2, actor)
}

type testEventItem struct {
	*testValuesItem
}

func (i *testEventItem) Events() map[string]api.SimpleHandler {
	return nil
}

func (i *testEventItem) SetEventTrigger(trigger func(action string, data map[string]interface{})) {

}

type testEventItemChecker struct {
	*testEventItem

	checker bool
}

func (i *testEventItemChecker) IsEventItem() bool {
	return i.checker
}

func TestGetEventItem(t *testing.T) {
	ass := assert.New(t)

	event, ok := GetEventItem(new(testValuesItem))
	ass.False(ok)
	ass.Nil(event)

	item := new(testEventItem)
	event, ok = GetEventItem(item)
	ass.True(ok)
	ass.Equal(item, event)

	item2 := new(testEventItemChecker)
	event, ok = GetEventItem(item2)
	ass.False(ok)
	ass.Nil(event)

	item2.checker = true
	event, ok = GetEventItem(item2)
	ass.True(ok)
	ass.Equal(item2, event)
}

type testLoggerItem struct {
	*testValuesItem
}

func (i *testLoggerItem) LogValues(id string, values map[string]interface{}) error {
	return nil
}

type testLoggerItemChecker struct {
	*testLoggerItem

	checker bool
}

func (i *testLoggerItemChecker) IsLoggerItem() bool {
	return i.checker
}

func TestGetLoggerItem(t *testing.T) {
	ass := assert.New(t)

	logger, ok := GetLoggerItem(new(testValuesItem))
	ass.False(ok)
	ass.Nil(logger)

	item := new(testLoggerItem)
	logger, ok = GetLoggerItem(item)
	ass.True(ok)
	ass.Equal(item, logger)

	item2 := new(testLoggerItemChecker)
	logger, ok = GetLoggerItem(item2)
	ass.False(ok)
	ass.Nil(logger)

	item2.checker = true
	logger, ok = GetLoggerItem(item2)
	ass.True(ok)
	ass.Equal(item2, logger)
}

func TestItemValue(t *testing.T) {
	ass := assert.New(t)

	v1 := ItemValue{
		ID:    "aaa",
		Value: "bbb",
	}
	v2, err := NewItemValue(v1)
	ass.NoError(err)
	ass.Equal(v1, v2)

	_, err = NewItemValue("aaa")
	ass.EqualError(err, "invalid item value should be [ITEM_ID]#[VALUE]")

	v3, err := NewItemValue("aaa#bbb")
	ass.NoError(err)
	ass.Equal(v1, v3)

	ass.Equal("aaa#bbb", v1.String())
}

func TestItemValueType(t *testing.T) {
	ass := assert.New(t)

	ass.Equal("ItemValue", ItemValueType.TypeName())
	ass.Equal("string", ItemValueType.BaseName())
	ass.Equal("", ItemValueType.SubType())

	ass.Equal("ItemValue", ActorItemValueType.TypeName())
	ass.Equal("string", ActorItemValueType.BaseName())
	ass.Equal("actor", ActorItemValueType.SubType())

	ass.Equal("ItemValue", SensorItemValueType.TypeName())
	ass.Equal("string", SensorItemValueType.BaseName())
	ass.Equal("sensor", SensorItemValueType.SubType())

	ass.False(ItemValueType.Check("aaa"))
	ass.True(ItemValueType.Check("aaa#bbb"))
	ass.True(ItemValueType.Check(ItemValue{
		ID:    "aaa",
		Value: "bbb",
	}))
}

func TestActorItemValues(t *testing.T) {
	ass := assert.New(t)

	bus := &TestBus{
		HandleRequestWaitFunc: func(request Request, duration time.Duration) Response {
			return Response{
				Data: map[string]interface{}{
					"aaa": map[string]interface{}{
						"name":     "AAA",
						"is_actor": true,
						"values": api.FieldMap{
							"111": {
								Type:        api.Bool,
								Description: "-1-",
							},
							"222": {
								Type:        api.String,
								Description: "-2-",
							},
						},
					},
					"bbb": map[string]interface{}{
						"name":     "BBB",
						"is_actor": false,
						"values": api.FieldMap{
							"111": {
								Type:        api.Int,
								Description: "-1-",
							},
						},
					},
				},
			}
		},
	}

	ass.Equal(map[interface{}]string{
		"aaa#111": "AAA - 111",
		"aaa#222": "AAA - 222",
	}, ActorItemValues(bus))

	ass.Equal(map[interface{}]string{
		"aaa#111": "AAA - 111",
	}, ActorItemValues(bus, api.Bool))
}

func TestSensorItemValues(t *testing.T) {
	ass := assert.New(t)

	bus := &TestBus{
		HandleRequestWaitFunc: func(request Request, duration time.Duration) Response {
			return Response{
				Data: map[string]interface{}{
					"aaa": map[string]interface{}{
						"name":      "AAA",
						"is_sensor": true,
						"values": api.FieldMap{
							"111": {
								Type:        api.Bool,
								Description: "-1-",
							},
							"222": {
								Type:        api.String,
								Description: "-2-",
							},
						},
					},
					"bbb": map[string]interface{}{
						"name":      "BBB",
						"is_sensor": false,
						"values": api.FieldMap{
							"111": {
								Type:        api.Int,
								Description: "-1-",
							},
						},
					},
				},
			}
		},
	}

	ass.Equal(map[interface{}]string{
		"aaa#111": "AAA - 111",
		"aaa#222": "AAA - 222",
	}, SensorItemValues(bus))

	ass.Equal(map[interface{}]string{
		"aaa#111": "AAA - 111",
	}, SensorItemValues(bus, api.Bool))
}
