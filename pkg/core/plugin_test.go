// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package core

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/go4matic/go4matic/pkg/api"
)

func TestPluginConfig_Dump(t *testing.T) {
	ass := assert.New(t)

	config := PluginConfig{
		Config: map[string]interface{}{
			"aaa": 42,
		},
		Entries: map[string]map[string]map[string]interface{}{
			"bbb": {
				"ccc": {
					"ddd": 1,
				},
			},
		},
	}

	ass.Equal(map[string]interface{}{
		"aaa": 42,
		"bbb": map[string]map[string]interface{}{
			"ccc": {
				"ddd": 1,
			},
		},
	}, config.Dump())
}

func TestPluginConfigDef_Validate(t *testing.T) {
	ass := assert.New(t)

	def := PluginConfigDef{
		Config: api.FieldMap{
			"aaa": {},
		},
	}

	ass.EqualError(def.Validate(), "missing LoadFunc")
	def.LoadFunc = func(map[string]interface{}) error { return nil }
	ass.EqualError(def.Validate(), "missing ChangeFunc")
	def.ChangeFunc = func(map[string]interface{}) error { return nil }
	ass.NoError(def.Validate())

	def.Entries = PluginConfigEntryDefMap{
		"aaa": {
			Config: api.FieldMap{
				"aaa": {},
			},
		},
	}
	ass.EqualError(def.Validate(), "missing LoadFunc in entry \"aaa\"")
}
func TestPluginConfigDef_Load(t *testing.T) {
	ass := assert.New(t)

	def := PluginConfigDef{
		Config: api.FieldMap{
			"value_a": {},
			"value_b": {
				DefaultValue: 42,
			},
		},
		LoadFunc: func(config map[string]interface{}) error {
			ass.Equal(map[string]interface{}{
				"value_a": "test",
				"value_b": 42,
			}, config)
			return errors.New("test")
		},
	}

	data := map[string]interface{}{
		"value_a": "test",
		"value_b": 42,
	}

	_, err := def.Load(data)
	ass.EqualError(err, "test")

	def.LoadFunc = func(config map[string]interface{}) error {
		ass.Equal(map[string]interface{}{
			"value_a": "test",
			"value_b": 42,
		}, config)
		return nil
	}

	config, err := def.Load(data)
	ass.NoError(err)

	ass.Equal(map[string]interface{}{
		"value_a": "test",
		"value_b": 42,
	}, config.Config)

	def2 := PluginConfigDef{
		Entries: PluginConfigEntryDefMap{
			"entries": {
				Config: api.FieldMap{
					"value_a": {},
				},
				LoadEntryFunc: func(entries map[string]map[string]interface{}) error {
					ass.Equal(map[string]map[string]interface{}{
						"key": {
							"value_a": "test",
						},
					}, entries)
					return errors.New("test")
				},
			},
		},
	}
	data2 := map[string]interface{}{
		"entries": map[string]interface{}{
			"key": map[string]interface{}{
				"value_a": "test",
			},
		},
	}

	_, err = def2.Load(data2)
	ass.EqualError(err, "test")

	def3 := PluginConfigDef{
		Entries: PluginConfigEntryDefMap{
			"entries": {
				Config: api.FieldMap{
					"value_a": {},
				},
				LoadEntryFunc: func(entries map[string]map[string]interface{}) error {
					ass.Equal(map[string]map[string]interface{}{
						"key": {
							"value_a": "test",
						},
					}, entries)
					return nil
				},
			},
		},
	}
	config2, err := def3.Load(data2)
	ass.NoError(err)

	ass.Contains(config2.Entries, "entries")
	ass.Contains(config2.Entries["entries"], "key")
	ass.Equal(map[string]interface{}{
		"value_a": "test",
	}, config2.Entries["entries"]["key"])
}

func TestPluginConfigEntryDef_Validate(t *testing.T) {
	ass := assert.New(t)

	def := PluginConfigEntryDef{
		Config: api.FieldMap{
			"aaa": {},
		},
	}
	ass.EqualError(def.Validate(), "missing LoadFunc")
	def.LoadEntryFunc = func(map[string]map[string]interface{}) error { return nil }
	ass.EqualError(def.Validate(), "missing AddEntryFunc")
	def.AddEntryFunc = func(string, map[string]interface{}) error { return nil }
	ass.EqualError(def.Validate(), "missing RemoveEntryFunc")
	def.RemoveEntryFunc = func(string) error { return nil }
	ass.NoError(def.Validate())
}

func TestPluginConfigEntryDef_Load(t *testing.T) {
	ass := assert.New(t)

	def := PluginConfigEntryDef{
		Config: api.FieldMap{
			"value_a": {},
			"value_b": {
				DefaultValue: 42,
			},
		},
		LoadEntryFunc: func(entries map[string]map[string]interface{}) error {
			ass.Equal(map[string]map[string]interface{}{
				"key": {
					"value_a": "test",
					"value_b": 42,
				},
			}, entries)
			return errors.New("test")
		},
	}

	data := map[string]interface{}{
		"key": map[string]interface{}{
			"value_a": "test",
			"value_b": 42,
		},
	}

	_, err := def.Load(data)
	ass.EqualError(err, "test")

	def.LoadEntryFunc = func(entries map[string]map[string]interface{}) error {
		ass.Equal(map[string]map[string]interface{}{
			"key": {
				"value_a": "test",
				"value_b": 42,
			},
		}, entries)
		return nil
	}

	config, err := def.Load(data)
	ass.NoError(err)

	ass.Contains(config, "key")
	ass.Equal(map[string]interface{}{
		"value_a": "test",
		"value_b": 42,
	}, config["key"])
}

func TestPluginConfigEntryDefMap_SortedKeys(t *testing.T) {
	ass := assert.New(t)

	def := PluginConfigEntryDefMap{
		"aaa": {
			Order: 2,
		},
		"bbb": {
			Order: 1,
		},
		"ccc": {
			Order: 1,
		},
	}
	ass.Equal([]string{"bbb", "ccc", "aaa"}, def.SortedKeys())
}
