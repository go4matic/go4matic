// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package core

import (
	"gitlab.com/go4matic/go4matic/pkg/api"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSimplePathEntry_PathEntry(t *testing.T) {
	ass := assert.New(t)

	def := api.SimpleHandler{
		Description: "test",
		Response: map[string]api.Field{
			"value": {
				Description: "aaa",
			},
		},
		Parameters: map[string]api.Field{
			"param": {
				Description: "bbb",
			},
		},
	}

	entry := SimplePathEntry{
		Def: def,

		Path:   "/aaa/bbb",
		Method: "POST",
	}.PathEntry()

	ass.Equal(def.Description, entry.Def.Description)
	ass.Equal(def.Parameters, entry.Def.Parameters)
	ass.Equal(def.Response, entry.Def.Responses["200"].Data)
	ass.Equal(api.InvalidRequestResponse, entry.Def.Responses["400"])
	ass.Equal("/aaa/bbb", entry.Path)
	ass.Equal("POST", entry.Method)
	ass.NotNil(entry.Handler)
}
