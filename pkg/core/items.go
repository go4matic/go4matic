// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package core

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/spf13/cast"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
)

// Item that can be handled by the item service
type Item interface {
	// Identifier of item
	ID() string

	// User friendly name of item
	Name() string
}

// ValuesItem is used by all items that can read or write values
type ValuesItem interface {
	Item

	// Get possible values type
	GetValueTypes() api.FieldMap
}

// SensorItem provides the functionality to read values
type SensorItem interface {
	ValuesItem

	// GetValues of sensor
	GetValues() map[string]interface{}
}

// SensorItemChecker provides interface to check if sensor item
type SensorItemChecker interface {
	// IsSensorItem returns true if item is a sensor
	IsSensorItem() bool
}

// GetSensorItem return the sensor item if item is a sensor
func GetSensorItem(item Item) (SensorItem, bool) {
	sensor, ok := item.(SensorItem)
	if !ok {
		// no sensor
		return nil, false
	}
	checker, ok := sensor.(SensorItemChecker)
	if !ok {
		// sensor without checker
		return sensor, true
	}
	if checker.IsSensorItem() {
		// sensor with positive checker
		return sensor, true
	}
	return nil, false
}

// SensorItemSingle provides the functionality to read a specific value
//
// If not set this will be provided by item service
type SensorItemSingle interface {
	SensorItem

	// GetValue returns the given value
	GetValue(name string) interface{}
}

// ActorItem provides the functionality to write values
type ActorItem interface {
	ValuesItem

	// SetValues of actor
	SetValues(values map[string]interface{}) error
}

// ActorItemChecker provides interface to check if actor item
type ActorItemChecker interface {
	// IsActorItem returns true if item is a actor
	IsActorItem() bool
}

// GetActorItem return the actor item if item is a actor
func GetActorItem(item Item) (ActorItem, bool) {
	actor, ok := item.(ActorItem)
	if !ok {
		// no actor
		return nil, false
	}
	checker, ok := actor.(ActorItemChecker)
	if !ok {
		// actor without checker
		return actor, true
	}
	if checker.IsActorItem() {
		// actor with positive checker
		return actor, true
	}
	return nil, false
}

// EventItem will trigger events
type EventItem interface {
	Item

	// Events returns all events that can be emitted
	Events() map[string]api.SimpleHandler

	// SetEventTrigger for item event
	SetEventTrigger(trigger func(action string, data map[string]interface{}))
}

// EventItemChecker provides interface to check if event item
type EventItemChecker interface {
	// IsEventItem returns true if item is a event
	IsEventItem() bool
}

// GetEventItem return the event item if item is a event
func GetEventItem(item Item) (EventItem, bool) {
	event, ok := item.(EventItem)
	if !ok {
		// no event
		return nil, false
	}
	checker, ok := event.(EventItemChecker)
	if !ok {
		// event without checker
		return event, true
	}
	if checker.IsEventItem() {
		// event with positive checker
		return event, true
	}
	return nil, false
}

// LoggerItem will log values to persistent storage
type LoggerItem interface {
	Item

	// LogValues to persistent storage
	LogValues(id string, values map[string]interface{}) error
}

// LoggerItemChecker provides interface to check if logger item
type LoggerItemChecker interface {
	// IsLoggerItem returns true if item is a logger
	IsLoggerItem() bool
}

// GetLoggerItem return the logger item if item is a logger
func GetLoggerItem(item Item) (LoggerItem, bool) {
	logger, ok := item.(LoggerItem)
	if !ok {
		// no logger
		return nil, false
	}
	checker, ok := logger.(LoggerItemChecker)
	if !ok {
		// logger without checker
		return logger, true
	}
	if checker.IsLoggerItem() {
		// logger with positive checker
		return logger, true
	}
	return nil, false
}

// NewItemValue from string
func NewItemValue(value interface{}) (ItemValue, error) {
	if itemValue, ok := value.(ItemValue); ok {
		return itemValue, nil
	}

	item := strings.Split(cast.ToString(value), "#")
	if len(item) < 2 {
		return ItemValue{}, errors.New(
			assets.L.Get("invalid item value should be [ITEM_ID]#[VALUE]"))
	}
	return ItemValue{
		ID:    item[0],
		Value: item[1],
	}, nil
}

// ItemValue represents a specific value from an item
type ItemValue struct {
	ID    string
	Value string
}

// String returns string representation of item value
func (i ItemValue) String() string {
	return fmt.Sprintf("%s#%s", i.ID, i.Value)
}

// MarshalYAML converts struct to item value string
func (i ItemValue) MarshalYAML() (interface{}, error) {
	return i.String(), nil
}

// ItemValueType represents an item value
var ItemValueType = &itemValueType{}

// ActorItemValueType represents an actor item value
var ActorItemValueType = &itemValueType{"actor"}

// SensorItemValueType represents an sensor item value
var SensorItemValueType = &itemValueType{"sensor"}

// itemValueType implements a error type (string or go error)
type itemValueType struct {
	subType string
}

// Convert given value to type if possible
func (t *itemValueType) Convert(value interface{}) (interface{}, error) {
	return NewItemValue(value)
}

// Check if the value matches the type
func (t *itemValueType) Check(value interface{}) bool {
	_, err := t.Convert(value)
	return err == nil
}

// TypeName return the name of the type
func (t *itemValueType) TypeName() string {
	return "ItemValue"
}

// BaseName return the name of the type category
func (t *itemValueType) BaseName() string {
	return "string"
}

// SubType return an optional sub type (actor, sensor, ...)
func (t *itemValueType) SubType() string {
	return t.subType
}

// ActorItemValues matching the given type or any item value if no type given
func ActorItemValues(b Bus, types ...api.Type) map[interface{}]string {
	response := b.HandleRequestWait(Request{
		ID: "core.items#list",
	}, time.Second)

	data := make(map[interface{}]string, len(response.Data))
	for id, item := range response.Data {
		itemData := cast.ToStringMap(item)

		// handle only actors
		if !cast.ToBool(itemData["is_actor"]) {
			continue
		}

		// get values from item -> ignore error if values is not a field map
		fields, _ := itemData["values"].(api.FieldMap)
		for name, value := range fields {
			// no types given -> use every value
			if types == nil {
				data[id+"#"+name] =
					fmt.Sprintf("%s - %s", itemData["name"], name)
			} else {
				// otherwise check if type of value matches
				for _, t := range types {
					if value.Type == t {
						data[id+"#"+name] =
							fmt.Sprintf("%s - %s", itemData["name"], name)
						break
					}
				}
			}
		}
	}
	return data
}

// SensorItemValues matching the given type or any item value if no type given
func SensorItemValues(b Bus, types ...api.Type) map[interface{}]string {
	response := b.HandleRequestWait(Request{
		ID: "core.items#list",
	}, time.Second)

	data := make(map[interface{}]string, len(response.Data))
	for id, item := range response.Data {
		itemData := cast.ToStringMap(item)

		// handle only actors
		if !cast.ToBool(itemData["is_sensor"]) {
			continue
		}

		// get values from item -> ignore error if values is not a field map
		fields, _ := itemData["values"].(api.FieldMap)
		for name, value := range fields {
			// no types given -> use every value
			if types == nil {
				data[id+"#"+name] =
					fmt.Sprintf("%s - %s", itemData["name"], name)
			} else {
				// otherwise check if type of value matches
				for _, t := range types {
					if value.Type == t {
						data[id+"#"+name] =
							fmt.Sprintf("%s - %s", itemData["name"], name)
						break
					}
				}
			}
		}
	}
	return data
}
