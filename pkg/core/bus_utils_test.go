// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package core

import (
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

func (s *BusTestSuite) TestAPIWrapper() {
	s.NoError(s.bus.RegisterService(s.service))

	wrapper := APIWrapper(s.bus, s.service, "test", PathEntry{
		Path: "/:test",
	})

	s.Equal(s.service.Handlers()["test"].Def.Parameters, wrapper.Def.Parameters)
	s.Len(wrapper.Def.Responses, 2)
}

func TestBus_ConfigReadWriteRemove(t *testing.T) {
	ass := assert.New(t)

	bus := new(TestBus)
	bus.HandleRequestWaitFunc = func(request Request, duration time.Duration) Response {
		ass.Equal("core.config#set", request.ID)
		ass.Equal(map[string]interface{}{
			"key":   "test",
			"value": 42,
		}, request.Parameters)
		return Response{}
	}
	ass.NoError(ConfigWrite(bus, "test", 42))

	bus.HandleRequestWaitFunc = func(request Request, duration time.Duration) Response {
		ass.Equal("core.config#get", request.ID)
		ass.Equal(map[string]interface{}{
			"key": "test",
		}, request.Parameters)
		return Response{
			Data: map[string]interface{}{
				"value": 42,
			},
		}
	}
	value, err := ConfigRead(bus, "test")
	ass.NoError(err)
	ass.Equal(42, value)

	bus.HandleRequestWaitFunc = func(request Request, duration time.Duration) Response {
		ass.Equal("core.config#remove", request.ID)
		ass.Equal(map[string]interface{}{
			"key": "test",
		}, request.Parameters)
		return Response{}
	}
	ass.NoError(ConfigRemove(bus, "test"))
}

type testItem struct{}

func (s *testItem) ID() string {
	return "id"
}
func (s *testItem) Name() string {
	return "name"
}

func TestBus_ItemAddRemove(t *testing.T) {
	ass := assert.New(t)

	bus := new(TestBus)

	item := &testItem{}

	called := false
	bus.HandleRequestWaitFunc = func(req Request, timeout time.Duration) Response {
		ass.Equal("core.items#add", req.ID)
		ass.Equal(map[string]interface{}{
			"item": item,
		}, req.Parameters)
		called = true
		return Response{
			OrgRequest: req,
			Error:      errors.New("test"),
		}
	}
	ass.Error(ItemAdd(bus, item), "test")
	ass.True(called)

	called = false
	bus.HandleRequestWaitFunc = func(req Request, timeout time.Duration) Response {
		ass.Equal("core.items#remove", req.ID)
		ass.Equal(map[string]interface{}{
			"item": item,
		}, req.Parameters)
		called = true
		return Response{
			OrgRequest: req,
			Error:      errors.New("test"),
		}
	}
	ass.Error(ItemRemove(bus, item))
	ass.True(called)
}

// TestTestBusTestSuite runs the TestSuite for TestBus
func TestTestBusTestSuite(t *testing.T) {
	suite.Run(t, new(TestBusTestSuite))
}

type TestBusTestSuite struct {
	suite.Suite
	bus *TestBus
}

func (s *TestBusTestSuite) SetupTest() {
	s.bus = new(TestBus)
}

func (s *TestBusTestSuite) TestStart() {
	s.Nil(s.bus.Start())

	called := false
	s.bus.StartFunc = func() error {
		called = true
		return errors.New("test")
	}
	s.EqualError(s.bus.Start(), "test")
	s.True(called)
}

func (s *TestBusTestSuite) TestShutdown() {
	service := new(TestService)
	s.Nil(s.bus.RegisterService(service))

	called := false
	s.bus.ShutdownFunc = func() {
		called = true
	}
	s.bus.Shutdown()
	s.True(called)
}

func (s *TestBusTestSuite) TestRegisterService() {
	service := new(TestService)
	s.Nil(s.bus.RegisterService(service))

	called := false
	s.bus.RegisterServiceFunc = func(srv Service) error {
		s.Equal(service, srv)
		called = true
		return errors.New("test")
	}
	s.EqualError(s.bus.RegisterService(service), "test")
	s.True(called)
}

func (s *TestBusTestSuite) TestHandleRequest() {
	request := Request{ID: "aaa"}
	s.bus.HandleRequest(request)

	called := false
	s.bus.HandleRequestFunc = func(req Request) {
		s.Equal(request, req)
		called = true
	}
	s.bus.HandleRequest(request)
	s.True(called)
}

func (s *TestBusTestSuite) TestHandleRequestWait() {
	request := Request{ID: "aaa"}
	resp := s.bus.HandleRequestWait(request, time.Second)
	s.Equal(request, resp.OrgRequest)
	s.EqualError(resp.Error, "no response to request aaa")

	testResponse := Response{
		OrgRequest: request,
		Data: map[string]interface{}{
			"aaa": 42,
		},
	}

	called := false
	s.bus.HandleRequestWaitFunc = func(req Request, timeout time.Duration) Response {
		s.Equal(request, req)
		s.Equal(time.Second, timeout)
		called = true
		return testResponse
	}
	resp = s.bus.HandleRequestWait(request, time.Second)
	s.Equal(testResponse, resp)
	s.True(called)
}

func (s *TestBusTestSuite) TestRegisterEventListener() {
	testChan := make(chan Event)
	s.Nil(s.bus.RegisterEventListener("test", testChan))

	called := false
	s.bus.RegisterEventListenerFunc = func(id string, listener chan Event) error {
		s.Equal("test", id)
		s.Equal(testChan, listener)
		called = true
		return errors.New("test")
	}
	s.EqualError(s.bus.RegisterEventListener("test", testChan), "test")
	s.True(called)
}

func (s *TestBusTestSuite) TestUnregisterEventListener() {
	testChan := make(chan Event)
	s.bus.UnregisterEventListener("test", testChan)

	called := false
	s.bus.UnregisterEventListenerFunc = func(id string, listener chan Event) {
		s.Equal("test", id)
		s.Equal(testChan, listener)
		called = true
	}
	s.bus.UnregisterEventListener("test", testChan)
	s.True(called)
}
