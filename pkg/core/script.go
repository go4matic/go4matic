// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package core

import (
	"gitlab.com/go4matic/go4matic/pkg/api"
)

// ScriptFunction defines a function that can be called from script
type ScriptFunction struct {
	Def  api.Function
	Func func(script Script, args []interface{}) []interface{}
}

// ScriptModuleDefinition groups multiple functions
type ScriptModuleDefinition struct {
	Description         string
	Functions           map[string]ScriptFunction
	CleanupFunc         func(script Script)
	CallbackChannelFunc func(script Script, c chan func())
}

// FunctionDef returns function definitions for module
func (s *ScriptModuleDefinition) FunctionDef() map[string]api.Function {
	functions := make(map[string]api.Function, len(s.Functions))
	for key, f := range s.Functions {
		functions[key] = f.Def
	}
	return functions
}

// ScriptDefinition defines the functionality of a script
type ScriptDefinition struct {
	// modules available in script
	Modules map[string]ScriptModuleDefinition

	// function available outside packages
	GlobalFunctions map[string]ScriptFunction

	// global const values
	ConstantValues map[string]interface{}
}

// Script object
type Script interface {
	Start()
	Stop()
	Content() string
	IsRunning() bool
}

// ScriptModule object
type ScriptModule interface {
	Name() string
	Def() ScriptModuleDefinition
}
