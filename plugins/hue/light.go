// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package hue

import (
	"errors"
	"math"
	"strings"
	"sync"

	"github.com/amimof/huego"
	"github.com/spf13/cast"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
)

// Light instance
type Light struct {
	light huego.Light

	trigger func(action string, data map[string]interface{})

	mutex sync.RWMutex
}

// NewLight create new light instance
func NewLight(light huego.Light) *Light {
	return &Light{
		light: light,
	}
}

// ID of item
func (l *Light) ID() string {
	l.mutex.RLock()
	defer l.mutex.RUnlock()

	return "plugin.hue." + strings.Replace(l.light.UniqueID, ":", "_", -1)
}

// Name of item
func (l *Light) Name() string {
	l.mutex.RLock()
	defer l.mutex.RUnlock()

	return l.light.Name
}

// GetValueTypes of light
func (l *Light) GetValueTypes() api.FieldMap {
	l.mutex.RLock()
	defer l.mutex.RUnlock()
	// OSRAM On/Off plug has no color or brightness
	if l.light.Type == "On/Off plug-in unit" {
		return api.FieldMap{
			"on": {
				Description: assets.L.Get("ON/Off state of plug"),
				Type:        api.Bool,
			},
		}
	}

	return api.FieldMap{
		"on": {
			Description: assets.L.Get("Power state of light"),
			Type:        api.Bool,
		},
		"brightness": {
			Description: assets.L.Get("Brightness of light"),
			Type:        api.Uint8,
		},
		"color": {
			Description: assets.L.Get("Color of light (RGB)"),
			Type:        api.Uint32,
		},
	}
}

// Events returns all events that can be emitted
func (l *Light) Events() map[string]api.SimpleHandler {
	return map[string]api.SimpleHandler{
		"changed": {
			Description: assets.L.Get("Emitted if a value changed"),
			Parameters:  l.GetValueTypes(),
		},
	}
}

// SetEventTrigger for item event
func (l *Light) SetEventTrigger(trigger func(action string, data map[string]interface{})) {
	l.mutex.RLock()
	defer l.mutex.RUnlock()

	l.trigger = trigger
}

// SetValues of light
func (l *Light) SetValues(values map[string]interface{}) error {
	l.mutex.RLock()
	defer l.mutex.RUnlock()

	state := huego.State{
		On:  l.light.State.On,
		Bri: l.light.State.Bri,
		Hue: l.light.State.Hue,
		Sat: l.light.State.Sat,
	}

	changedValues := make(map[string]interface{})

	// power state
	if on, ok := values["on"]; ok {
		state.On = cast.ToBool(on)
		changedValues["on"] = state.On
	}

	// brightness
	if bri, ok := values["brightness"]; ok {
		briInt := cast.ToUint8(bri)
		if briInt < 1 {
			briInt = 1
		}
		state.Bri = briInt
		changedValues["brightness"] = state.Bri
	}

	// color as rgb
	if h, ok := values["color"]; ok {
		color := cast.ToUint32(h)
		state.Hue, state.Sat = rgbToHueSat(color)
		changedValues["color"] = color

		// to set hue and set even if 0 increase it by one
		//  required to workaround omitempty in hue lib
		if state.Hue == 0 {
			state.Hue = 1
		}
		if state.Sat == 0 {
			state.Sat = 1
		}
	}

	// set state
	err := l.light.SetState(state)
	if err != nil {
		// catch error if light is switched off again
		if !(strings.Contains(err.Error(), "Device is set to off") && !state.On) {
			return errors.New(assets.L.Get("failed to set light state: %s", err))
		}
	}

	// trigger change event here because SetState already updates cached state
	if len(changedValues) > 0 {
		l.trigger("changed", changedValues)
	}
	return nil
}

// GetValues of sensor
func (l *Light) GetValues() map[string]interface{} {
	l.mutex.RLock()
	defer l.mutex.RUnlock()

	values := make(map[string]interface{})
	values["on"] = l.light.State.On
	values["brightness"] = l.light.State.Bri
	values["color"] = hueSatToRgb(l.light.State.Hue, l.light.State.Sat)
	return values
}

// update actor state
func (l *Light) update(light huego.Light) {
	l.mutex.Lock()
	defer l.mutex.Unlock()

	values := make(map[string]interface{})
	if l.light.State.On != light.State.On {
		values["on"] = light.State.On
	}
	if l.light.State.Bri != light.State.Bri {
		values["brightness"] = light.State.Bri
	}
	if l.light.State.Sat != light.State.Sat ||
		l.light.State.Hue != light.State.Hue {

		values["color"] = hueSatToRgb(light.State.Hue, light.State.Sat)
	}

	if len(values) > 0 {
		l.trigger("changed", values)
	}

	l.light.State = light.State
	l.light.Name = light.Name
}

// convert philips hue & sat values to RGB
func hueSatToRgb(hue uint16, sat uint8) uint32 {
	// convert from philips hue range
	fHue := float64(hue) / 65535
	fSat := float64(sat) / 254

	x := fSat * (1 - math.Abs(math.Mod(fHue*6, 2)-1))
	m := 0.5 - fSat/2

	// if sat or hue is 0 -> no color -> m = 0
	if sat == 0 || hue == 0 {
		m = 0
	}

	// convert hue and sat to RGB
	var fR, fG, fB float64
	if fHue < 1/6.0 {
		fR = fSat
		fG = x
		fB = 0

	} else if fHue < 1/3.0 {
		fR = x
		fG = fSat
		fB = 0

	} else if fHue < 1/2.0 {
		fR = 0
		fG = fSat
		fB = x

	} else if fHue < 2/3.0 {
		fR = 0
		fG = x
		fB = fSat

	} else if fHue < 5/6.0 {
		fR = x
		fG = 0
		fB = fSat

	} else {
		fR = fSat
		fG = 0
		fB = x
	}

	// get RGB values
	r := uint32(math.Round((fR + m) * 255))
	g := uint32(math.Round((fG + m) * 255))
	b := uint32(math.Round((fB + m) * 255))

	// merge to single variable
	return r<<16 | g<<8 | b
}

// convert RGB to philips hue & sat values
func rgbToHueSat(rgb uint32) (uint16, uint8) {
	// split rgb
	r := uint8((rgb & 0xFF0000) >> 16)
	g := uint8((rgb & 0xFF00) >> 8)
	b := uint8(rgb & 0xFF)

	// convert to range 0-1
	fR := float64(r) / 255
	fG := float64(g) / 255
	fB := float64(b) / 255

	// get min and max
	cMax := math.Max(fR, math.Max(fG, fB))
	cMin := math.Min(fR, math.Min(fG, fB))

	// find delta of min and max
	cDelta := cMax - cMin

	// prepare float variables
	var fHue, fSat float64

	// if delta is 0 -> no color
	if cDelta == 0 {
		fHue = 0
		fSat = 0

	} else {
		// calculate hue
		if cMax == fR {
			fHue = (fG - fB) / cDelta
			if fG < fB {
				fHue += 6
			}
		} else if cMax == fG {
			fHue = (fB-fR)/cDelta + 2
		} else {
			fHue = (fR-fG)/cDelta + 4
		}
		fHue /= 6

		// calculate sat
		if cMax+cMin-1 < 0 {
			fSat = cDelta / (1 - (cMax+cMin-1)*(-1))
		} else {
			fSat = cDelta / (1 - (cMax + cMin - 1))
		}
	}

	// convert to philips hue range
	h := uint16(math.Round(fHue * 65535))
	sat := uint8(math.Round(fSat * 254))
	return h, sat
}
