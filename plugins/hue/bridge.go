// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package hue

import (
	"errors"
	"sync"
	"time"

	"github.com/amimof/huego"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// Bridge instance
type Bridge struct {
	// address of hue bridge
	Address string
	// user name for authentication
	Username string
	// interval for state updates
	UpdateInterval time.Duration

	// connection to hue bridge
	connection *huego.Bridge
	// ticker for state updates
	ticker *time.Ticker

	// map of loaded actors/lights
	lights map[string]*Light
	// map of loaded sensors
	sensors map[string]*Sensor

	mutex sync.Mutex
}

// RegisterBridge creates new bridge instance wit automatic user registration
func RegisterBridge(address string) (*Bridge, error) {
	bridge := &huego.Bridge{Host: address}

	username, err := bridge.CreateUser("Go4Matic")
	if err != nil {
		return nil, errors.New(assets.L.Get("failed to create user: %s", err))
	}

	return NewBridge(address, username)
}

// NewBridge creates new bridge instance
func NewBridge(address, username string) (*Bridge, error) {
	b := &Bridge{
		Address:        address,
		Username:       username,
		UpdateInterval: time.Millisecond * 500,
		lights:         make(map[string]*Light),
		sensors:        make(map[string]*Sensor),
	}

	// try to connect
	if err := b.connect(); err != nil {
		return b, errors.New(assets.L.Get("failed to connect to HUE bridge: %s", err))
	}

	return b, nil
}

// connect to bridge
func (b *Bridge) connect() error {
	b.mutex.Lock()
	defer b.mutex.Unlock()

	// create bridge connection
	bridge := huego.New(b.Address, b.Username)

	// check login to bridge
	_, err := bridge.GetConfig()
	if err != nil {
		return err
	}

	b.connection = bridge
	return nil
}

// start state update
func (b *Bridge) start(bus core.Bus) {
	b.mutex.Lock()
	defer b.mutex.Unlock()

	if b.ticker != nil {
		// already running
		return
	}

	b.ticker = time.NewTicker(b.UpdateInterval)
	go func() {
		for range b.ticker.C {
			b.update(bus)
		}
	}()
}
func (b *Bridge) stop() {
	b.mutex.Lock()
	defer b.mutex.Unlock()

	if b.ticker == nil {
		// not running
		return
	}

	// stop the ticker for the state update
	b.ticker.Stop()
	b.ticker = nil
}

// update items states
func (b *Bridge) update(bus core.Bus) {
	b.mutex.Lock()
	defer b.mutex.Unlock()

	// not connected ?
	if b.connection == nil {
		err := b.connect()
		// connection failed
		if err != nil {
			// TODO to much log entries?
			zap.L().Warn(err.Error())
			return
		}
	}

	// get all lights
	lights, err := b.connection.GetLights()
	if err != nil {
		zap.L().Error(err.Error())
		return
	}
	knownIds := make(map[string]bool, len(lights))

	// add & update actors
	for _, light := range lights {
		knownIds[light.UniqueID] = true

		// update existing
		item, ok := b.lights[light.UniqueID]
		if ok {
			item.update(light)
			continue
		}

		// add new light
		item = NewLight(light)
		err = core.ItemAdd(bus, item)
		if err != nil {
			zap.L().Error(err.Error())
			continue
		}
		b.lights[light.UniqueID] = item
	}

	// remove actors
	for k, item := range b.lights {
		if _, ok := knownIds[k]; ok {
			continue
		}

		// remove non existing lights
		err = core.ItemRemove(bus, item)
		if err != nil {
			zap.L().Error(err.Error())
		}
		delete(b.lights, k)
	}

	// get all sensors
	sensors, err := b.connection.GetSensors()
	if err != nil {
		zap.L().Error(err.Error())
		return
	}

	knownIds = make(map[string]bool, len(sensors))
	// add & update sensors
	for _, sensor := range sensors {
		if sensor.Type == "Daylight" {
			continue
		}
		knownIds[sensor.UniqueID] = true

		// update existing
		item, ok := b.sensors[sensor.UniqueID]
		if ok {
			item.update(sensor)
			continue
		}

		// add new
		item = NewSensor(sensor)
		err = core.ItemAdd(bus, item)
		if err != nil {
			zap.L().Error(err.Error())
			continue
		}
		b.sensors[sensor.UniqueID] = item
	}

	// remove sensors
	for k, item := range b.sensors {
		if _, ok := knownIds[k]; ok {
			continue
		}

		// remove non existing sensors
		err := core.ItemRemove(bus, item)
		if err != nil {
			zap.L().Error(err.Error())
		}
		delete(b.sensors, k)
	}
}
