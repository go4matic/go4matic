// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package hue

import (
	"errors"
	"sync"

	"github.com/spf13/cast"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// Plugin that implements HUE functionality
type Plugin struct {
	bus core.Bus

	// list of configured hue bridges
	bridges map[string]*Bridge

	// mutex for entries
	mutex sync.RWMutex
}

// ID of plugin
func (p *Plugin) ID() string {
	return "hue"
}

// Version information of plugin
func (p *Plugin) Version() string {
	return "0.5.0"
}

// Init plugin
func (p *Plugin) Init(b core.Bus) error {
	p.bus = b
	return nil
}

// Shutdown called if plugin should be stopped
func (p *Plugin) Shutdown() {
	p.mutex.RLock()
	defer p.mutex.RUnlock()

	for _, b := range p.bridges {
		b.stop()
	}
}

// Config returns the definition of the config interface
func (p *Plugin) Config() *core.PluginConfigDef {
	return &core.PluginConfigDef{
		Entries: core.PluginConfigEntryDefMap{
			"bridges": {
				Config: map[string]api.Field{
					"address": {
						Type:        api.String,
						Required:    true,
						Description: assets.L.Get("Address of Hue bridge"),
					},
					"username": {
						Type:        api.String,
						Description: assets.L.Get("Username for authentication (Empty for automatic registration)"),
					},
				},

				LoadEntryFunc: p.configLoad,
				AddEntryFunc:  p.configAdd,
				//ChangeEntryFunc: nil,
				RemoveEntryFunc: p.configRemove,
			},
		},
	}
}

func (p *Plugin) configLoad(entries map[string]map[string]interface{}) error {
	p.bridges = make(map[string]*Bridge, len(entries))
	for k, v := range entries {
		// create bridge
		bridge, err := NewBridge(cast.ToString(v["address"]), cast.ToString(v["username"]))
		if err != nil {
			zap.L().Warn(err.Error())
		}
		p.bridges[k] = bridge

		// start handling
		bridge.start(p.bus)
	}
	return nil
}

func (p *Plugin) configAdd(key string, config map[string]interface{}) error {
	address := cast.ToString(config["address"])
	username := cast.ToString(config["username"])

	var err error
	var bridge *Bridge
	if username == "" {
		bridge, err = RegisterBridge(address)
	} else {
		bridge, err = NewBridge(address, username)
	}
	if err != nil {
		return errors.New(assets.L.Get("failed to connect to HUE bridge at %s: %v", address, err))
	}

	config["username"] = bridge.Username

	p.bridges[key] = bridge
	bridge.start(p.bus)
	return nil
}

func (p *Plugin) configRemove(key string) error {
	bridge, ok := p.bridges[key]
	if !ok {
		return errors.New(assets.L.Get("no bridge %s", key))
	}

	// remove bridge
	bridge.stop()
	delete(p.bridges, key)

	for _, item := range bridge.lights {
		err := core.ItemRemove(p.bus, item)
		if err != nil {
			zap.L().Error(err.Error())
		}
	}
	for _, item := range bridge.sensors {
		err := core.ItemRemove(p.bus, item)
		if err != nil {
			zap.L().Error(err.Error())
		}
	}
	return nil
}
