// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package sma

import (
	"errors"
	"sync"
	"time"

	"github.com/spf13/cast"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"

	"gitlab.com/bboehmke/sunny"
)

// Plugin that implements HUE functionality
type Plugin struct {
	bus core.Bus

	// ticker for updates
	tickerUpdate *time.Ticker
	// ticker for discovery
	tickerDiscovery *time.Ticker

	// list of configured devices
	devices map[uint32]*Device

	// user password for devices
	password string

	// mutex for entries
	mutex sync.RWMutex
}

// ID of plugin
func (p *Plugin) ID() string {
	return "sma"
}

// Version information of plugin
func (p *Plugin) Version() string {
	return "0.1.0"
}

// Init plugin
func (p *Plugin) Init(b core.Bus) error {
	p.bus = b
	p.devices = make(map[uint32]*Device)
	return nil
}

// Config returns the definition of the config interface
func (p *Plugin) Config() *core.PluginConfigDef {
	return &core.PluginConfigDef{
		Config: api.FieldMap{
			"enabled": {
				Type:         api.Bool,
				Required:     true,
				DefaultValue: false,
				Description:  assets.L.Get("Enable communication with devices"),
			},
			"password": {
				Type:         api.String,
				DefaultValue: "0000",
				Description:  assets.L.Get("User password for devices"),
			},
			"interface": {
				Type:        api.String,
				Description: assets.L.Get("Interface to use for communication (requires restart on change)"),
			},
		},
		LoadFunc:   p.configLoad,
		ChangeFunc: p.configChanged,
	}
}

// start device update
func (p *Plugin) start() {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	if p.tickerUpdate != nil {
		// already running
		return
	}

	p.tickerUpdate = time.NewTicker(time.Second)
	go func() {
		for range p.tickerUpdate.C {
			p.update(p.bus)
		}
	}()
	p.tickerDiscovery = time.NewTicker(time.Second * 10)
	go func() {
		for range p.tickerDiscovery.C {
			p.discover(p.bus)
		}
	}()
}

// stop device update
func (p *Plugin) stop() {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	if p.tickerUpdate == nil {
		// not running
		return
	}

	// stop tickers
	p.tickerUpdate.Stop()
	p.tickerDiscovery.Stop()
	p.tickerUpdate = nil

	for _, item := range p.devices {
		err := core.ItemRemove(p.bus, item)
		if err != nil {
			zap.L().Error(err.Error())
		}
	}
}

// discover devices
func (p *Plugin) discover(bus core.Bus) {
	devices, err := sunny.DiscoverDevices(p.password)
	if err != nil {
		return
	}

	p.mutex.Lock()
	defer p.mutex.Unlock()

	// stop if not running
	if p.tickerUpdate == nil {
		return
	}

	for _, device := range devices {
		if _, ok := p.devices[device.SerialNumber()]; ok {
			continue
		}

		name, _ := device.GetDeviceName()

		p.devices[device.SerialNumber()] = &Device{
			device: device,
			name:   name,
		}

		err := core.ItemAdd(bus, p.devices[device.SerialNumber()])
		if err != nil {
			zap.L().Error(assets.L.Get("failed to add device: %s", err))
		}
	}
	// TODO cleanup
}

// update devices
func (p *Plugin) update(bus core.Bus) {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	for _, device := range p.devices {
		device.update(bus)
	}
}

func (p *Plugin) configLoad(config map[string]interface{}) error {
	err := sunny.SetMulticastInterface(cast.ToString(config["interface"]))
	if err != nil {
		zap.L().Error(assets.L.Get("failed to set interface: %s", err))
	}

	p.password = cast.ToString(config["password"])

	if cast.ToBool(config["enabled"]) {
		p.start()
	}
	return nil
}

func (p *Plugin) configChanged(config map[string]interface{}) error {
	if _, ok := config["interface"]; ok {
		err := sunny.SetMulticastInterface(cast.ToString(config["interface"]))
		if err != nil {
			return errors.New(assets.L.Get("failed to set interface: %s", err))
		}
	}

	if _, ok := config["password"]; ok {
		p.password = cast.ToString(config["password"])
	}

	if _, ok := config["enabled"]; ok {
		if cast.ToBool(config["enabled"]) {
			p.start()
		} else {
			p.stop()
		}
	}
	return nil
}
