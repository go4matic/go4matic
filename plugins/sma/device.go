// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package sma

import (
	"fmt"
	"sync"

	"gitlab.com/bboehmke/sunny"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// Device handles one SMA device as item
type Device struct {
	device *sunny.Device
	name   string

	values map[string]interface{}

	mutex   sync.RWMutex
	trigger func(action string, data map[string]interface{})
}

// ID of item
func (d *Device) ID() string {
	d.mutex.RLock()
	defer d.mutex.RUnlock()

	return fmt.Sprintf("plugin.sma.%d", d.device.SerialNumber())
}

// Name of item
func (d *Device) Name() string {
	d.mutex.RLock()
	defer d.mutex.RUnlock()

	return fmt.Sprintf("%s (%s)", d.name, d.device.Address().IP)
}

// GetValueTypes of item
func (d *Device) GetValueTypes() api.FieldMap {
	d.mutex.RLock()
	defer d.mutex.RUnlock()

	fields := make(api.FieldMap, len(d.values))
	for key, value := range d.values {
		fields[key] = api.Field{
			Type:        api.TypeFromValue(value),
			Description: d.device.GetValueDescription(key),
		}
	}
	return fields
}

// Events returns all events that can be emitted
func (d *Device) Events() map[string]api.SimpleHandler {
	return map[string]api.SimpleHandler{
		"changed": {
			Description: assets.L.Get("Emitted if a value changed"),
			Parameters:  d.GetValueTypes(),
		},
	}
}

// SetEventTrigger for item event
func (d *Device) SetEventTrigger(trigger func(action string, data map[string]interface{})) {
	d.trigger = trigger
}

// GetValues of sensor
func (d *Device) GetValues() map[string]interface{} {
	d.mutex.RLock()
	defer d.mutex.RUnlock()

	return d.values
}

// update devices
func (d *Device) update(bus core.Bus) {
	d.mutex.RLock()

	values, err := d.device.GetValues()
	if err != nil {
		d.mutex.RUnlock()
		return
	}
	d.mutex.RUnlock()
	d.mutex.Lock()
	defer d.mutex.Unlock()

	d.values = values
	d.trigger("changed", values)
}
