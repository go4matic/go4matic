// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mqtt

import (
	"errors"
	"fmt"
	"math/rand"
	"sync"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
)

// Broker handles the connection to an MQTT broker
type Broker struct {
	client   mqtt.Client
	address  string
	username string
	password string

	valuesMutex sync.RWMutex
	values      map[string][]*Value
}

// NewBroker creates a new broker connection
func NewBroker(address, username, password string) (*Broker, error) {
	// set options for client
	opt := mqtt.NewClientOptions()
	opt.AddBroker(address)
	opt.SetClientID(fmt.Sprintf("go4matic-%d", rand.Int()))
	opt.SetAutoReconnect(true)
	opt.SetConnectionLostHandler(func(client mqtt.Client, e error) {
		zap.S().Debugf("MQTT: connection to %s lost - %s", address, e)
	})

	// only use valid credentials
	if username != "" && password != "" {
		opt.SetUsername(username)
		opt.SetPassword(password)
	}

	broker := &Broker{
		address:  address,
		username: username,
		password: password,
		values:   make(map[string][]*Value),
	}
	// resubscribe topics on reconnect
	opt.SetOnConnectHandler(broker.resubscribe)

	// connect to broker
	client := mqtt.NewClient(opt)
	token := client.Connect()
	if !token.WaitTimeout(time.Second) {
		client.Disconnect(250)
		return nil, errors.New(assets.L.Get("timeout while connecting to %s", address))
	} else if token.Error() != nil {
		return nil, errors.New(assets.L.Get("failed to connect to %s: %s", address, token.Error()))
	}
	broker.client = client

	return broker, nil
}

// Close connection to broker
func (b *Broker) Close() {
	b.client.Disconnect(250)
}

// handle received message
func (b *Broker) handle(client mqtt.Client, msg mqtt.Message) {
	b.valuesMutex.RLock()
	defer b.valuesMutex.RUnlock()

	for _, v := range b.values[msg.Topic()] {
		go v.update(string(msg.Payload()))
	}
}

// resubscribe topics on broker
func (b *Broker) resubscribe(mqtt.Client) {
	zap.S().Debugf("MQTT: connected to %s", b.address)

	b.valuesMutex.RLock()
	defer b.valuesMutex.RUnlock()

	for topic := range b.values {
		token := b.client.Subscribe(topic, 2, b.handle)
		if !token.WaitTimeout(time.Millisecond * 100) {
			zap.L().Error(assets.L.Get("failed to subscribe to topic %s", topic))
		} else if token.Error() != nil {
			zap.L().Error(token.Error().Error())
		}
	}
}

// Subscribe value topic
func (b *Broker) Subscribe(value *Value) error {
	b.valuesMutex.Lock()
	defer b.valuesMutex.Unlock()

	_, ok := b.values[value.topic]
	if !ok {
		token := b.client.Subscribe(value.topic, 2, b.handle)
		if !token.WaitTimeout(time.Millisecond * 100) {
			return errors.New(assets.L.Get("failed to subscribe to topic %s", value.topic))
		} else if token.Error() != nil {
			return token.Error()
		}
		b.values[value.topic] = []*Value{value}
	} else {
		b.values[value.topic] = append(b.values[value.topic], value)
	}
	return nil
}

// Unsubscribe value topic
func (b *Broker) Unsubscribe(value *Value) {
	b.valuesMutex.Lock()
	defer b.valuesMutex.Unlock()

	_, ok := b.values[value.topic]
	if !ok {
		return
	}

	values := make([]*Value, 0, len(b.values[value.topic]))
	for _, v := range b.values[value.topic] {
		if value != v {
			values = append(values, v)
		}
	}

	if len(values) == 0 {
		b.client.Unsubscribe(value.topic)
	}
	delete(b.values, value.topic)
}

// Publish sends message to topic
func (b *Broker) Publish(topic, value string) error {
	token := b.client.Publish(topic, 2, true, value)
	if !token.WaitTimeout(time.Millisecond * 100) {
		return errors.New(assets.L.Get("timeout while publish message to %s", b.address))
	}
	return token.Error()
}
