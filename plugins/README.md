# Plugins

In this directory you find all plugins that are maintained together with
Go4Matic.

| Name                 | Description                                                                                                  |
| -------------------- | ------------------------------------------------------------------------------------------------------------ |
| dummy                | Plugin for test and demonstration purpose                                                                    |
| homematic            | Interface to [HomeMatic](https://www.homematic.com/) devices                                                 |
| hue                  | Interface to [Philips HUE](https://www.meethue.com) devices                                                  |
| influxdb             | Log data to [InfluxDB](https://docs.influxdata.com/influxdb/v1.7/)                                           |
| mqtt                 | Interface to items based on [MQTT](http://mqtt.org/)                                                         |
| sma                  | Interface to [SMA](https://www.sma.de/) devices                                                              |
| weather_underground  | Implementation of [Weather Underground](https://www.wunderground.com/) service fore private weather stations |
