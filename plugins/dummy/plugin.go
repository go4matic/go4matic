// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dummy

import (
	"fmt"
	"time"

	"github.com/spf13/cast"

	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// Plugin that implements some dummy functions
type Plugin struct {
	bus core.Bus

	// list of configured entries
	entries map[string]*dummyItem
}

// ID of plugin
func (p *Plugin) ID() string {
	return "dummy"
}

// Version information of plugin
func (p *Plugin) Version() string {
	return "0.0.0"
}

// Init plugin
func (p *Plugin) Init(b core.Bus) error {
	p.bus = b
	return nil
}

// Config returns the definition of the config interface
func (p *Plugin) Config() *core.PluginConfigDef {
	return &core.PluginConfigDef{
		Config: api.FieldMap{
			"test": {
				Description:  "Test global value",
				Type:         api.Int,
				DefaultValue: 42,
			},
		},
		Entries: core.PluginConfigEntryDefMap{
			"entries": {
				Config: map[string]api.Field{
					"interval": {
						Type:         api.Uint,
						Description:  "Event interval (in s)",
						DefaultValue: 10,
					},
				},

				LoadEntryFunc: p.entryConfigLoad,
				AddEntryFunc:  p.entryConfigAdd,
				//ChangeEntryFunc: nil,
				RemoveEntryFunc: p.entryConfigRemove,
			},
		},
		LoadFunc:   p.configLoad,
		ChangeFunc: p.configChanged,
	}
}
func (p *Plugin) configLoad(config map[string]interface{}) error {
	return nil
}
func (p *Plugin) configChanged(config map[string]interface{}) error {
	return nil
}

func (p *Plugin) entryConfigLoad(entries map[string]map[string]interface{}) error {
	p.entries = make(map[string]*dummyItem, len(entries))
	for k, v := range entries {
		err := p.createEntry(k, cast.ToUint(v["interval"]))
		if err != nil {
			return err
		}
	}
	return nil
}
func (p *Plugin) entryConfigAdd(key string, config map[string]interface{}) error {
	interval := cast.ToUint(config["interval"])

	if interval < 1 {
		return fmt.Errorf("invalid interval %d - must be positive", interval)
	}
	return p.createEntry(key, interval)
}
func (p *Plugin) entryConfigRemove(key string) error {
	return p.removeEntry(key)
}

func (p *Plugin) createEntry(key string, interval uint) error {
	item := &dummyItem{key: key}
	err := core.ItemAdd(p.bus, item)
	if err != nil {
		return fmt.Errorf("failed to add dummy item %s", err)
	}

	item.StartTrigger(time.Second * time.Duration(interval))

	p.entries[key] = item

	return nil
}
func (p *Plugin) removeEntry(key string) error {
	p.entries[key].StopTrigger()

	err := core.ItemRemove(p.bus, p.entries[key])
	if err != nil {
		return fmt.Errorf("failed to remove dummy item %s", err)
	}

	delete(p.entries, key)

	return nil
}
