// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package homematic

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/bboehmke/homematic"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
)

// NewDevice creates a new device
func NewDevice(device *homematic.Device) (*Device, error) {
	desc, err := device.GetValuesDescription()
	if err != nil {
		return nil, errors.New(assets.L.Get("failed to get device info from %s: %v", device.Address, err))
	}

	// no values -> empty device -> ignore
	if len(desc) == 0 {
		return nil, nil
	}

	// create device
	dev := &Device{
		device: device,
		desc:   desc,
	}

	for _, desc := range desc {
		if desc.OperationWrite {
			dev.isActor = true
		}
		if desc.OperationRead {
			dev.isSensor = true
		}
		if desc.OperationEvent {
			dev.isEvent = true
		}
	}
	return dev, nil
}

// Device of HomeMatic CCU
type Device struct {
	device *homematic.Device
	desc   map[string]homematic.ParameterDescription
	fields api.FieldMap

	isSensor bool
	isActor  bool
	isEvent  bool
}

// ID of item
func (d *Device) ID() string {
	return "plugin.homematic." + strings.Replace(d.device.Address, ":", "_", -1)
}

// Name of item
func (d *Device) Name() string {
	return d.device.GetName()
}

// fieldFromDescription converts a ParameterDescription to an api Field
func fieldFromDescription(desc homematic.ParameterDescription) api.Field {
	description := desc.Unit
	var t api.Type
	switch strings.ToLower(desc.Type) {
	case "float":
		t = api.Float32
	case "integer":
		t = api.Int32
	case "enum":
		t = api.Int32

		values := make([]string, len(desc.ValueList))
		for i, v := range desc.ValueList {
			values[i] = fmt.Sprintf("%d=%s", i, v)
		}
		description = strings.Join(values, ", ")
	case "bool", "action":
		t = api.Bool
	case "string":
		t = api.String
	default:
		return api.Field{}
	}

	return api.Field{
		Type:         t,
		Description:  description,
		Required:     false,
		DefaultValue: desc.Default,
		Order:        uint(desc.TabOrder),
	}
}

// GetValueTypes of switch
func (d *Device) GetValueTypes() api.FieldMap {
	if d.fields == nil {
		d.fields = make(api.FieldMap, len(d.desc))
		for key, desc := range d.desc {
			if desc.FlagInternal || desc.FlagTransform {
				continue
			}
			if !desc.OperationRead && !desc.OperationWrite {
				continue
			}

			field := fieldFromDescription(desc)
			if field.Type == nil {
				continue
			}

			d.fields[key] = field
		}
	}
	return d.fields
}

// Events returns all events that can be emitted
func (d *Device) Events() map[string]api.SimpleHandler {
	fields := make(api.FieldMap, len(d.desc))
	for key, desc := range d.desc {
		if desc.FlagInternal || desc.FlagTransform {
			continue
		}
		if !desc.OperationEvent {
			continue
		}

		field := fieldFromDescription(desc)
		if field.Type == nil {
			continue
		}

		fields[key] = field
	}
	return map[string]api.SimpleHandler{
		"changed": {
			Parameters: fields,
		},
	}
}

// SetEventTrigger for item event
func (d *Device) SetEventTrigger(trigger func(action string, data map[string]interface{})) {
	d.device.SetValueChangedHandler(func(key string, value interface{}) {
		trigger("changed", map[string]interface{}{
			key: value,
		})
	})
}

// SetValues of switch
func (d *Device) SetValues(values map[string]interface{}) error {
	for key, value := range values {
		desc, ok := d.desc[key]
		if !ok {
			return errors.New(assets.L.Get("unknown value: %s", key))
		}
		if !desc.OperationWrite {
			return errors.New(assets.L.Get("value not writeable: %s", key))
		}

		err := d.device.SetValue(key, value)
		if err != nil {
			return errors.New(assets.L.Get("failed to set value: %v", err))
		}
	}
	return nil
}

// GetValues of switch
func (d *Device) GetValues() map[string]interface{} {
	if len(d.fields) == 0 {
		return map[string]interface{}{}
	}

	values, err := d.device.GetValues()
	if err != nil {
		zap.L().Error(assets.L.Get("failed to get values: %v", err))
		return map[string]interface{}{}
	}
	return values
}

// IsEventItem returns true if item is a event
func (d *Device) IsEventItem() bool {
	return d.isEvent
}

// IsSensorItem returns true if item is a sensor
func (d *Device) IsSensorItem() bool {
	return d.isSensor
}

// IsActorItem returns true if item is a actor
func (d *Device) IsActorItem() bool {
	return d.isActor
}
