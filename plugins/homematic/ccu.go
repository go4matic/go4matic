// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package homematic

import (
	"errors"
	"fmt"
	"regexp"
	"sync"
	"time"

	"gitlab.com/bboehmke/homematic"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// CCU instance of homematic CCU
type CCU struct {
	// address of CCU
	Address string
	// Regex to exclude entries from item list
	Exclude *regexp.Regexp
	// interval for state updates
	UpdateInterval time.Duration

	// connection to CCU
	ccu *homematic.CCU
	// ticker for state updates
	ticker        *time.Ticker
	tickerRunning bool

	// known devices
	devices map[string]*Device

	mutex sync.Mutex
}

// NewCCU creates new CCU instance
func NewCCU(address string, exclude *regexp.Regexp) (*CCU, error) {
	ccu, err := homematic.NewCCUCustom(address,
		fmt.Sprintf("go4matic%x", time.Now().UnixNano()))
	if err != nil {
		return nil, err
	}

	b := &CCU{
		Address:        address,
		Exclude:        exclude,
		UpdateInterval: time.Minute * 1,

		ccu: ccu,

		devices: make(map[string]*Device),
	}

	err = ccu.UpdateDevices(true)
	if err != nil {
		return b, errors.New(assets.L.Get("CCU not reachable at %s", address))
	}
	return b, nil
}

// start handling of CCU
func (c *CCU) start(bus core.Bus) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	if c.tickerRunning {
		// already running
		return
	}

	// start event handling of CCU
	err := c.ccu.Start()
	if err != nil {
		zap.L().Error(err.Error())
	}

	// start device tracker
	go c.update(bus)
	c.ticker = time.NewTicker(c.UpdateInterval)
	c.tickerRunning = true
	go func() {
		for range c.ticker.C {
			c.update(bus)
		}
	}()
}

// stop handling of CCU
func (c *CCU) stop() {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	if !c.tickerRunning {
		// not running
		return
	}

	// stop CCU event handling
	err := c.ccu.Stop()
	if err != nil {
		zap.L().Error(err.Error())
	}

	// stop the ticker for the device tracker
	c.ticker.Stop()
	c.tickerRunning = false
}

// update known devices
func (c *CCU) update(bus core.Bus) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	knownDevices := make(map[string]bool)
	devices, err := c.ccu.GetDevices()
	if err != nil {
		zap.L().Error(err.Error())
		return
	}
	for _, device := range devices {
		// ignore devices
		if device.Type == "MAINTENANCE" || // maintenance ones
			!device.HasValues() || // without values
			c.Exclude.MatchString(device.GetName()) { // that match exclude regex
			continue
		}

		_, ok := c.devices[device.Address]
		if !ok {
			dev, err := NewDevice(device)
			if err != nil {
				zap.L().Error(err.Error())
				continue
			}

			// skip "empty" devices
			if dev == nil {
				continue
			}

			// add item
			err = core.ItemAdd(bus, dev)
			if err != nil {
				zap.L().Error(err.Error())
				continue
			}
			c.devices[device.Address] = dev
		}
		knownDevices[device.Address] = true
	}

	// remove devices
	for a, dev := range c.devices {
		if knownDevices[a] {
			continue
		}

		// remove non existing device
		err := core.ItemRemove(bus, dev)
		if err != nil {
			zap.L().Error(err.Error())
		}
		delete(c.devices, a)
	}
}
