// Copyright 2020 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package weather_underground

import (
	"github.com/leonelquinteros/gotext"
	"github.com/spf13/cast"

	"gitlab.com/go4matic/go4matic/pkg/api"
)

// Values defines known values
type Value struct {
	Description string
	Type        api.Type

	SourceValue string
	CalcFunc    func(interface{}) interface{}
}

// values supported by plugin
var values = map[string]Value{
	"absbaromin": {
		Description: gotext.Get("Absolute barometric pressure (in inch Hg)"),
		Type:        api.Float32,
	},
	"baromin": {
		Description: gotext.Get("Relative barometric pressure (in inch Hg)"),
		Type:        api.Float32,
	},
	"dailyrainin": {
		Description: gotext.Get("Rain so far today (in inch)"),
		Type:        api.Float32,
	},
	"dewptf": {
		Description: gotext.Get("Outdoor dewpoint (in °F)"),
		Type:        api.Float32,
	},
	"humidity": {
		Description: gotext.Get("Outdoor humidity (in %)"),
		Type:        api.Uint8,
	},
	"indoorhumidity": {
		Description: gotext.Get("Indoor humidity (in %)"),
		Type:        api.Uint8,
	},
	"indoortempf": {
		Description: gotext.Get("Indoor temperature (in °F)"),
		Type:        api.Float32,
	},
	"monthlyrainin": {
		Description: gotext.Get("Rain over the past month (in inch)"),
		Type:        api.Float32,
	},
	"rainin": {
		Description: gotext.Get("Rain over the past hour (in inch)"),
		Type:        api.Float32,
	},
	"soilmoisture": {
		Description: gotext.Get("Soil moisture (in %)"),
		Type:        api.Uint8,
	},
	"soiltempf": {
		Description: gotext.Get("Soil temperature (in °F)"),
		Type:        api.Float32,
	},
	"solarradiation": {
		Description: gotext.Get("Solar radiation (W/m^2)"),
		Type:        api.Float32,
	},
	"softwaretype": {
		Description: gotext.Get("Name of weather software"),
		Type:        api.String,
	},
	"tempf": {
		Description: gotext.Get("Outdoor temperature (in °F)"),
		Type:        api.Float32,
	},
	"uv": {
		Description: gotext.Get("UV index"),
		Type:        api.Uint8,
	},
	"weeklyrainin": {
		Description: gotext.Get("Rain over the past week (in inch)"),
		Type:        api.Float32,
	},
	"windchillf": {
		Description: gotext.Get("Windchill temperature (in °F)"),
		Type:        api.Float32,
	},
	"winddir": {
		Description: gotext.Get("Wind direction (0-360)"),
		Type:        api.Uint16,
	},
	"windgustmph": {
		Description: gotext.Get("Current wind gust (in mph)"),
		Type:        api.Float32,
	},
	"windspeedmph": {
		Description: gotext.Get("Current wind speed (in mph)"),
		Type:        api.Float32,
	},

	// calculated values
	"absbaromhpa": {
		Description: gotext.Get("Absolute barometric pressure (in hPa)"),
		Type:        api.Float32,
		SourceValue: "absbaromin",
		CalcFunc:    convertInHgToHPa,
	},
	"baromhpa": {
		Description: gotext.Get("Relative barometric pressure (in hPa)"),
		Type:        api.Float32,
		SourceValue: "baromin",
		CalcFunc:    convertInHgToHPa,
	},
	"dailyrainmm": {
		Description: gotext.Get("Rain so far today (in mm)"),
		Type:        api.Float32,
		SourceValue: "dailyrainin",
		CalcFunc:    convertInchToMm,
	},
	"dewptc": {
		Description: gotext.Get("Outdoor dewpoint (in °C)"),
		Type:        api.Float32,
		SourceValue: "dewptf",
		CalcFunc:    convertFToC,
	},
	"indoortempc": {
		Description: gotext.Get("Indoor temperature (in °C)"),
		Type:        api.Float32,
		SourceValue: "indoortempf",
		CalcFunc:    convertFToC,
	},
	"monthlyrainmm": {
		Description: gotext.Get("Rain over the past month (in mm)"),
		Type:        api.Float32,
		SourceValue: "monthlyrainin",
		CalcFunc:    convertInchToMm,
	},
	"rainmm": {
		Description: gotext.Get("Rain over the past hour (in mm)"),
		Type:        api.Float32,
		SourceValue: "rainin",
		CalcFunc:    convertInchToMm,
	},
	"soiltempc": {
		Description: gotext.Get("Soil temperature (in °C)"),
		Type:        api.Float32,
		SourceValue: "soiltempf",
		CalcFunc:    convertFToC,
	},
	"tempc": {
		Description: gotext.Get("Outdoor temperature (in °C)"),
		Type:        api.Float32,
		SourceValue: "tempf",
		CalcFunc:    convertFToC,
	},
	"weeklyrainmm": {
		Description: gotext.Get("Rain over the past week (in mm)"),
		Type:        api.Float32,
		SourceValue: "weeklyrainin",
		CalcFunc:    convertInchToMm,
	},
	"windchillc": {
		Description: gotext.Get("Windchill temperature (in °C)"),
		Type:        api.Float32,
		SourceValue: "windchillf",
		CalcFunc:    convertFToC,
	},
	"windgustkmph": {
		Description: gotext.Get("Current wind gust (in km/h)"),
		Type:        api.Float32,
		SourceValue: "windgustmph",
		CalcFunc:    convertMphToKmph,
	},
	"windspeedkmph": {
		Description: gotext.Get("Current wind speed (in km/h)"),
		Type:        api.Float32,
		SourceValue: "windspeedmph",
		CalcFunc:    convertMphToKmph,
	},

	// values to ignore
	"rtfreq":   {},
	"realtime": {},
}

// convertValues from HTTP request to item values
func convertValues(raw map[string]interface{}) map[string]interface{} {
	data := make(map[string]interface{}, len(values))

	for key, value := range raw {
		valueDef, ok := values[key]

		// fallback to original value if unknown
		if !ok {
			data[key] = value
			continue
		}

		// empty definition -> ignore value
		if valueDef.Type == nil {
			continue
		}

		// convert value type
		data[key], _ = valueDef.Type.Convert(value)

		// calculated values
		for k, valueDef := range values {
			if valueDef.SourceValue == key && valueDef.CalcFunc != nil {
				data[k] = valueDef.CalcFunc(value)
			}
		}
	}
	return data
}

// convertFToC converts values from °F to °C
func convertFToC(f interface{}) interface{} {
	return (cast.ToFloat32(f) - 32.0) * (5.0 / 9.0)
}

// convertInchToMm converts values from inch to mm
func convertInchToMm(f interface{}) interface{} {
	return cast.ToFloat32(f) * 25.4
}

// convertMphToKmph converts values from mph to km/h
func convertMphToKmph(f interface{}) interface{} {
	return cast.ToFloat32(f) * 1.609344
}

// convertInHgToHPa converts values from inHg to hPa
func convertInHgToHPa(f interface{}) interface{} {
	return cast.ToFloat32(f) * 33.86389
}
