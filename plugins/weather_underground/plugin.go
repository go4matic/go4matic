// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package weather_underground

import (
	"errors"
	"strings"
	"sync"

	"github.com/spf13/cast"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// Plugin that implements weather underground API
type Plugin struct {
	bus core.Bus

	// list of configured stations
	stations map[string]*Station
	mutex    sync.RWMutex
}

// ID of plugin
func (p *Plugin) ID() string {
	return "weather_underground"
}

// Version information of plugin
func (p *Plugin) Version() string {
	return "0.8.0"
}

// Init plugin
func (p *Plugin) Init(b core.Bus) error {
	p.bus = b
	return nil
}

// Config returns the definition of the config interface
func (p *Plugin) Config() *core.PluginConfigDef {
	return &core.PluginConfigDef{
		Entries: core.PluginConfigEntryDefMap{
			"station": {
				Order: 1,
				Config: api.FieldMap{
					"password": {
						Type:         api.String,
						Description:  assets.L.Get("Weather station key or password"),
						DefaultValue: "",
					},
				},
				LoadEntryFunc:   p.loadStations,
				AddEntryFunc:    p.addStation,
				RemoveEntryFunc: p.removeStation,
			},
		},
	}
}

// loadStations from configuration
func (p *Plugin) loadStations(entries map[string]map[string]interface{}) error {
	p.stations = make(map[string]*Station, len(entries))
	for k, v := range entries {
		err := p.addStation(k, cast.ToStringMap(v))
		if err != nil {
			return err
		}
	}
	return nil
}

// addStation with the given key
func (p *Plugin) addStation(key string, config map[string]interface{}) error {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	station := &Station{
		id:       key,
		password: cast.ToString(config["password"]),
	}

	err := core.ItemAdd(p.bus, station)
	if err != nil {
		return errors.New(assets.L.Get("failed to add station item %v", err))
	}

	p.stations[key] = station
	return nil
}

// removeStation with the given key
func (p *Plugin) removeStation(key string) error {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	err := core.ItemRemove(p.bus, p.stations[key])
	if err != nil {
		return errors.New(assets.L.Get("failed to remove station item %v", err))
	}

	delete(p.stations, key)
	return nil
}

// APIEndpoints of this plugin
func (p *Plugin) APIEndpoints() []core.PathEntry {
	return []core.PathEntry{{
		Def: api.ExtendedHandler{
			Description: "Update weather station",
		},
		Path:    "update",
		Handler: p.update,
	}}
}

// update weather station values
func (p *Plugin) update(ctx core.HTTPContext) {
	// https://support.weather.com/s/article/PWS-Upload-Protocol?language=en_US
	p.mutex.RLock()
	defer p.mutex.RUnlock()

	params := ctx.Request().URL.Query()

	// check if station is valid
	id := params.Get("ID")
	action := params.Get("action")
	if id == "" || action != "updateraw" {
		ctx.Status(400)
		ctx.Abort()
		return
	}

	// get station from ID
	station, ok := p.stations[id]
	if !ok {
		ctx.Status(404)
		ctx.Abort()
		return
	}

	// check password
	pw := params.Get("PASSWORD")
	if pw != "" && pw == station.password {
		ctx.Status(403)
		ctx.Abort()
		return
	}

	// convert parameters to map
	data := make(map[string]interface{}, len(params))
	for rawKey := range params {
		key := strings.ToLower(rawKey)
		if key == "id" ||
			key == "password" ||
			key == "dateutc" ||
			key == "action" {
			continue
		}
		data[key] = params.Get(rawKey)
	}

	station.update(data)
}
