// Copyright 2020 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package weather_underground

import (
	"fmt"
	"strings"
	"sync"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
)

// Station represents one weather station
type Station struct {
	id       string
	password string

	values map[string]interface{}

	mutex   sync.RWMutex
	trigger func(action string, data map[string]interface{})
}

// ID of item
func (s *Station) ID() string {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	return fmt.Sprintf("plugin.weather_underground.%s",
		strings.Replace(s.id, " ", "", -1))
}

// Name of item
func (s *Station) Name() string {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	return s.id
}

// GetValueTypes of item
func (s *Station) GetValueTypes() api.FieldMap {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	fields := make(api.FieldMap, len(s.values))
	for key, value := range s.values {
		field := api.Field{
			Type: api.TypeFromValue(value),
		}
		if v, ok := values[key]; ok {
			field.Description = v.Description
		}
		fields[key] = field
	}
	return fields
}

// Events returns all events that can be emitted
func (s *Station) Events() map[string]api.SimpleHandler {
	return map[string]api.SimpleHandler{
		"changed": {
			Description: assets.L.Get("Emitted if a value changed"),
			Parameters:  s.GetValueTypes(),
		},
	}
}

// SetEventTrigger for item event
func (s *Station) SetEventTrigger(trigger func(action string, data map[string]interface{})) {
	s.trigger = trigger
}

// GetValues of sensor
func (s *Station) GetValues() map[string]interface{} {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	return s.values
}

// update station values
func (s *Station) update(data map[string]interface{}) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	s.values = convertValues(data)
	s.trigger("changed", s.values)
}
