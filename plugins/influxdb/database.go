// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package influxdb

import (
	"errors"
	"time"

	"github.com/influxdata/influxdb1-client/v2"

	"gitlab.com/go4matic/go4matic/assets"
)

// Database connects to influxdb and log values
type Database struct {
	key string

	address  string
	username string
	password string
	database string

	db client.Client
}

// Connect creates connection to database
func (i *Database) Connect() error {
	var err error
	i.db, err = client.NewHTTPClient(client.HTTPConfig{
		Addr:     i.address,
		Username: i.username,
		Password: i.password,
		Timeout:  time.Millisecond * 100,
	})
	return err
}

// ID identifies item
func (i *Database) ID() string {
	return "plugin.influxdb.database." + i.key
}

// Name of item
func (i *Database) Name() string {
	return "InfluxDB " + i.key
}

// LogValues to persistent storage
func (i *Database) LogValues(id string, values map[string]interface{}) error {
	bp, _ := client.NewBatchPoints(client.BatchPointsConfig{
		Database: i.database,
	})

	pt, err := client.NewPoint(id, nil, values, time.Now())
	if err != nil {
		return errors.New(assets.L.Get("influx: failed to create point %v", err))
	}
	bp.AddPoint(pt)

	err = i.db.Write(bp)
	if err != nil {
		return errors.New(assets.L.Get("influx: failed to write point %v", err))
	}
	return nil
}
