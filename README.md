[![pipeline status](https://gitlab.com/go4matic/go4matic/badges/master/pipeline.svg)](https://gitlab.com/go4matic/go4matic/pipelines)
[![coverage report](https://gitlab.com/go4matic/go4matic/badges/master/coverage.svg)](https://gitlab.com/go4matic/go4matic)
[![godoc](https://godoc.org/gitlab.com/go4matic/go4matic?status.svg)](https://godoc.org/gitlab.com/go4matic/go4matic)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/go4matic/go4matic)](https://goreportcard.com/report/gitlab.com/go4matic/go4matic)

# Go4Matic

Go4Matic is a home automation system that lets you control and connect various
devices.


## Features

*  Single binary - no dependencies
*  Simple setup - no complicated configuration files
*  Responsive web interface and dashboard
*  Expandable via plugins
*  Simple REST API to integrate in other applications
*  LUA script engine with optional graphical editor
*  Distributed setup - connect multiple instances together (*)

> Features marked with (*) are not finished/implemented jet


## Plugins

Go4Matic provides interface to extend the functionality and add integration
to the application.

You can find a list of plugins that are maintained and provided with Go4Matic
[here](plugins/README.md) as static plugins.

Additional it is possible load dynamic plugins at runtime. This can be 
useful to test a new version of a plugin or for plugins that should not be 
public.

> **Note:** Dynamic plugins are only supported on Linux.


## Build

To build the core application simply run the following command in the 
base directory of the repository.

```bash
go generate ./...
go build -v -o go4matic -tags 'prod' ./cmd/go4matic
```
> `go generate ./...` will also load web dependencies from npm

This will build the core application and store it as `go4matic` in the current 
directory.

> On Windows you should use `go build -v -o go4matic.exe ./cmd/go4matic` to
> get a executable application.

To build the application with dynamic plugin support add the tag `dynplugin`
to the build command:

```bash
go build -v -o go4matic -tags 'prod dynplugin' ./cmd/go4matic
```

## Run

Simply start the application with this command:
```bash
./go4matic
```

This will start the application in the current console and you can access the 
web interface via http://127.0.0.1:8080/.

*TODO: how to start as service*


## Dependencies

The currently used go dependencies can be found in the [go.mod](go.mod).

The web interface is mainly based on [AdminLTE](https://adminlte.io/).
Versions of used dependencies can be found in the [package.json](package.json).

For additional dependencies see the different [Plugins](plugins/README.md).

The Raspberry image is based on [alpine linux](https://alpinelinux.org/) and 
build with the [raspi-alpine-builder](https://gitlab.com/bboehmke/raspi-alpine-builder).
