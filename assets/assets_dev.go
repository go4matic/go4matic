// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// +build !prod

package assets

import (
	"net/http"
	"path"
	"runtime"

	"github.com/shurcooL/httpfs/union"
)

// get path to base directory
var _, file, _, _ = runtime.Caller(0)
var staticBase = path.Join(path.Dir(file), "../node_modules")

// define assets
var staticAssets = map[string]http.FileSystem{
	"/admin-lte": &staticAsset{
		Base: path.Join(staticBase, "admin-lte/dist"),
		Filter: []string{
			"css/adminlte.min.css",
			"js/adminlte.min.js",
		},
	},
	"/blockly": &staticAsset{
		Base: path.Join(staticBase, "blockly"),
		Filter: []string{
			"media/*",
			"msg/*.js",
			"blockly.min.js",
			"blocks.js",
			"lua.js",
		},
	},
	"/bootstrap": &staticAsset{
		Base: path.Join(staticBase, "bootstrap/dist"),
		Filter: []string{
			"css/bootstrap.min.css",
			"js/bootstrap.min.js",
		},
	},
	"/clockpicker": &staticAsset{
		Base: path.Join(staticBase, "clockpicker/dist"),
		Filter: []string{
			"bootstrap-clockpicker.min.css",
			"bootstrap-clockpicker.min.js",
		},
	},
	"/font-awesome": &staticAsset{
		Base: path.Join(staticBase, "@fortawesome/fontawesome-free"),
		Filter: []string{
			"css/all.min.css",
			"webfonts/*",
		},
	},
	"/jquery": &staticAsset{
		Base: path.Join(staticBase, "jquery/dist"),
		Filter: []string{
			"jquery.min.js",
		},
	},
	"/monaco-editor": &staticAsset{
		Base: path.Join(staticBase, "monaco-editor/min"),
		Filter: []string{
			"vs/base/worker/*",
			"vs/basic-languages/lua/*",
			"vs/editor/*",
			"vs/loader.js",
		},
	},
	"/select2": &staticAsset{
		Base: path.Join(staticBase, "select2/dist"),
		Filter: []string{
			"css/select2.min.css",
			"js/select2.full.min.js",
			"js/i18n/*.js",
		},
	},
	"/select2-bootstrap4-theme": &staticAsset{
		Base: path.Join(staticBase, "@ttskch/select2-bootstrap4-theme/dist"),
		Filter: []string{
			"select2-bootstrap4.min.css",
		},
	},
	"/datatables.net": &staticAsset{
		Base: path.Join(staticBase, "datatables.net"),
		Filter: []string{
			"js/jquery.dataTables.min.js",
		},
	},
	"/datatables.net-bs4": &staticAsset{
		Base: path.Join(staticBase, "datatables.net-bs4"),
		Filter: []string{
			"css/dataTables.bootstrap4.min.css",
			"js/dataTables.bootstrap4.min.js",
		},
	},
}

// Assets file system
var Assets = union.New(map[string]http.FileSystem{
	"/static":    union.New(staticAssets),
	"/templates": http.Dir(path.Join(path.Dir(file), "templates")),
	"/locales":   http.Dir(path.Join(path.Dir(file), "locales")),
})

// DevMode is true if build in dev mode
var DevMode = true
