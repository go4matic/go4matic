// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package assets

import (
	"net/http"
	"strings"
)

//go:generate go run ./asset_generator.go

type subFileSystem struct {
	subDir string
}

// Static returns filesystem for static files
func Static() http.FileSystem {
	return &subFileSystem{
		subDir: "static",
	}
}

// Templates returns filesystem for template files
func Templates() http.FileSystem {
	return &subFileSystem{
		subDir: "templates",
	}
}

// Open file object
func (f *subFileSystem) Open(name string) (http.File, error) {
	if !strings.HasPrefix(name, "/") {
		name = "/" + name
	}
	return Assets.Open("/" + f.subDir + name)
}
