// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// +build !prod

package assets

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"
)

// staticAsset file system
type staticAsset struct {
	Base   string
	Filter []string
}

// match checks if the path match a filer
func (a *staticAsset) match(path string) bool {
	for _, f := range a.Filter {
		// filter starts with path -> also match
		if strings.HasPrefix(f, path) {
			return true
		}
		m, err := filepath.Match(f, path)
		if err == nil && m {
			return true
		}
	}
	return false
}

// Open file
func (a *staticAsset) Open(name string) (http.File, error) {
	// check if file exists
	if !a.match(name[1:]) {
		return nil, &os.PathError{Op: "open", Path: name, Err: os.ErrNotExist}
	}

	// open file and return if it is not a directory
	f, err := os.Open(a.Base + name)
	if err != nil {
		return nil, err
	}

	fi, err := f.Stat()
	if err != nil {
		f.Close()
		return nil, err
	}

	if !fi.IsDir() {
		return f, nil
	}
	defer f.Close()

	// read directory content
	fis, err := f.Readdir(0)
	if err != nil {
		return nil, err
	}

	// filter directory content
	var entries []os.FileInfo
	for _, fi := range fis {
		if a.match(path.Join(name[1:], fi.Name())) {
			entries = append(entries, fi)
		}
	}

	return &dir{
		name:    fi.Name(),
		entries: entries,
		modTime: fi.ModTime(),
	}, nil
}

// dir is an opened dir instance.
//   from github.com/shurcooL/httpfs/filter
type dir struct {
	name    string
	modTime time.Time
	entries []os.FileInfo
	pos     int // Position within entries for Seek and Readdir.
}

// Read not supported for directories
func (d *dir) Read([]byte) (int, error) {
	return 0, fmt.Errorf("cannot Read from directory %s", d.name)
}

// Close virtual directory
func (d *dir) Close() error { return nil }

// Stat of virtual directory
func (d *dir) Stat() (os.FileInfo, error) { return d, nil }

// Name of virtual directory
func (d *dir) Name() string { return d.name }

// Size of virtual directory
func (d *dir) Size() int64 { return 0 }

// Mode of virtual directory
func (d *dir) Mode() os.FileMode { return 0755 | os.ModeDir }

// ModTime of virtual directory
func (d *dir) ModTime() time.Time { return d.modTime }

// IsDir of virtual directory
func (d *dir) IsDir() bool { return true }

// Sys of virtual directory
func (d *dir) Sys() interface{} { return nil }

// Seek of virtual directory
func (d *dir) Seek(offset int64, whence int) (int64, error) {
	if offset == 0 && whence == io.SeekStart {
		d.pos = 0
		return 0, nil
	}
	return 0, fmt.Errorf("unsupported Seek in directory %s", d.name)
}

// Readdir of virtual directory
func (d *dir) Readdir(count int) ([]os.FileInfo, error) {
	if d.pos >= len(d.entries) && count > 0 {
		return nil, io.EOF
	}
	if count <= 0 || count > len(d.entries)-d.pos {
		count = len(d.entries) - d.pos
	}
	e := d.entries[d.pos : d.pos+count]
	d.pos += count
	return e, nil
}
