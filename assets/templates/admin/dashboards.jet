{{extends "base.jet"}}
{{import "utils/modal.jet"}}

{{block body()}}
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="mt-3 card card-default">
            <div class="card-header">
                <h3>{{Locale.Get("Dashboards")}}</h3>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-hover">
                    <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>{{Locale.Get("Name")}}</th>
                        <th>{{Locale.Get("Hide")}}</th>
                        <th>{{Locale.Get("Icon")}}</th>
                        <th>{{Locale.Get("Groups")}}</th>
                        <th>
                            <div class="btn-group btn-group-sm">
                                <button type="button" class="btn btn-success"
                                        data-toggle="modal" data-target="#addModal">
                                    <i class="fa fa-plus"></i>
                                    {{Locale.Get("Widget")}}
                                </button>
                            </div>
                        </th>
                    </tr>
                    {{range idx, dashboard := dashboards}}
                    <tr>
                        <td>{{idx}}</td>
                        <td><a href="./dashboards/{{dashboard.Name}}">{{dashboard.Name}}</a></td>
                        <td>{{dashboard.Hide}}</td>
                        <td>
                            {{dashboard.Icon}}
                            {{if dashboard.Icon != ""}}
                                <i class="fa fa-{{dashboard.Icon}}"></i>
                            {{end}}
                        </td>
                        <td>{{len(dashboard.Groups)}}</td>
                        <td>
                            <div class="btn-group btn-group-sm">
                                <a type="button" class="btn btn-warning" href="./dashboards/{{dashboard.Name}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <button type="button" class="btn btn-danger"
                                        data-toggle="modal" data-target="#removeModal"
                                        data-name="{{ dashboard.Name }}">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    {{end}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


{{yield formModal(modalId="addModal", title="Add Dashboard", submit="Add") content}}
    <div class="form-group">
        <label for="name" class="col-form-label">{{Locale.Get("Name:")}}</label>
        <input type="text" class="form-control" id="name" name="name"/>
    </div>
    <div class="form-group">
        <label for="hide" class="col-form-label">{{Locale.Get("Hide:")}}</label>
        <input type="checkbox" id="hide" name="hide"/>
    </div>
    <div class="form-group">
        <label for="icon" class="col-form-label">{{Locale.Get("Icon:")}}</label>
        <input type="text" class="form-control" id="icon" name="icon"/>
    </div>
    <div class="form-group">
        <label for="max_columns" class="col-form-label">{{Locale.Get("Max Columns:")}}</label>
        <input type="number" class="form-control" id="max_columns" name="max_columns" value="3"/>
    </div>
    <input type="hidden" name="action" value="add"/>
{{end}}

{{yield formModal(modalId="removeModal", submit=Locale.Get("Remove"), submitType="danger") content}}
    <p><b>{{Locale.Get("This action cannot be undone.")}}</b></p>
    <input type="hidden" name="name" id="remove_dashboard_name"/>
    <input type="hidden" name="action" value="remove"/>
{{end}}

<script>
    $("#removeModal").on("show.bs.modal", function (event) {
        let button = $(event.relatedTarget);

        let modal = $(this);
        modal.find("#removeModal_label").text('{{Locale.Get("Delete Dashboard {0}?")}}'.format(button.data("name")));
        modal.find("#remove_dashboard_name").val(button.data("name"));
    });
</script>
{{end}}