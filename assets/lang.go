// Copyright 2020 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package assets

import (
	"bytes"
	"io"
	"path"

	"github.com/leonelquinteros/gotext"
	"go.uber.org/zap"
)

// L contains loaded locale
var L = new(gotext.Locale)

// LoadLocale from translation files
func LoadLocale(lang string) {
	// default
	if lang == "en" {
		L.AddTranslator("default", new(gotext.Po))
		return
	}

	// open translation file
	file, err := Assets.Open(path.Join("/locales", lang, "default.po"))
	if err != nil {
		zap.S().Errorf("Failed to load locales for %s -> fallback to en", lang)
		LoadLocale("en")
		return
	}
	defer file.Close()

	// load file as PO file and add to locale
	buf := bytes.NewBuffer(nil)
	io.Copy(buf, file)
	file.Close()

	po := new(gotext.Po)
	po.Parse(buf.Bytes())
	L.AddTranslator("default", po)
}

// SetLanguage for locale to use
func SetLanguage(lang string) {
	LoadLocale(lang)
}

// ListLanguages available for translation
func ListLanguages() map[interface{}]string {
	data := map[interface{}]string{
		"en": "English",
	}

	// load directory for selected locale
	file, err := Assets.Open("/locales")
	if err != nil {
		zap.S().Error("Failed to list locales")
		return data
	}
	defer file.Close()

	// search for existing translation files
	files, err := file.Readdir(0)
	if err != nil {
		zap.S().Error("Failed to list locales")
		return data
	}

	for _, fileInfo := range files {
		if fileInfo.IsDir() {
			// TODO -> improve language name
			data[fileInfo.Name()] = fileInfo.Name()
		}
	}
	return data
}
