// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"io/ioutil"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestConfigService_LoadAndSave(t *testing.T) {
	ass := assert.New(t)

	service := NewConfigService("not_existing.yml")
	ass.Nil(service.data)
	ass.Equal("not_existing.yml", service.configFile)

	tempFile, err := ioutil.TempFile("", "*.yml")
	ass.NoError(err)
	defer ass.NoError(os.Remove(tempFile.Name()))

	service = NewConfigService(tempFile.Name())
	ass.NoError(service.Init(nil))

	ass.NoError(ioutil.WriteFile(tempFile.Name(), []byte("invalid"), 0777))
	service = NewConfigService(tempFile.Name())
	err = service.Init(nil)
	ass.Error(err)
	ass.True(strings.HasPrefix(err.Error(), "read invalid config"))

	ass.NoError(ioutil.WriteFile(tempFile.Name(), []byte("aaa: 42\nbbb: \"test\""), 0777))
	service = NewConfigService(tempFile.Name())
	err = service.Init(nil)
	ass.NoError(err)
	ass.Len(service.data, 2)
	ass.Equal(42, service.data["aaa"])
	ass.Equal("test", service.data["bbb"])

	ass.NoError(ioutil.WriteFile(tempFile.Name(), []byte{}, 0777))
	service = NewConfigService(tempFile.Name())
	err = service.Init(nil)
	ass.NoError(err)

	service.data = map[string]interface{}{"aaa": 42}
	service.save()

	bytes, _ := ioutil.ReadFile(tempFile.Name())
	ass.Equal("aaa: 42\n", string(bytes))
}

func TestConfigService_Key(t *testing.T) {
	service := ConfigService{}
	assert.Equal(t, "core.config", service.Key())
}

func TestConfigService_Handlers(t *testing.T) {
	ass := assert.New(t)

	service := ConfigService{}
	ass.Len(service.Handlers(), 5)
	ass.NotNil(service.Handlers()["get"])
	ass.NotNil(service.Handlers()["set"])
	ass.NotNil(service.Handlers()["remove"])
	ass.NotNil(service.Handlers()["global_set"])
	ass.NotNil(service.Handlers()["global_get"])
}

func TestConfigService_configGet(t *testing.T) {
	ass := assert.New(t)

	service := ConfigService{}
	service.data = map[string]interface{}{
		"aaa.bbb": 42,
	}

	value, err := service.configGet(map[string]interface{}{"key": "aaa.bbb"})
	ass.NoError(err)
	ass.Equal(42, value["value"])

	value, err = service.configGet(map[string]interface{}{"key": "aaa.ccc"})
	ass.NoError(err)
	ass.Nil(value["value"])

	value, err = service.configGet(map[string]interface{}{"key": "aaa.bbb.ccc"})
	ass.NoError(err)
	ass.Nil(value["value"])
}

func TestConfigService_configSet(t *testing.T) {
	ass := assert.New(t)

	tempFile, err := ioutil.TempFile("", "*.yml")
	ass.NoError(err)
	defer ass.NoError(os.Remove(tempFile.Name()))

	service := NewConfigService(tempFile.Name())
	err = service.Init(nil)
	ass.NoError(err)

	value, err := service.configSet(map[string]interface{}{
		"key":   "aaa.bbb",
		"value": 42,
	})
	ass.Nil(err)
	ass.Nil(value)

	time.Sleep(time.Millisecond * 10)
	bytes, _ := ioutil.ReadFile(tempFile.Name())
	ass.Equal("aaa.bbb: 42\n", string(bytes))

	// check if config file is only update on changes
	info1, _ := os.Stat(tempFile.Name())
	value, err = service.configSet(map[string]interface{}{
		"key":   "aaa.bbb",
		"value": 42,
	})
	ass.Nil(err)
	ass.Nil(value)

	time.Sleep(time.Millisecond * 10)
	info2, _ := os.Stat(tempFile.Name())
	ass.Equal(info1.ModTime(), info2.ModTime())
}

func TestConfigService_configRemove(t *testing.T) {
	ass := assert.New(t)

	tempFile, err := ioutil.TempFile("", "*.yml")
	ass.NoError(err)
	defer ass.NoError(os.Remove(tempFile.Name()))

	service := NewConfigService(tempFile.Name())
	err = service.Init(nil)
	ass.NoError(err)
	service.data = map[string]interface{}{
		"aaa.bbb": 42,
	}

	value, err := service.configRemove(map[string]interface{}{
		"key": "aaa.bbb",
	})
	ass.Nil(err)
	ass.Nil(value)

	time.Sleep(time.Millisecond * 10)
	bytes, _ := ioutil.ReadFile(tempFile.Name())
	ass.Equal("{}\n", string(bytes))
}
