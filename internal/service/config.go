// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"bytes"
	"errors"
	"io/ioutil"
	"os"
	"path"
	"sync"

	"github.com/spf13/cast"
	"go.uber.org/zap"
	"gopkg.in/yaml.v2"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// ConfigService stores persistent configurations
type ConfigService struct {
	mutex sync.RWMutex

	// storage location of config file
	configFile string

	// data from config file
	yamlData []byte

	// config content
	data map[string]interface{}
}

// NewConfigService loads conf if exists and creates service
func NewConfigService(configFile string) *ConfigService {
	return &ConfigService{
		configFile: configFile,
	}
}

// Key that identifies this service
func (s *ConfigService) Key() string {
	return "core.config"
}

// Init load the service
func (s *ConfigService) Init(bus core.Bus) error {
	// load config file if exist
	if _, err := os.Stat(s.configFile); err == nil {
		// read file
		var err error
		s.yamlData, err = ioutil.ReadFile(s.configFile)
		if err != nil {
			return errors.New(assets.L.Get("failed to read config %s", err))
		}

		// parse data
		err = yaml.Unmarshal(s.yamlData, &s.data)
		if err != nil {
			return errors.New(assets.L.Get("read invalid config %s", err))
		}
	}

	if s.data == nil {
		s.data = make(map[string]interface{})
	}

	// trigger initial config load
	s.triggerGlobalConfigChange(
		core.Config().Load(cast.ToStringMap(s.data["global"])))
	return nil
}

// Handlers for this service
func (s *ConfigService) Handlers() map[string]core.RequestHandler {
	return map[string]core.RequestHandler{
		"get": {
			Func: s.configGet,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Read a config value"),
				Parameters: api.FieldMap{
					"key": {
						Type:        api.String,
						Description: assets.L.Get("Identifier of config value"),
						Required:    true,
					},
				},
				Response: api.FieldMap{
					"value": {
						Description: assets.L.Get("Value of the config or nil if not set"),
						Required:    true,
					},
				},
			},
		},
		"set": {
			Func: s.configSet,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Write config value"),
				Parameters: api.FieldMap{
					"key": {
						Type:        api.String,
						Description: assets.L.Get("Identifier of config value"),
						Required:    true,
					},
					"value": {
						Description: assets.L.Get("Value to set for config"),
						Required:    true,
					},
				},
			},
		},
		"remove": {
			Func: s.configSet,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Remove config value"),
				Parameters: api.FieldMap{
					"key": {
						Type:        api.String,
						Description: assets.L.Get("Identifier of config value"),
						Required:    true,
					},
				},
			},
		},
		"global_set": {
			Func: s.configGlobalSet,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Set global config"),
				Parameters:  core.Config(),
			},
		},
		"global_get": {
			Func: s.configGlobalGet,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Get global config"),
				Parameters:  core.Config(),
			},
		},
	}
}

// save config to file
func (s *ConfigService) save() {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	// convert data
	data, err := yaml.Marshal(s.data)
	if err != nil {
		zap.L().Error(assets.L.Get("failed to marshal config: %s", err))
	}

	// no change ?
	if bytes.Equal(s.yamlData, data) {
		return
	}

	// create directory if not exist
	err = os.MkdirAll(path.Dir(s.configFile), os.ModePerm)
	if err != nil {
		zap.L().Error(assets.L.Get("failed to config dir %s: %s", s.configFile, err))
		s.yamlData = data
		return
	}

	// write file
	err = ioutil.WriteFile(s.configFile, data, 0644)
	if err != nil {
		zap.L().Error(assets.L.Get("failed to write %s: %s", s.configFile, err))
	}
	s.yamlData = data
}

// configGet reads a config value
func (s *ConfigService) configGet(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	return map[string]interface{}{
		"value": s.data[cast.ToString(data["key"])],
	}, nil
}

// configSet writes config value
func (s *ConfigService) configSet(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	s.data[cast.ToString(data["key"])] = data["value"]
	go s.save()
	return nil, nil
}

// configRemove value
func (s *ConfigService) configRemove(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	delete(s.data, cast.ToString(data["key"]))
	go s.save()
	return nil, nil
}

// configGlobalGet returns the global configuration
func (s *ConfigService) configGlobalGet(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	return core.Config().Load(cast.ToStringMap(s.data["global"])), nil
}

// configGlobalSet writes the global configuration
func (s *ConfigService) configGlobalSet(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	cfg, err := core.Config().Validate(data)
	if err != nil {
		return nil, err
	}

	s.data["global"] = cfg
	go s.triggerGlobalConfigChange(cfg)
	go s.save()
	return nil, nil
}

// triggerGlobalConfigChange forwards changes to global instances
func (s *ConfigService) triggerGlobalConfigChange(config map[string]interface{}) {
	assets.SetLanguage(cast.ToString(config["language"]))
}
