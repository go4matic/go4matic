// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"errors"
	"strings"
	"sync"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// ItemService manages all items of application instance
type ItemService struct {
	// map of registered items
	items map[string]core.Item

	// mutex for items
	mutex sync.RWMutex

	// trigger functions for events
	trigger core.EventTrigger
}

// NewItemService creates the item service instance
func NewItemService() *ItemService {
	return &ItemService{
		items: make(map[string]core.Item),
	}
}

// Key that identifies this service
func (s *ItemService) Key() string {
	return "core.items"
}

// Events returns all events that can be emitted
func (s *ItemService) Events() map[string]api.SimpleHandler {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	events := make(map[string]api.SimpleHandler)
	for id, item := range s.items {
		eventItem, ok := core.GetEventItem(item)
		if !ok {
			continue
		}

		for eventKey, eventDefinition := range eventItem.Events() {
			events[id+"#"+eventKey] = eventDefinition
		}
	}
	return events
}

// SetEventTrigger for service
func (s *ItemService) SetEventTrigger(trigger core.EventTrigger) {
	s.trigger = trigger
}

// Handlers for this service
func (s *ItemService) Handlers() map[string]core.RequestHandler {
	infoFields := api.FieldMap{
		"name": {
			Type:        api.String,
			Description: assets.L.Get("Name of item"),
			Required:    true,
		},
		"is_actor": {
			Type:        api.Bool,
			Description: assets.L.Get("True if item is an actor"),
			Required:    true,
		},
		"is_sensor": {
			Type:        api.Bool,
			Description: assets.L.Get("True if item is a sensor"),
			Required:    true,
		},
		"is_logger": {
			Type:        api.Bool,
			Description: assets.L.Get("True if item is a logger"),
			Required:    true,
		},
		"events": {
			Description: assets.L.Get("True if item triggers changed events"),
			Required:    true,
			Type: api.FieldMap{
				"{action}": {
					Description: assets.L.Get("Event triggered by item"),
					Type: api.FieldMap{
						"description": {
							Description: assets.L.Get("Description of event"),
							Type:        api.String,
						},
						"parameters": {
							Description: assets.L.Get("Parameters of event"),
							Type: api.FieldMap{
								"{name}": {
									Description: assets.L.Get("Event data value type"),
									Type: api.FieldMap{
										"description": {
											Description: assets.L.Get("Base date type"),
											Type:        api.String,
										},
										"base_type": {
											Description: assets.L.Get("Base date type"),
											Type:        api.String,
										},
										"type_name": {
											Description: assets.L.Get("Concrete type name"),
											Type:        api.String,
										},
									},
								},
							},
						},
					},
				},
			},
		},
		"values": {
			Description: assets.L.Get("Map with provided values and their types"),
			Required:    true,
			Type: api.FieldMap{
				"{name}": {
					Description: assets.L.Get("Value type"),
					Type: api.FieldMap{
						"description": {
							Description: assets.L.Get("Description of values"),
							Type:        api.String,
						},
						"base_type": {
							Description: assets.L.Get("Base date type"),
							Type:        api.String,
						},
						"type_name": {
							Description: assets.L.Get("Concrete type name"),
							Type:        api.String,
						},
					},
				},
			},
		},
	}

	return map[string]core.RequestHandler{
		"add": {
			Func: s.addItem,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Register a new item instance to service"),
				Parameters: api.FieldMap{
					"item": {
						Description: assets.L.Get("Instance of item that should be registered"),
						Required:    true,
					},
				},
			},
		},
		"remove": {
			Func: s.removeItem,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Remove the given item from service"),
				Parameters: api.FieldMap{
					"item": {
						Description: assets.L.Get("Instance of item that should be registered"),
						Required:    true,
					},
				},
			},
		},
		"list": {
			Func: s.itemList,
			Def: api.SimpleHandler{
				Description: assets.L.Get("List all known items"),
				Response: api.FieldMap{
					"{id}": {
						Description: assets.L.Get("Information of item"),
						Type:        infoFields,
					},
				},
			},
		},

		"get_info": {
			Func: s.itemGetInfo,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Returns a list of registered items"),
				Parameters: api.FieldMap{
					"id": {
						Type:        api.String,
						Description: assets.L.Get("Identifier of item"),
						Required:    true,
					},
				},
				Response: infoFields,
			},
		},
		"get_values": {
			Func: s.itemGetValues,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Returns values of a sensor item"),
				Parameters: api.FieldMap{
					"id": {
						Type:        api.String,
						Description: assets.L.Get("Identifier of item (must be a sensor)"),
						Required:    true,
					},
				},
				Response: api.FieldMap{
					"{name}": {
						Description: assets.L.Get("Value of sensor"),
						Required:    true,
					},
				},
			},
		},
		"set_values": {
			Func: s.itemSetValues,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Set values of an actor item"),
				Parameters: api.FieldMap{
					"id": {
						Type:        api.String,
						Description: assets.L.Get("Identifier of item (must be an actor)"),
						Required:    true,
					},
					"values": {
						Description: assets.L.Get("Map of values to set"),
						Required:    true,
						Type: api.FieldMap{
							"{name}": {
								Description: assets.L.Get("Value to set"),
							},
						},
					},
				},
			},
		},
		"log_values": {
			Func: s.itemLogValues,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Log values to item"),
				Parameters: api.FieldMap{
					"id": {
						Type:        api.String,
						Description: assets.L.Get("Identifier of item (must be a logger)"),
						Required:    true,
					},
					"values_id": {
						Type:        api.String,
						Description: assets.L.Get("Identifier of values"),
						Required:    true,
					},
					"values": {
						Description: assets.L.Get("Map of values to log"),
						Required:    true,
						Type: api.FieldMap{
							"{name}": {
								Description: assets.L.Get("Value to log"),
							},
						},
					},
				},
			},
		},
	}
}

// APIEndpoints for item service
func (s *ItemService) APIEndpoints(bus core.Bus) []core.PathEntry {
	return []core.PathEntry{{
		Path: "/items",
		SubEntries: []core.PathEntry{
			core.APIWrapper(bus, s, "list", core.PathEntry{
				Path:  "/",
				Group: "Core",
			}),
			core.APIWrapper(bus, s, "get_values", core.PathEntry{
				Path:  "/:id",
				Group: "Core",
			}),
			core.APIWrapper(bus, s, "set_values", core.PathEntry{
				Path:   "/:id",
				Group:  "Core",
				Method: "PUT",
			}),
		},
	}}
}

// addItem registers an item instance to service
func (s *ItemService) addItem(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	item, ok := data["item"].(core.Item)
	if !ok {
		return nil, errors.New(assets.L.Get("invalid item instance given"))
	}

	id := item.ID()

	// check if ID is valid
	if strings.ContainsAny(id, ":#") {
		return nil, errors.New(assets.L.Get("item id invalid: %s", id))
	}

	// check if item was already added
	if _, ok = s.items[id]; ok {
		return nil, errors.New(assets.L.Get("item already exist: %s", id))
	}

	eventItem, ok := core.GetEventItem(item)
	if ok {
		eventItem.SetEventTrigger(func(action string, data map[string]interface{}) {
			s.trigger(id+"#"+action, data)
		})
	}

	// add item
	s.items[id] = item

	return nil, nil
}

// removeItem remove an item instance from service
func (s *ItemService) removeItem(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	item, ok := data["item"].(core.Item)
	if !ok {
		return nil, errors.New(assets.L.Get("invalid item instance given"))
	}

	delete(s.items, item.ID())
	return nil, nil
}

// itemList returns a map with information about all items
func (s *ItemService) itemList(map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	items := make(map[string]interface{}, len(s.items))

	for id, item := range s.items {
		items[id] = getItemInfo(item)
	}
	return items, nil
}

// itemGetInfo returns information about one items
func (s *ItemService) itemGetInfo(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	id := data["id"].(string)

	// check if item exists
	item, ok := s.items[id]
	if !ok {
		return nil, errors.New(assets.L.Get("item does not exist: %s", id))
	}

	return getItemInfo(item), nil
}

// getItemInfo returns item information map
func getItemInfo(item core.Item) map[string]interface{} {
	info := make(map[string]interface{}, 6)

	info["name"] = item.Name()
	_, info["is_actor"] = core.GetActorItem(item)
	_, info["is_sensor"] = core.GetSensorItem(item)
	_, info["is_logger"] = core.GetLoggerItem(item)

	eventItems, ok := core.GetEventItem(item)
	info["is_event"] = ok
	if ok {
		events := eventItems.Events()
		eventMap := make(map[string]interface{}, len(events))

		for key, event := range events {
			var parameters interface{}

			if event.Parameters != nil {
				parameters = event.Parameters
			} else {
				parameters = make(api.FieldMap)
			}
			eventMap[key] = map[string]interface{}{
				"description": event.Description,
				"parameters":  parameters,
			}
		}

		info["events"] = eventMap
	} else {
		info["events"] = make(map[string]interface{})
	}

	if valueItems, ok := item.(core.ValuesItem); ok {
		values := valueItems.GetValueTypes()

		if values != nil {
			info["values"] = values
		} else {
			info["values"] = make(api.FieldMap)
		}
	} else {
		info["values"] = make(api.FieldMap)
	}

	return info
}

// itemGetValues returns values of item
func (s *ItemService) itemGetValues(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	id := data["id"].(string)

	// check if item exists
	item, ok := s.items[id]
	if !ok {
		return nil, errors.New(assets.L.Get("item does not exist: %s", id))
	}

	sensorItem, ok := core.GetSensorItem(item)
	if !ok {
		return nil, errors.New(assets.L.Get("item %s is not a sensor", id))
	}

	return sensorItem.GetValues(), nil
}

// itemSetValues set values of item
func (s *ItemService) itemSetValues(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	id := data["id"].(string)

	// check if item exists
	item, ok := s.items[id]
	if !ok {
		return nil, errors.New(assets.L.Get("item does not exist: %s", id))
	}

	actorItem, ok := core.GetActorItem(item)
	if !ok {
		return nil, errors.New(assets.L.Get("item %s is not an actor", id))
	}

	return nil, actorItem.SetValues(data["values"].(map[string]interface{}))
}

// itemLogValues logs values to item
func (s *ItemService) itemLogValues(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	id := data["id"].(string)

	// check if item exists
	item, ok := s.items[id]
	if !ok {
		return nil, errors.New(assets.L.Get("item does not exist: %s", id))
	}

	loggerItem, ok := core.GetLoggerItem(item)
	if !ok {
		return nil, errors.New(assets.L.Get("item %s is not a logger", id))
	}

	return nil, loggerItem.LogValues(
		data["values_id"].(string),
		data["values"].(map[string]interface{}))
}
