// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"errors"
	"sync/atomic"
	"testing"
	"time"

	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"

	"github.com/stretchr/testify/assert"
	"github.com/yuin/gopher-lua"
)

func Test_lua2go(t *testing.T) {
	ass := assert.New(t)

	ls := lua.NewState()
	defer ls.Close()

	ass.Equal(true, lua2go(ls, lua.LTrue))
	ass.Equal(float64(42), lua2go(ls, lua.LNumber(42)))
	ass.Equal("test", lua2go(ls, lua.LString("test")))
	ass.Nil(lua2go(ls, lua.LNil))

	data := &lua.LTable{}
	data.RawSetString("test", lua.LString("aaa"))
	ass.Equal(map[string]interface{}{
		"test": "aaa",
	}, lua2go(ls, data))

	luaFunc := &lua.LFunction{}
	function, ok := lua2go(ls, luaFunc).(func([]interface{}) []interface{})
	ass.True(ok)

	resp := function([]interface{}{
		"test",
	})
	ass.Len(resp, 1)
	ass.Error(resp[0].(error))

}

func Test_lua2goL(t *testing.T) {
	ass := assert.New(t)

	ls := lua.NewState()
	defer ls.Close()

	ass.Empty(lua2goL(ls, 0))

	ls.Push(lua.LTrue)
	ls.Push(lua.LNil)

	ass.Equal([]interface{}{
		true, nil,
	}, lua2goL(ls, 2))
}

func Test_go2lua(t *testing.T) {
	ass := assert.New(t)

	ass.Equal(lua.LTrue, go2lua(true))
	ass.Equal(lua.LNumber(42), go2lua(42))
	ass.Equal(lua.LString("test"), go2lua("test"))
	ass.Equal(lua.LString("test"), go2lua(errors.New("test")))

	value := go2lua(map[string]interface{}{
		"test": "aaa",
	}).(*lua.LTable)
	ass.Equal(lua.LString("aaa"), value.RawGet(lua.LString("test")))

	value2 := go2lua([]string{"aaa", "bbb"}).(*lua.LTable)
	ass.Equal(lua.LString("aaa"), value2.RawGetInt(1))
	ass.Equal(lua.LString("bbb"), value2.RawGetInt(2))

}

func Test_go2luaL(t *testing.T) {
	ass := assert.New(t)

	ass.Empty(go2lua([]interface{}{}))

	ass.Equal([]lua.LValue{
		lua.LTrue,
		lua.LNumber(42),
		lua.LNumber(42),
		lua.LString("test"),
		lua.LNil,
	}, go2luaL([]interface{}{
		true,
		float64(42),
		42,
		"test",
		nil,
	}))
}

func Test_go2luaResp(t *testing.T) {
	ass := assert.New(t)

	ls := lua.NewState()
	defer ls.Close()

	ass.Equal(0, go2luaResp(ls, nil))
	ass.Equal(0, go2luaResp(ls, []interface{}{}))

	ass.Equal(5, go2luaResp(ls, []interface{}{
		true, float64(42), 42, "test", nil,
	}))

	ass.Equal(lua.LTrue, ls.Get(1))
	ass.Equal(lua.LNumber(42), ls.Get(2))
	ass.Equal(lua.LNumber(42), ls.Get(3))
	ass.Equal(lua.LString("test"), ls.Get(4))
	ass.Equal(lua.LNil, ls.Get(5))
}

func Test_luaModule(t *testing.T) {
	ass := assert.New(t)

	ls := lua.NewState()
	defer ls.Close()

	var hasPanic bool
	ls.Panic = func(l *lua.LState) {
		hasPanic = true
	}

	functions := map[string]core.ScriptFunction{
		"test": {
			Def: api.Function{
				Arguments: api.FieldMap{
					"aaa": {
						Type:     api.Float64,
						Required: true,
					},
				},
			},
			Func: func(script core.Script, args []interface{}) []interface{} {
				return args
			},
		},
	}

	modules := luaModule(functions, nil)
	ass.Len(modules, 1)
	ass.Contains(modules, "test")

	ls.Push(lua.LNumber(42))
	ass.Equal(1, modules["test"](ls))
	ass.False(hasPanic)
	ass.Equal(lua.LNumber(42), ls.Get(1))
	ass.Equal(lua.LNumber(42), ls.Get(2))
	ls.Pop(2)

	ls.Push(lua.LString("test"))
	ass.Equal(0, modules["test"](ls))
	ass.True(hasPanic)
}
func Test_luaCheck(t *testing.T) {
	ass := assert.New(t)

	ass.NoError(luaCheck(`print("test")`))

	// lexar error
	ass.EqualError(luaCheck(`print("test"`), "script.lua at EOF:   syntax error\n")

	// script error
	ass.EqualError(luaCheck(`break`), "compile error near line(1) script.lua: no loop to break")
}

func TestScript_Start(t *testing.T) {
	ass := assert.New(t)

	var f1Called int32
	var f2Called int32
	var cCalled int32

	f1 := func(script core.Script, args []interface{}) []interface{} {
		atomic.AddInt32(&f1Called, 1)
		ass.Equal("test", args[0].(string))
		return nil
	}
	f2 := func(script core.Script, args []interface{}) []interface{} {
		atomic.AddInt32(&f2Called, 1)
		return nil
	}
	f3 := func(script core.Script, args []interface{}) []interface{} {
		time.Sleep(time.Millisecond * 10)
		return nil
	}

	def := core.ScriptDefinition{
		Modules: map[string]core.ScriptModuleDefinition{
			"test": {
				Functions: map[string]core.ScriptFunction{
					"f1": {
						Def: api.Function{
							Arguments: api.FieldMap{
								"aaa": {
									Type:     api.String,
									Required: true,
								},
							},
						},
						Func: f1,
					},
				},
				CleanupFunc: func(script core.Script) {
					atomic.AddInt32(&cCalled, 1)
				},
			},
		},
		GlobalFunctions: map[string]core.ScriptFunction{
			"f2": {
				Func: f2,
			},
			"f3": {
				Func: f3,
			},
		},
		ConstantValues: map[string]interface{}{
			"value": "test",
		},
	}

	content := `
local n = require("test")
n.f1(value)
f2()
`
	s := luaScript{
		def:     &def,
		content: content,
	}

	s.Start()
	time.Sleep(time.Millisecond * 10)

	ass.True(s.IsRunning())
	ass.Equal(content, s.Content())

	ass.Equal(int32(1), atomic.LoadInt32(&f1Called))
	ass.Equal(int32(1), atomic.LoadInt32(&f2Called))
	s.Stop()

	s2 := luaScript{
		def: &def,
		content: `
f3()
f2()
`,
	}
	s2.Start()
	s2.Stop()
	s2.Stop()
	time.Sleep(time.Millisecond * 100)

	ass.True(atomic.LoadInt32(&cCalled) >= 2)
	ass.Equal(int32(1), atomic.LoadInt32(&f2Called))

	s.Start()
	time.Sleep(time.Millisecond * 100)
	ass.Equal(int32(2), atomic.LoadInt32(&f2Called))
}

func TestScriptService_Key(t *testing.T) {
	ass := assert.New(t)
	scriptService := new(ScriptService)
	ass.Equal("core.scripts", scriptService.Key())
}

type testModule bool

func (m *testModule) Name() string {
	return "test"
}
func (m *testModule) Def() core.ScriptModuleDefinition {
	return core.ScriptModuleDefinition{}
}

func TestScriptService_AddModule(t *testing.T) {
	ass := assert.New(t)

	scriptService := new(ScriptService)
	scriptService.def = core.ScriptDefinition{
		Modules: make(map[string]core.ScriptModuleDefinition),
	}

	ass.NoError(scriptService.AddModule(new(testModule)))
	ass.Len(scriptService.def.Modules, 1)
	ass.Contains(scriptService.def.Modules, "test")

	ass.EqualError(scriptService.AddModule(new(testModule)), "module test already added")
}

func TestScriptService_Init(t *testing.T) {
	ass := assert.New(t)

	bus := new(core.TestBus)

	err := errors.New("test")
	data := map[string]interface{}{}
	bus.HandleRequestWaitFunc = func(req core.Request, timeout time.Duration) core.Response {
		ass.Equal("core.config#get", req.ID)
		ass.Equal(map[string]interface{}{
			"key": "core.scripts",
		}, req.Parameters)
		return core.Response{
			OrgRequest: req,
			Error:      err,
			Data: map[string]interface{}{
				"value": data,
			},
		}
	}

	scriptService := new(ScriptService)
	ass.EqualError(scriptService.Init(bus), "test")
	err = nil

	scriptService = new(ScriptService)
	ass.NoError(scriptService.Init(bus))
	ass.Empty(scriptService.scripts)

	data = map[string]interface{}{
		"aaa": map[string]interface{}{
			"script":  "test1",
			"enabled": false,
		},
		"bbb": map[string]interface{}{
			"script":  "test2",
			"enabled": true,
		},
	}

	scriptService = new(ScriptService)
	ass.NoError(scriptService.Init(bus))
	ass.Equal("test1", scriptService.scripts["aaa"].content)
	ass.Nil(scriptService.scripts["aaa"].luaVM)

	ass.NoError(scriptService.Start(nil))
	ass.Equal("test2", scriptService.scripts["bbb"].content)
}

func TestScriptService_Update(t *testing.T) {
	ass := assert.New(t)

	bus := new(core.TestBus)

	err := errors.New("test")
	bus.HandleRequestWaitFunc = func(req core.Request, timeout time.Duration) core.Response {
		ass.Equal("core.config#set", req.ID)
		ass.Equal(map[string]interface{}{
			"key": "core.scripts",
			"value": map[string]interface{}{
				"aaa": map[string]interface{}{
					"script":          "test",
					"enabled":         true,
					"blockly_enabled": true,
					"blockly_xml":     "aaa",
				},
			},
		}, req.Parameters)
		return core.Response{
			OrgRequest: req,
			Error:      err,
		}
	}

	scriptService := new(ScriptService)
	scriptService.scripts = map[string]*luaScript{
		"aaa": {
			content:        "test",
			enabled:        true,
			blocklyEnabled: true,
			blocklyXML:     "aaa",
		},
	}
	scriptService.Update(bus)
}

func TestScriptService_Shutdown(t *testing.T) {
	ass := assert.New(t)

	bus := new(core.TestBus)

	bus.HandleRequestWaitFunc = func(req core.Request, timeout time.Duration) core.Response {
		ass.Equal("core.config#set", req.ID)
		ass.Equal(map[string]interface{}{
			"key": "core.scripts",
			"value": map[string]interface{}{
				"aaa": map[string]interface{}{
					"script":          "test",
					"enabled":         true,
					"blockly_enabled": true,
					"blockly_xml":     "aaa",
				},
			},
		}, req.Parameters)
		return core.Response{
			OrgRequest: req,
		}
	}

	scriptService := new(ScriptService)
	scriptService.scripts = map[string]*luaScript{
		"aaa": {
			content:        "test",
			enabled:        true,
			blocklyEnabled: true,
			blocklyXML:     "aaa",
		},
	}
	scriptService.Shutdown(bus)
}

func TestScriptService_Handlers(t *testing.T) {
	ass := assert.New(t)

	scriptService := new(ScriptService)

	ass.Len(scriptService.Handlers(), 6)
	ass.Contains(scriptService.Handlers(), "list_modules")
	ass.Contains(scriptService.Handlers(), "add")
	ass.Contains(scriptService.Handlers(), "update")
	ass.Contains(scriptService.Handlers(), "remove")
	ass.Contains(scriptService.Handlers(), "list")
	ass.Contains(scriptService.Handlers(), "get")
}

func TestScriptService_APIEndpoints(t *testing.T) {
	ass := assert.New(t)

	scriptService := new(ScriptService)
	endpoints := scriptService.APIEndpoints(new(core.TestBus))
	ass.Len(endpoints, 1)
	ass.Len(endpoints[0].SubEntries, 5)
}

func TestScriptService_moduleList(t *testing.T) {
	ass := assert.New(t)

	scriptService := new(ScriptService)
	scriptService.def = core.ScriptDefinition{
		Modules: make(map[string]core.ScriptModuleDefinition),
	}

	ass.NoError(scriptService.AddModule(new(testModule)))

	data, err := scriptService.moduleList(nil)
	ass.NoError(err)
	ass.Equal(map[string]interface{}{
		"test": map[string]interface{}{
			"description": "",
			"functions":   map[string]api.Function{},
		},
	}, data)
}

func TestScriptService_scriptAdd(t *testing.T) {
	ass := assert.New(t)

	scriptService := new(ScriptService)
	scriptService.scripts = map[string]*luaScript{}

	data := map[string]interface{}{
		"id":      "aaa",
		"script":  "test",
		"enabled": true,
	}

	_, err := scriptService.scriptAdd(data)
	ass.EqualError(err, "script invalid: script.lua at EOF:   parse error\n")

	data["script"] = "print()"

	_, err = scriptService.scriptAdd(data)
	ass.NoError(err)
	ass.Contains(scriptService.scripts, "aaa")
	ass.Equal("print()", scriptService.scripts["aaa"].content)
	ass.True(scriptService.scripts["aaa"].enabled)

	_, err = scriptService.scriptAdd(data)
	ass.EqualError(err, "script already exist: aaa")
}

func TestScriptService_scriptUpdate(t *testing.T) {
	ass := assert.New(t)

	scriptService := new(ScriptService)
	scriptService.scripts = map[string]*luaScript{}

	data := map[string]interface{}{
		"id":      "aaa",
		"script":  "test",
		"enabled": true,
	}

	_, err := scriptService.scriptUpdate(data)
	ass.EqualError(err, "script does not exist: aaa")

	scriptService.scripts = map[string]*luaScript{
		"aaa": {
			def: new(core.ScriptDefinition),
		},
	}

	_, err = scriptService.scriptUpdate(data)
	ass.EqualError(err, "script invalid: script.lua at EOF:   parse error\n")

	data["script"] = "print()"

	_, err = scriptService.scriptUpdate(data)
	ass.NoError(err)
	ass.Contains(scriptService.scripts, "aaa")
	ass.Equal("print()", scriptService.scripts["aaa"].content)
	ass.True(scriptService.scripts["aaa"].enabled)
	ass.NotNil(scriptService.scripts["aaa"].luaVM)

	_, err = scriptService.scriptUpdate(data)
	ass.NoError(err)
}

func TestScriptService_scriptRemove(t *testing.T) {
	ass := assert.New(t)

	scriptService := new(ScriptService)
	scriptService.scripts = map[string]*luaScript{}

	data := map[string]interface{}{
		"id": "aaa",
	}

	_, err := scriptService.scriptRemove(data)
	ass.EqualError(err, "script does not exist: aaa")

	_, err = scriptService.scriptAdd(map[string]interface{}{
		"id":      "aaa",
		"script":  "print()",
		"enabled": true,
	})
	ass.NoError(err)

	_, err = scriptService.scriptRemove(data)
	ass.NoError(err)
	ass.Empty(scriptService.scripts)
}

func TestScriptService_scriptList(t *testing.T) {
	ass := assert.New(t)

	scriptService := new(ScriptService)
	scriptService.scripts = map[string]*luaScript{
		"aaa": {
			content:        "aaa",
			enabled:        true,
			blocklyEnabled: true,
			blocklyXML:     "bbb",
		},
	}

	value, err := scriptService.scriptList(nil)
	ass.NoError(err)
	ass.Equal(map[string]interface{}{
		"aaa": map[string]interface{}{
			"script":          "aaa",
			"enabled":         true,
			"blockly_enabled": true,
			"blockly_xml":     "bbb",
		},
	}, value)
}

func TestScriptService_scriptGet(t *testing.T) {
	ass := assert.New(t)

	scriptService := new(ScriptService)
	scriptService.scripts = map[string]*luaScript{}

	data := map[string]interface{}{
		"id": "aaa",
	}

	_, err := scriptService.scriptGet(data)
	ass.EqualError(err, "script does not exist: aaa")

	scriptService.scripts = map[string]*luaScript{
		"aaa": {
			content:        "aaa",
			enabled:        true,
			blocklyEnabled: true,
			blocklyXML:     "bbb",
		},
	}

	value, err := scriptService.scriptGet(map[string]interface{}{
		"id": "aaa",
	})
	ass.NoError(err)
	ass.Equal(map[string]interface{}{
		"script":          "aaa",
		"enabled":         true,
		"blockly_enabled": true,
		"blockly_xml":     "bbb",
	}, value)
}
