// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/leonelquinteros/gotext"
	"github.com/spf13/cast"
	"gitlab.com/bboehmke/timer"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// values that describe the timer
var timerValues = api.FieldMap{
	"enabled": {
		Description: gotext.Get("True if the timer is enabled"),
		Type:        api.Bool,
	},
	"item_values": {
		Description: assets.L.Get("List of item values"),
		Type:        api.Slice(api.String),
	},
	"times": {
		Description: assets.L.Get("List of time entries"),
		Type: api.FieldMap{
			"{idx}": {
				Type: api.FieldMap{
					"active": {
						Description: assets.L.Get("True if entry is enabled"),
						Type:        api.Bool,
						Required:    true,
					},
					"time": {
						Description: assets.L.Get("Time of entry"),
						Type:        api.Int,
						Required:    true,
					},
					"repeat": {
						Description: assets.L.Get("Repeat weekday or 0 for onetime"),
						Type:        api.Int,
						Required:    true,
					},
					"state": {
						Description: assets.L.Get("State value for this entry"),
						Type:        api.Any,
						Required:    true,
					},
				},
			},
		},
	},
}

// Timer triggers on time entries
// -> Inspired from https://github.com/robfig/cron
type Timer struct {
	timer.Timer

	// item values to set on trigger
	itemValues []string

	// mutex for synchronisation of item values
	mutex sync.RWMutex

	// key of this timer
	key string

	// instance of timer service
	service *TimerService

	trigger func(action string, data map[string]interface{})
}

// ID of item
func (t *Timer) ID() string {
	return "core.timer." + t.key
}

// Name of item
func (t *Timer) Name() string {
	return "Timer " + t.key
}

// Events returns all events that can be emitted
func (t *Timer) Events() map[string]api.SimpleHandler {
	return map[string]api.SimpleHandler{
		"trigger": {
			Description: assets.L.Get("Emitted on timer trigger"),
			Parameters: map[string]api.Field{
				"state": {
					Description: assets.L.Get("State of timer entry"),
				},
			},
		},
		"changed": {
			Description: assets.L.Get("Emitted if a value changed"),
			Parameters:  timerValues,
		},
	}
}

// SetEventTrigger for item event
func (t *Timer) SetEventTrigger(trigger func(action string, data map[string]interface{})) {
	t.trigger = trigger
}

// GetValueTypes of item
func (t *Timer) GetValueTypes() api.FieldMap {
	return timerValues
}

// SetValues of timer
func (t *Timer) SetValues(values map[string]interface{}) error {
	if v, ok := values["enabled"]; ok {
		if cast.ToBool(v) {
			t.Start()
		} else {
			t.Stop()
		}
		t.trigger("changed", map[string]interface{}{
			"enabled": v,
		})
	}
	return nil
}

// GetValues from timer
func (t *Timer) GetValues() map[string]interface{} {
	return t.Dump()
}

// AddItemValue to set on trigger event
func (t *Timer) AddItemValue(itemValue string) {
	t.mutex.Lock()
	defer t.mutex.Unlock()

	for _, value := range t.itemValues {
		if value == itemValue {
			return
		}
	}
	t.itemValues = append(t.itemValues, itemValue)

	t.trigger("changed", map[string]interface{}{
		"item_values": t.itemValues,
	})
}

// RemoveItemValue from timer
func (t *Timer) RemoveItemValue(itemValue string) {
	t.mutex.Lock()
	defer t.mutex.Unlock()

	values := make([]string, 0)
	for _, value := range t.itemValues {
		if value != itemValue {
			values = append(values, value)
		}
	}
	t.itemValues = values

	t.trigger("changed", map[string]interface{}{
		"item_values": t.itemValues,
	})
}

// Dump timer information
func (t *Timer) Dump() map[string]interface{} {
	return map[string]interface{}{
		"times":       t.ListEntries(),
		"item_values": t.itemValues,
		"enabled":     t.IsRunning(),
	}
}

// triggerState if given and active
func (t *Timer) triggerState(state interface{}) {
	t.trigger("trigger", map[string]interface{}{
		"state": state,
	})

	t.mutex.RLock()
	for _, val := range t.itemValues {
		err := t.setState(val, state)
		if err != nil {
			zap.L().Error(err.Error())
		}
	}
	t.mutex.RUnlock()
}

// setState of the given item value
func (t *Timer) setState(itemValue string, state interface{}) error {
	item := strings.Split(itemValue, "#")
	if len(item) < 2 {
		return errors.New(assets.L.Get("invalid item_value should be [ITEM_ID]#[VALUE] given %s", itemValue))
	}

	return t.service.bus.HandleRequestWait(core.Request{
		ID: "core.items#set_values",
		Parameters: map[string]interface{}{
			"id": item[0],
			"values": map[string]interface{}{
				item[1]: state,
			},
		},
	}, time.Second).Error
}

// TimerService handles timer objects
type TimerService struct {
	// map of created timers
	timers map[string]*Timer

	// map of timers to start on init
	timersToStart map[string]*Timer

	// mutex for timers
	mutex sync.RWMutex

	bus core.Bus
}

// Key that identifies this service
func (s *TimerService) Key() string {
	return "core.timers"
}

// Init load the service
func (s *TimerService) Init(bus core.Bus) error {
	s.bus = bus

	value, err := core.ConfigRead(bus, "core.timers")
	if err != nil {
		return err
	}

	timersRaw := cast.ToStringMap(value)
	s.timers = make(map[string]*Timer, len(timersRaw))
	s.timersToStart = make(map[string]*Timer, len(timersRaw))
	for key, info := range timersRaw {
		infoData := cast.ToStringMap(info)

		timesRaw := cast.ToSlice(infoData["times"])
		times := make([]*timer.TimeEntry, len(timesRaw))
		for idx, entry := range timesRaw {
			data := cast.ToStringMap(entry)
			times[idx] = &timer.TimeEntry{
				Active: cast.ToBool(data["active"]),
				Time:   cast.ToInt(data["time"]),
				Repeat: cast.ToInt(data["repeat"]),
				State:  data["state"],
			}
		}

		s.timers[key] = s.newTimer(key, times, cast.ToStringSlice(infoData["item_values"]))

		// start timer if:
		//   > enabled not in config (old config)
		//   > enabled set and true
		if value, ok := infoData["enabled"]; !ok || cast.ToBool(value) {
			s.timersToStart[key] = s.timers[key]
		}

		err := core.ItemAdd(bus, s.timers[key])
		if err != nil {
			return fmt.Errorf("failed to add item %s", err)
		}
	}
	zap.L().Info(assets.L.GetN(
		"Loaded %d timer", "Loaded %d timers",
		len(s.timers), len(s.timers)))
	return nil
}

// Start timers
func (s *TimerService) Start(bus core.Bus) error {
	for _, t := range s.timersToStart {
		t.Start()
	}
	return nil
}

// Update saves actual timers
func (s *TimerService) Update(bus core.Bus) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	data := make(map[string]interface{}, len(s.timers))
	for key, t := range s.timers {
		data[key] = t.Dump()
	}

	err := core.ConfigWrite(bus, "core.timers", data)
	if err != nil {
		zap.L().Error(err.Error())
	}
}

// Shutdown saves actual timers
func (s *TimerService) Shutdown(bus core.Bus) {
	s.Update(bus)
	for _, t := range s.timers {
		t.Stop()

		err := core.ItemRemove(bus, t)
		if err != nil {
			zap.L().Error(err.Error())
		}
	}
}

// Handlers for this service
func (s *TimerService) Handlers() map[string]core.RequestHandler {
	return map[string]core.RequestHandler{
		"add": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("Add a new timer"),
				Parameters: api.FieldMap{
					"key": {
						Description: assets.L.Get("Identifier of timer"),
						Type:        api.String,
						Required:    true,
					},
				},
			},
			Func: s.addTimer,
		},
		"remove": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("Remove timer"),
				Parameters: api.FieldMap{
					"key": {
						Description: assets.L.Get("Identifier of timer"),
						Type:        api.String,
						Required:    true,
					},
				},
			},
			Func: s.removeTimer,
		},
		"list": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("List all timers"),
				Response: map[string]api.Field{
					"{key}": {
						Description: assets.L.Get("Information of timer"),
						Type:        timerValues,
					},
				},
			},
			Func: s.listTimer,
		},
		"get": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("Get timer information"),
				Parameters: api.FieldMap{
					"key": {
						Description: assets.L.Get("Identifier of timer"),
						Type:        api.String,
						Required:    true,
					},
				},
				Response: timerValues,
			},
			Func: s.getTimer,
		},

		"add_time": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("Add time to timer"),
				Parameters: api.FieldMap{
					"key": {
						Description: assets.L.Get("Identifier of timer"),
						Type:        api.String,
						Required:    true,
					},

					"active": {
						Description:  assets.L.Get("True if entry is enabled"),
						Type:         api.Bool,
						DefaultValue: true,
					},
					"time": {
						Description: assets.L.Get("Time of entry"),
						//Type: api.Int,
						Required: true,
					},
					"repeat": {
						Description:  assets.L.Get("Repeat weekday or 0 for onetime"),
						Type:         api.Int,
						DefaultValue: timer.Everyday,
					},
					"state": {
						Description: assets.L.Get("State value for this entry"),
						Type:        api.Any,
					},
				},
			},
			Func: s.addTimeEntry,
		},
		"edit_time": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("Edit time of timer"),
				Parameters: api.FieldMap{
					"key": {
						Description: assets.L.Get("Identifier of timer"),
						Type:        api.String,
						Required:    true,
					},
					"idx": {
						Description: assets.L.Get("Index of time entry"),
						Type:        api.Uint,
						Required:    true,
					},

					"active": {
						Description: assets.L.Get("True if entry is enabled"),
						Type:        api.Bool,
					},
					"time": {
						Description: assets.L.Get("Time of entry"),
						//Type: api.Int,
					},
					"repeat": {
						Description: assets.L.Get("Repeat weekday or 0 for onetime"),
						Type:        api.Uint8,
					},
					"state": {
						Description: assets.L.Get("State value for this entry"),
						Type:        api.Any,
					},
				},
			},
			Func: s.editTimeEntry,
		},
		"remove_time": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("Remove time of timer"),
				Parameters: api.FieldMap{
					"key": {
						Description: assets.L.Get("Identifier of timer"),
						Type:        api.String,
						Required:    true,
					},
					"idx": {
						Description: assets.L.Get("Index of time entry"),
						Type:        api.Uint,
						Required:    true,
					},
				},
			},
			Func: s.removeTimeEntry,
		},

		"add_item_value": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("Add item value to timer"),
				Parameters: api.FieldMap{
					"key": {
						Description: assets.L.Get("Identifier of timer"),
						Type:        api.String,
						Required:    true,
					},
					"item_value": {
						Description: assets.L.Get("Item value"),
						Type:        api.String,
						Required:    true,
					},
				},
			},
			Func: s.addItemValue,
		},
		"remove_item_value": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("Remove item value from timer"),
				Parameters: api.FieldMap{
					"key": {
						Description: assets.L.Get("Identifier of timer"),
						Type:        api.String,
						Required:    true,
					},
					"item_value": {
						Description: assets.L.Get("Item value"),
						Type:        api.String,
						Required:    true,
					},
				},
			},
			Func: s.removeItemValue,
		},
	}
}

// APIEndpoints for service
func (s *TimerService) APIEndpoints(bus core.Bus) []core.PathEntry {
	return []core.PathEntry{{
		Path: "/timers",
		SubEntries: []core.PathEntry{
			core.APIWrapper(bus, s, "list", core.PathEntry{
				Path:  "/",
				Group: "Core",
			}),
			core.APIWrapper(bus, s, "add", core.PathEntry{
				Path:   "/",
				Group:  "Core",
				Method: "POST",
			}),
			core.APIWrapper(bus, s, "get", core.PathEntry{
				Path:  "/:key",
				Group: "Core",
			}),
			core.APIWrapper(bus, s, "remove", core.PathEntry{
				Path:   "/:key",
				Group:  "Core",
				Method: "DELETE",
			}),

			core.APIWrapper(bus, s, "add_time", core.PathEntry{
				Path:   "/:key/times",
				Group:  "Core",
				Method: "POST",
			}),
			core.APIWrapper(bus, s, "edit_time", core.PathEntry{
				Path:   "/:key/times/:idx",
				Group:  "Core",
				Method: "PUT",
			}),
			core.APIWrapper(bus, s, "remove_time", core.PathEntry{
				Path:   "/:key/times/:idx",
				Group:  "Core",
				Method: "DELETE",
			}),

			core.APIWrapper(bus, s, "add_item_value", core.PathEntry{
				Path:   "/:key/item_values",
				Group:  "Core",
				Method: "POST",
			}),
			core.APIWrapper(bus, s, "remove_item_value", core.PathEntry{
				Path:   "/:key/item_values/:item_value",
				Group:  "Core",
				Method: "DELETE",
			}),
		},
	}}
}

// newTimer creates a new empty timer
func (s *TimerService) newTimer(key string, times []*timer.TimeEntry, itemValues []string) *Timer {
	if itemValues == nil {
		itemValues = make([]string, 0)
	}

	t := &Timer{
		service:    s,
		key:        key,
		itemValues: itemValues,
	}

	t.Timer = timer.NewTimerFunc(t.triggerState, times)

	return t
}

func (s *TimerService) addTimer(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	key := cast.ToString(data["key"])
	_, ok := s.timers[key]
	if ok {
		return nil, errors.New(assets.L.Get("timer with key %s already exist", key))
	}

	t := s.newTimer(key, nil, nil)
	t.Start()

	s.timers[key] = t

	return nil, nil
}

func (s *TimerService) removeTimer(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	key := cast.ToString(data["key"])
	t, ok := s.timers[key]
	if !ok {
		return nil, errors.New(assets.L.Get("no timer with key %s", key))
	}

	delete(s.timers, key)
	t.Stop()

	return nil, nil
}

func (s *TimerService) listTimer(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	timers := make(map[string]interface{}, len(s.timers))
	for key, t := range s.timers {
		timers[key] = t.Dump()
	}
	return timers, nil
}

func (s *TimerService) getTimer(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	key := cast.ToString(data["key"])
	t, ok := s.timers[key]
	if !ok {
		return nil, errors.New(assets.L.Get("no timer with key %s", key))
	}

	return t.Dump(), nil
}

// convertTime from interface to int (seconds since midnight)
func convertTime(raw interface{}) int {
	timeStr := cast.ToString(raw)
	if strings.Contains(timeStr, ":") {
		t := 0
		parts := strings.SplitN(timeStr, ":", 3)
		for _, e := range parts {
			i, _ := strconv.Atoi(e)
			t = t*60 + i
		}

		// no seconds given?
		if len(parts) < 3 {
			t = t * 60
		}
		return t
	}
	return cast.ToInt(raw)
}

func (s *TimerService) addTimeEntry(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	key := cast.ToString(data["key"])
	t, ok := s.timers[key]
	if !ok {
		return nil, errors.New(assets.L.Get("no timer with key %s", key))
	}

	t.AddEntry(timer.TimeEntry{
		Active: cast.ToBool(data["active"]),
		Time:   convertTime(data["time"]),
		Repeat: cast.ToInt(data["repeat"]),
		State:  data["state"],
	})

	// trigger change event for times
	t.trigger("changed", map[string]interface{}{
		"times": t.ListEntries(),
	})
	return nil, nil
}

func (s *TimerService) editTimeEntry(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	key := cast.ToString(data["key"])
	t, ok := s.timers[key]
	if !ok {
		return nil, errors.New(assets.L.Get("no timer with key %s", key))
	}

	idx := cast.ToInt(data["idx"])
	entries := t.ListEntries()
	if idx < 0 || idx >= len(entries) {
		return nil, errors.New(assets.L.Get("invalid time entry index %d", idx))
	}
	entry := entries[idx]

	// remove old entry
	t.RemoveEntry(idx)

	if active, ok := data["active"]; ok {
		entry.Active = cast.ToBool(active)
	}

	if dataTime, ok := data["time"]; ok {
		entry.Time = convertTime(dataTime)
	}

	if repeat, ok := data["repeat"]; ok {
		entry.Repeat = cast.ToInt(repeat)
	}

	if state, ok := data["state"]; ok {
		entry.State = state
	}

	// add entry again
	t.AddEntry(entry)

	// trigger change event for times
	t.trigger("changed", map[string]interface{}{
		"times": t.ListEntries(),
	})
	return nil, nil
}

func (s *TimerService) removeTimeEntry(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	key := cast.ToString(data["key"])
	t, ok := s.timers[key]
	if !ok {
		return nil, errors.New(assets.L.Get("no timer with key %s", key))
	}

	idx := cast.ToInt(data["idx"])
	entries := t.ListEntries()
	if idx < 0 || idx >= len(entries) {
		return nil, errors.New(assets.L.Get("invalid time entry index %d", idx))
	}

	// remove old entry
	t.RemoveEntry(idx)

	// trigger change event for times
	t.trigger("changed", map[string]interface{}{
		"times": t.ListEntries(),
	})
	return nil, nil
}

func (s *TimerService) addItemValue(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	key := cast.ToString(data["key"])
	t, ok := s.timers[key]
	if !ok {
		return nil, errors.New(assets.L.Get("no timer with key %s", key))
	}

	t.AddItemValue(cast.ToString(data["item_value"]))
	return nil, nil
}

func (s *TimerService) removeItemValue(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	key := cast.ToString(data["key"])
	t, ok := s.timers[key]
	if !ok {
		return nil, errors.New(assets.L.Get("no timer with key %s", key))
	}

	t.RemoveItemValue(cast.ToString(data["item_value"]))
	return nil, nil
}
