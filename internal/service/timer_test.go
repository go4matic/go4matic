// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package service

import (
	"errors"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/bboehmke/timer"

	"gitlab.com/go4matic/go4matic/pkg/core"
)

func TestTimer_itemInterface(t *testing.T) {
	ass := assert.New(t)

	testTrigger := func(action string, data map[string]interface{}) {}

	ti := Timer{
		key:        "test",
		itemValues: []string{"aaa", "bbb"},
		Timer: &timer.TestTimer{
			Times: []timer.TimeEntry{},
		},
	}
	ass.Equal("core.timer.test", ti.ID())
	ass.Equal("Timer test", ti.Name())

	ass.Len(ti.Events(), 2)
	ass.Contains(ti.Events(), "trigger")
	ass.Contains(ti.Events(), "changed")

	ass.Nil(ti.trigger)
	ti.SetEventTrigger(testTrigger)
	ass.NotNil(ti.trigger)

	ass.Equal(timerValues, ti.GetValueTypes())

	ass.Equal(map[string]interface{}{
		"enabled": false,
		"item_values": []string{
			"aaa", "bbb",
		},
		"times": []timer.TimeEntry{},
	}, ti.GetValues())

	ass.NoError(ti.SetValues(map[string]interface{}{
		"enabled": true,
	}))
	ass.True(ti.IsRunning())
	ass.NoError(ti.SetValues(map[string]interface{}{
		"enabled": false,
	}))
	ass.False(ti.IsRunning())
}

func TestTimer_setState(t *testing.T) {
	ass := assert.New(t)

	bus := core.CreateBus()
	defer bus.Shutdown()

	service := &TimerService{
		bus: bus,
	}
	ti := service.newTimer("", nil, nil)

	ass.EqualError(ti.setState("test", nil),
		"invalid item_value should be [ITEM_ID]#[VALUE] given test")

	// will fail because missing service
	ass.EqualError(ti.setState("test#aaa", nil),
		"no service for core.items")
}

func TestTimer_triggerEntries(t *testing.T) {
	ass := assert.New(t)

	bus := core.CreateBus()
	defer bus.Shutdown()

	var eventID string
	var eventData map[string]interface{}

	service := new(TimerService)

	ti := service.newTimer("aaa", nil, []string{"bbb"})
	ti.SetEventTrigger(func(id string, data map[string]interface{}) {
		eventID = id
		eventData = data
	})
	ti.triggerState("test")

	ass.Equal("trigger", eventID)
	ass.Equal(map[string]interface{}{
		"state": "test",
	}, eventData)
}

func TestTimer_run_trigger(t *testing.T) {
	ass := assert.New(t)

	service := new(TimerService)

	now := time.Now()
	ti := service.newTimer("", []*timer.TimeEntry{
		{
			Active: true,
			Time:   (now.Hour()*60+now.Minute())*60 + now.Second() + 1,
		},
	}, nil)

	var triggerTime time.Time
	mutex := sync.Mutex{}
	ti.SetEventTrigger(func(id string, data map[string]interface{}) {
		mutex.Lock()
		defer mutex.Unlock()
		triggerTime = time.Now()
	})

	ti.Start()
	ass.True(ti.IsRunning())
	defer ti.Stop()

	time.Sleep(time.Second * 3)
	mutex.Lock()
	defer mutex.Unlock()
	ass.True(triggerTime.Sub(now) <= time.Second*2)
}

func TestTimer_ItemValues(t *testing.T) {
	ass := assert.New(t)

	service := new(TimerService)
	ti := service.newTimer("", nil, nil)
	ti.SetEventTrigger(func(string, map[string]interface{}) {})

	ti.AddItemValue("aaa")
	ass.Equal([]string{"aaa"}, ti.itemValues)
	ti.AddItemValue("bbb")
	ass.Equal([]string{"aaa", "bbb"}, ti.itemValues)
	ti.AddItemValue("bbb")
	ass.Equal([]string{"aaa", "bbb"}, ti.itemValues)

	ti.RemoveItemValue("bbb")
	ass.Equal([]string{"aaa"}, ti.itemValues)
	ti.RemoveItemValue("bbb")
	ass.Equal([]string{"aaa"}, ti.itemValues)
}

func TestTimer_Dump(t *testing.T) {
	ass := assert.New(t)

	service := new(TimerService)
	ti := service.newTimer("", []*timer.TimeEntry{
		{
			Time:   42,
			Active: true,
			State:  "test",
			Repeat: 2,
		},
	}, []string{"aaa", "bbb"})

	ti.Start()
	ass.True(ti.IsRunning())
	defer ti.Stop()

	ass.Equal(map[string]interface{}{
		"enabled":     true,
		"item_values": []string{"aaa", "bbb"},
		"times": []timer.TimeEntry{
			{Active: true, Time: 42, Repeat: 2, State: "test"},
		},
	}, ti.Dump())
}

func TestTimerService_Key(t *testing.T) {
	ass := assert.New(t)
	service := new(TimerService)
	ass.Equal("core.timers", service.Key())
}

func TestTimerService_Handlers(t *testing.T) {
	ass := assert.New(t)
	service := new(TimerService)
	ass.Len(service.Handlers(), 9)
}

func TestTimerService_APIEndpoints(t *testing.T) {
	ass := assert.New(t)

	bus := core.CreateBus()
	defer bus.Shutdown()

	service := new(TimerService)
	ass.Len(service.APIEndpoints(bus), 1)
}

func TestTimerService_InitStartShutdown(t *testing.T) {
	ass := assert.New(t)

	bus := new(core.TestBus)

	err := errors.New("test")
	data := map[string]interface{}{}
	bus.HandleRequestWaitFunc = func(req core.Request, timeout time.Duration) core.Response {
		switch req.ID {
		case "core.config#get":
			ass.Equal(map[string]interface{}{
				"key": "core.timers",
			}, req.Parameters)
			return core.Response{
				OrgRequest: req,
				Error:      err,
				Data: map[string]interface{}{
					"value": data,
				},
			}
		case "core.items#add":
			ass.NotNil(req.Parameters["item"])
			return core.Response{
				OrgRequest: req,
				Error:      err,
			}
		default:
			ass.Failf("unexpected request: %s", req.ID)
			return core.Response{
				OrgRequest: req,
				Error:      err,
			}
		}
	}

	timerService := new(TimerService)
	ass.EqualError(timerService.Init(bus), "test")

	err = nil

	timerService = new(TimerService)
	ass.NoError(timerService.Init(bus))
	ass.Empty(timerService.timers)

	data = map[string]interface{}{
		"aaa": map[string]interface{}{
			"times": []interface{}{
				map[string]interface{}{
					"active": true,
					"time":   42,
					"repeat": 2,
					"state":  "test",
				},
			},
			"item_values": []string{
				"aaa", "bbb",
			},
		},
	}

	timerService = new(TimerService)
	ass.NoError(timerService.Init(bus))
	ass.Len(timerService.timers, 1)

	timerService.timers["aaa"].SetEventTrigger(func(id string, data map[string]interface{}) {})

	ass.Equal([]string{"aaa", "bbb"}, timerService.timers["aaa"].itemValues)
	ass.Equal([]timer.TimeEntry{
		{
			Active: true,
			Time:   42,
			Repeat: 2,
			State:  "test",
		},
	}, timerService.timers["aaa"].ListEntries())

	ass.NoError(timerService.Start(bus))
	ass.True(timerService.timers["aaa"].IsRunning())

	bus.HandleRequestWaitFunc = func(req core.Request, timeout time.Duration) core.Response {
		switch req.ID {
		case "core.config#set":
			ass.Equal(map[string]interface{}{
				"key": "core.timers",
				"value": map[string]interface{}{
					"aaa": map[string]interface{}{
						"times": []timer.TimeEntry{
							{
								Active: true,
								Time:   42,
								Repeat: 2,
								State:  "test",
							},
						},
						"item_values": []string{
							"aaa", "bbb",
						},
						"enabled": true,
					},
				},
			}, req.Parameters)
		case "core.items#remove":
			ass.NotNil(req.Parameters["item"])
		default:
			ass.Failf("unexpected request: %s", req.ID)
		}
		return core.Response{
			OrgRequest: req,
			Error:      err,
		}
	}

	timerService.Shutdown(bus)
	ass.False(timerService.timers["aaa"].IsRunning())

}

func TestTimerService_handler_Timer(t *testing.T) {
	ass := assert.New(t)

	bus := core.CreateBus()
	defer bus.Shutdown()

	timerService := &TimerService{
		timers: make(map[string]*Timer),
	}

	// add
	_, err := timerService.addTimer(map[string]interface{}{
		"key": "aaa",
	})
	ass.NoError(err)

	_, err = timerService.addTimer(map[string]interface{}{
		"key": "aaa",
	})
	ass.EqualError(err, "timer with key aaa already exist")

	// get
	data, err := timerService.listTimer(map[string]interface{}{})
	ass.NoError(err)
	ass.Equal(map[string]interface{}{
		"aaa": map[string]interface{}{
			"item_values": []string{},
			"times":       []timer.TimeEntry{},
			"enabled":     true,
		},
	}, data)

	data, err = timerService.getTimer(map[string]interface{}{
		"key": "aaa",
	})
	ass.NoError(err)
	ass.Equal(map[string]interface{}{
		"item_values": []string{},
		"times":       []timer.TimeEntry{},
		"enabled":     true,
	}, data)

	_, err = timerService.getTimer(map[string]interface{}{
		"key": "bbb",
	})
	ass.EqualError(err, "no timer with key bbb")

	// remove
	_, err = timerService.removeTimer(map[string]interface{}{
		"key": "aaa",
	})
	ass.NoError(err)

	_, err = timerService.removeTimer(map[string]interface{}{
		"key": "aaa",
	})
	ass.EqualError(err, "no timer with key aaa")
}

func Test_convertTime(t *testing.T) {
	ass := assert.New(t)

	ass.Equal(5*60*60, convertTime("05:00"))
	ass.Equal(5*60, convertTime("00:05:00"))
	ass.Equal(1234, convertTime("1234"))
}

func TestTimerService_handler_TimeEntry(t *testing.T) {
	ass := assert.New(t)

	bus := core.CreateBus()
	defer bus.Shutdown()

	timerService := new(TimerService)

	ti := timerService.newTimer("aaa", nil, nil)
	ti.SetEventTrigger(func(id string, data map[string]interface{}) {})
	timerService.timers = map[string]*Timer{"aaa": ti}

	ti.Start()
	defer ti.Stop()

	// add
	_, err := timerService.addTimeEntry(map[string]interface{}{
		"key": "bbb",
	})
	ass.EqualError(err, "no timer with key bbb")

	_, err = timerService.addTimeEntry(map[string]interface{}{
		"key":    "aaa",
		"active": true,
		"time":   42,
		"repeat": 2,
		"state":  "test",
	})
	ass.NoError(err)

	ass.Len(ti.ListEntries(), 1)
	ass.Equal(timer.TimeEntry{
		Active: true,
		Time:   42,
		Repeat: 2,
		State:  "test",
	}, ti.ListEntries()[0])

	// edit
	_, err = timerService.editTimeEntry(map[string]interface{}{
		"key": "bbb",
	})
	ass.EqualError(err, "no timer with key bbb")

	_, err = timerService.editTimeEntry(map[string]interface{}{
		"key": "aaa",
		"idx": 1,
	})
	ass.EqualError(err, "invalid time entry index 1")

	_, err = timerService.editTimeEntry(map[string]interface{}{
		"key": "aaa",
		"idx": 0,

		"active": false,
		"time":   32,
		"repeat": 3,
		"state":  "bbb",
	})
	ass.NoError(err)

	ass.Len(ti.ListEntries(), 1)
	ass.Equal(timer.TimeEntry{
		Active: false,
		Time:   32,
		Repeat: 3,
		State:  "bbb",
	}, ti.ListEntries()[0])

	// remove
	_, err = timerService.removeTimeEntry(map[string]interface{}{
		"key": "bbb",
	})
	ass.EqualError(err, "no timer with key bbb")

	_, err = timerService.removeTimeEntry(map[string]interface{}{
		"key": "aaa",
		"idx": 1,
	})
	ass.EqualError(err, "invalid time entry index 1")

	_, err = timerService.removeTimeEntry(map[string]interface{}{
		"key": "aaa",
		"idx": 0,
	})
	ass.NoError(err)
	ass.Len(ti.ListEntries(), 0)
}

func TestTimerService_handler_ItemValues(t *testing.T) {
	ass := assert.New(t)

	bus := core.CreateBus()
	defer bus.Shutdown()

	timerService := new(TimerService)

	ti := timerService.newTimer("aaa", nil, nil)
	ti.SetEventTrigger(func(id string, data map[string]interface{}) {})
	timerService.timers = map[string]*Timer{"aaa": ti}

	ti.Start()
	defer ti.Stop()

	// add
	_, err := timerService.addItemValue(map[string]interface{}{
		"key": "bbb",
	})
	ass.EqualError(err, "no timer with key bbb")

	_, err = timerService.addItemValue(map[string]interface{}{
		"key":        "aaa",
		"item_value": "test",
	})
	ass.NoError(err)

	ass.Equal([]string{"test"}, ti.itemValues)

	// remove
	_, err = timerService.removeItemValue(map[string]interface{}{
		"key": "bbb",
	})
	ass.EqualError(err, "no timer with key bbb")

	_, err = timerService.removeItemValue(map[string]interface{}{
		"key":        "aaa",
		"item_value": "test",
	})
	ass.NoError(err)
	ass.Len(ti.itemValues, 0)
}
