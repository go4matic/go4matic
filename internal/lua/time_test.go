// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package lua

import (
	"testing"
	"time"

	"github.com/spf13/cast"
	"github.com/stretchr/testify/assert"
)

func TestTimeModule(t *testing.T) {
	ass := assert.New(t)

	module := new(TimeModule)
	ass.Equal("time", module.Name())
	ass.Len(module.Def().Functions, 1)
}

func Test_unix(t *testing.T) {
	ass := assert.New(t)

	module := new(TimeModule)
	response := module.unix(nil, nil)
	timestamp1 := time.Now().Unix()
	timestamp2 := cast.ToInt64(response[0])

	ass.True(timestamp1 == timestamp2 || timestamp1+1 == timestamp2)
}
