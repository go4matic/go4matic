// Copyright 2020 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package lua

import (
	"time"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// TimeModule for basic time functions
type TimeModule struct {
}

// Name of module
func (m *TimeModule) Name() string {
	return "time"
}

// Def return the definition of this module
func (m *TimeModule) Def() core.ScriptModuleDefinition {
	return core.ScriptModuleDefinition{
		Description: assets.L.Get("Time script functionality"),
		Functions: map[string]core.ScriptFunction{
			"unix": {
				Def: api.Function{
					Description: assets.L.Get("Returns unix timestamp"),
					Response: api.FieldList{{
						Description: assets.L.Get("Seconds since 1970-01-01"),
						Type:        api.Int64,
					}},
				},
				Func: m.unix,
			},
		},
	}
}

// unix timestamp
func (m *TimeModule) unix(_ core.Script, _ []interface{}) []interface{} {
	return []interface{}{time.Now().Unix()}
}
