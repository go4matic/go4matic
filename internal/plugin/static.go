// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package plugin

import (
	"gitlab.com/go4matic/go4matic/pkg/core"
	"gitlab.com/go4matic/go4matic/plugins/dummy"
	"gitlab.com/go4matic/go4matic/plugins/homematic"
	"gitlab.com/go4matic/go4matic/plugins/hue"
	"gitlab.com/go4matic/go4matic/plugins/influxdb"
	"gitlab.com/go4matic/go4matic/plugins/mqtt"
	"gitlab.com/go4matic/go4matic/plugins/sma"
	"gitlab.com/go4matic/go4matic/plugins/weather_underground"
)

// TODO disable static plugins with build tag

var staticPlugins = []core.Plugin{
	&dummy.Plugin{},
	&hue.Plugin{},
	&homematic.Plugin{},
	&sma.Plugin{},
	&influxdb.Plugin{},
	&mqtt.Plugin{},
	&weather_underground.Plugin{},
}

// StaticPlugins returns list of static plugins
func StaticPlugins() []core.Plugin {
	return staticPlugins
}
