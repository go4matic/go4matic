// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// +build dynplugin

package plugin

import (
	"errors"
	"path/filepath"
	"plugin"

	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/pkg/core"
)

// loadDynPlugins at runtime from plugin directory
func loadDynPlugins() (map[string]core.Plugin, error) {
	// get list of existing pluginList
	pluginPaths, err := filepath.Glob("pluginList/*.so")
	if err != nil {
		return nil, err
	}
	pluginList := make(map[string]core.Plugin, len(pluginPaths))

	for _, path := range pluginPaths {
		zap.L().Debug("Load " + path)
		// load plugin
		p, err := plugin.Open(path)
		if err != nil {
			return nil, err
		}

		// get instance of plugin class
		pluginSymbol, err := p.Lookup("P")
		if err != nil {
			zap.S().Errorf("%+v\n", p)
			return nil, err
		}

		pluginInstance, ok := pluginSymbol.(core.Plugin)
		if !ok {
			return nil, errors.New(assets.L.Get("%s is not a valid plugin", path))
		}

		zap.L().Info(assets.L.Get("Loaded plugin %s in Version %s",
			pluginInstance.ID(), pluginInstance.Version()))

		if _, ok := pluginList[pluginInstance.ID()]; ok {
			return nil, errors.New(
				assets.L.Get("Plugin with ID %s already loaded", pluginInstance.ID()))
		}

		pluginList[pluginInstance.ID()] = pluginInstance
	}

	return pluginList, nil
}
