// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package plugin

import (
	"errors"
	"fmt"
	"sync"

	"github.com/spf13/cast"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// information related to a plugin
type pluginInfo struct {
	bus    core.Bus
	plugin core.Plugin

	// definition of config
	configDef *core.PluginConfigDef

	// config data or nil if no config interface
	config *core.PluginConfig

	// mutex to synchronise access to config from API
	mutex sync.RWMutex
}

// init plugin and load config if defined
func (p *pluginInfo) init(bus core.Bus) error {
	p.bus = bus

	err := p.plugin.Init(bus)
	if err != nil {
		return err
	}

	if cp, ok := p.plugin.(core.ConfigPlugin); ok {
		p.configDef = cp.Config()

		if err := p.configDef.Validate(); err != nil {
			return err
		}

		value, err := core.ConfigRead(bus, fmt.Sprintf("plugin.%s", p.plugin.ID()))
		if err != nil {
			return err
		}

		p.config, err = p.configDef.Load(value)
		if err != nil {
			return err
		}
	}
	return nil
}

// info for plugin
func (p *pluginInfo) info() map[string]interface{} {
	data := map[string]interface{}{
		"id":      p.plugin.ID(),
		"version": p.plugin.Version(),
	}

	if p.configDef != nil {
		data["config"] = p.configDef
	}
	return data
}

// save config to config service
func (p *pluginInfo) save() {
	p.mutex.RLock()
	defer p.mutex.RUnlock()

	err := core.ConfigWrite(p.bus, fmt.Sprintf("plugin.%s", p.plugin.ID()), p.config.Dump())
	if err != nil {
		zap.L().Error(assets.L.Get("failed to save plugin config: %v", err))
	}
}

// #####################################################################################################################

// helper to change/update values
func (p *pluginInfo) changedValues(fields api.FieldMap, config map[string]interface{},
	param map[string]interface{}, handler func(changes map[string]interface{}) error) error {

	// add values from config to parameter that are not given
	for k := range fields {
		if _, ok := param[k]; !ok {
			param[k] = config[k]
		}
	}

	values, err := fields.Validate(param)
	if err != nil {
		return err
	}

	// get only changed values
	changedValues := make(map[string]interface{}, len(fields))
	for k := range fields {
		if values[k] != config[k] {
			changedValues[k] = param[k]
		}
	}

	if len(changedValues) == 0 {
		return errors.New(assets.L.Get("no values changed"))
	}

	err = handler(changedValues)
	if err != nil {
		return err
	}

	for k, v := range changedValues {
		config[k] = v
	}

	go p.save()
	return nil
}

// get global config
func (p *pluginInfo) handlerConfigGet(param map[string]interface{}) (map[string]interface{}, error) {
	p.mutex.RLock()
	defer p.mutex.RUnlock()

	return p.config.Config, nil
}

// returns handler to change global config
func (p *pluginInfo) handlerConfigChange(param map[string]interface{}) (map[string]interface{}, error) {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	return nil, p.changedValues(p.configDef.Config, p.config.Config, param, p.configDef.ChangeFunc)
}

// return list of existing entries
func (p *pluginInfo) handlerEntryList(param map[string]interface{}) (map[string]interface{}, error) {
	p.mutex.RLock()
	defer p.mutex.RUnlock()

	entry, ok := p.config.Entries[cast.ToString(param["entry_type"])]
	if !ok {
		return nil, errors.New(assets.L.Get("invalid entry type for plugin %s", p.plugin.ID()))
	}

	data := make(map[string]interface{}, len(entry))
	for k, e := range entry {
		data[k] = e
	}
	return data, nil
}

// get information of specific entry
func (p *pluginInfo) handlerEntryGet(param map[string]interface{}) (map[string]interface{}, error) {
	p.mutex.RLock()
	defer p.mutex.RUnlock()

	entry, ok := p.config.Entries[cast.ToString(param["entry_type"])]
	if !ok {
		return nil, errors.New(assets.L.Get("invalid entry type for plugin %s", p.plugin.ID()))
	}

	config, ok := entry[cast.ToString(param["entry_key"])]
	if !ok {
		return nil, errors.New(assets.L.Get("entry not found for plugin %s", p.plugin.ID()))
	}

	return config, nil
}

// add entry to config
func (p *pluginInfo) handlerEntryAdd(param map[string]interface{}) (map[string]interface{}, error) {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	entryType := cast.ToString(param["entry_type"])
	entry, ok := p.config.Entries[entryType]
	if !ok {
		return nil, errors.New(assets.L.Get("invalid entry type for plugin %s", p.plugin.ID()))
	}

	entryKey := cast.ToString(param["entry_key"])
	_, ok = entry[entryKey]
	if ok {
		return nil, errors.New(assets.L.Get("entry with key %v already exist", param["entry_key"]))
	}

	delete(param, "entry_type")
	delete(param, "entry_key")
	err := p.configDef.Entries[entryType].AddEntryFunc(entryKey, param)
	if err != nil {
		return nil, err
	}

	entry[entryKey] = param

	go p.save()
	return nil, nil
}

// remove entry
func (p *pluginInfo) handlerEntryDelete(param map[string]interface{}) (map[string]interface{}, error) {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	entryType := cast.ToString(param["entry_type"])
	entry, ok := p.config.Entries[entryType]
	if !ok {
		return nil, errors.New(assets.L.Get("invalid entry type for plugin %s", p.plugin.ID()))
	}

	entryKey := cast.ToString(param["entry_key"])
	_, ok = entry[entryKey]
	if !ok {
		return nil, errors.New(assets.L.Get("entry with key %v does not exist", param["entry_key"]))
	}

	err := p.configDef.Entries[entryType].RemoveEntryFunc(entryKey)
	if err != nil {
		return nil, err
	}

	delete(entry, entryKey)

	go p.save()
	return nil, nil
}

// change entry config
func (p *pluginInfo) handlerEntryChange(param map[string]interface{}) (map[string]interface{}, error) {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	entryType := cast.ToString(param["entry_type"])
	entry, ok := p.config.Entries[entryType]
	if !ok {
		return nil, errors.New(assets.L.Get("invalid entry type for plugin %s", p.plugin.ID()))
	}

	entryKey := cast.ToString(param["entry_key"])
	config, ok := entry[entryKey]
	if !ok {
		return nil, errors.New(assets.L.Get("entry with key %v does not exist", param["entry_key"]))
	}

	delete(param, "entry_type")
	delete(param, "entry_key")
	entryDef := p.configDef.Entries[entryType]

	// custom change function given ?
	if entryDef.ChangeEntryFunc != nil {
		return nil, p.changedValues(entryDef.Config, cast.ToStringMap(config), param,
			func(changes map[string]interface{}) error {
				return entryDef.ChangeEntryFunc(entryKey, changes)
			})
	}
	// fallback to remove and recreate

	// add values from config to parameter that are not given
	for k := range entryDef.Config {
		if _, ok := param[k]; !ok {
			param[k] = config[k]
		}
	}

	values, err := entryDef.Config.Validate(param)
	if err != nil {
		return nil, err
	}

	err = p.configDef.Entries[entryType].RemoveEntryFunc(entryKey)
	if err != nil {
		return nil, err
	}

	err = p.configDef.Entries[entryType].AddEntryFunc(entryKey, values)
	if err != nil {
		return nil, err
	}

	entry[entryKey] = values
	go p.save()
	return nil, nil
}

// #####################################################################################################################

// get list of path entries to interact with the config
func (p *pluginInfo) configEndpoints() []core.PathEntry {
	cp, ok := p.plugin.(core.ConfigPlugin)
	if !ok {
		return []core.PathEntry{}
	}

	configEntry := core.PathEntry{
		Path:       "/config/",
		SubEntries: make([]core.PathEntry, 0),
	}

	config := cp.Config()
	if config.Config != nil {
		configEntry.SubEntries = append(configEntry.SubEntries,
			core.SimplePathEntry{
				Def: api.SimpleHandler{
					Description: assets.L.Get("Get configuration for plugin %s", cp.ID()),
					Response:    config.Config,
				},
				Handler: p.handlerConfigGet,
			}.PathEntry(),
			core.SimplePathEntry{
				Def: api.SimpleHandler{
					Description: assets.L.Get("Set configuration for plugin %s", cp.ID()),
					Parameters:  config.Config,
				},
				Method:  "PUT",
				Handler: p.handlerConfigChange,
			}.PathEntry(),
		)
	}

	for _, entry := range p.configEntriesEndpoints() {
		configEntry.SubEntries = append(configEntry.SubEntries, entry.PathEntry())
	}
	return []core.PathEntry{configEntry}
}

// entryWrapper adds entry_type to parameters
func entryWrapper(entryType string, handler core.SimplePathEntryHandler) core.SimplePathEntryHandler {
	return func(param map[string]interface{}) (map[string]interface{}, error) {
		param["entry_type"] = entryType
		return handler(param)
	}
}

// appendEntryKey to parameters
func appendEntryKey(fieldsOrg api.FieldMap) api.FieldMap {
	fields := api.FieldMap{}
	for key, field := range fieldsOrg {
		fields[key] = field
	}

	fields["entry_key"] = api.Field{
		Description: assets.L.Get("Key of entry"),
		Type:        api.String,
		Required:    true,
	}
	return fields
}

// get endpoints of the given entries
func (p *pluginInfo) configEntriesEndpoints() []core.SimplePathEntry {
	entries := make([]core.SimplePathEntry, 0)
	for key, entry := range p.configDef.Entries {
		entries = append(entries,

			core.SimplePathEntry{
				Def: api.SimpleHandler{
					Description: assets.L.Get(
						"Get configuration for plugin %s - entry %s", p.plugin.ID(), key),
					Response: api.FieldMap{
						"{key}": {
							Description: assets.L.Get("List of entries"),
							Type:        entry.Config,
						},
					},
				},
				Path:    "/" + key,
				Handler: entryWrapper(key, p.handlerEntryList),
			},
			core.SimplePathEntry{
				Def: api.SimpleHandler{
					Description: assets.L.Get(
						"Get configuration for plugin %s - entry %s", p.plugin.ID(), key),
					Parameters: appendEntryKey(nil),
					Response:   entry.Config,
				},

				Path:    "/" + key + "/:entry_key",
				Handler: entryWrapper(key, p.handlerEntryGet),
			},

			core.SimplePathEntry{
				Def: api.SimpleHandler{
					Description: assets.L.Get(
						"Add entry for plugin %s - entry %s", p.plugin.ID(), key),
					Parameters: appendEntryKey(entry.Config),
				},

				Method:  "POST",
				Path:    "/" + key,
				Handler: entryWrapper(key, p.handlerEntryAdd),
			},
			core.SimplePathEntry{
				Def: api.SimpleHandler{
					Description: assets.L.Get(
						"Remove entry for plugin %s - entry %s", p.plugin.ID(), key),
					Parameters: appendEntryKey(nil),
				},

				Method:  "DELETE",
				Path:    "/" + key + "/:entry_key",
				Handler: entryWrapper(key, p.handlerEntryDelete),
			},
		)

		// optional: change of existing entries
		// if not set fallback to remove and recreate
		entries = append(entries,
			core.SimplePathEntry{
				Def: api.SimpleHandler{
					Description: assets.L.Get(
						"Change configuration for plugin %s - entry %s", p.plugin.ID(), key),
					Parameters: appendEntryKey(entry.Config),
				},

				Method:  "PUT",
				Path:    "/" + key + "/:entry_key",
				Handler: entryWrapper(key, p.handlerEntryChange),
			},
		)
	}
	return entries
}
