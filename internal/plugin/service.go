// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package plugin

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/spf13/cast"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// Loader for plugins
type Loader struct {
	// list of loaded plugins
	plugins map[string]*pluginInfo
}

// Key that identifies this service
func (l *Loader) Key() string {
	return "core.plugins"
}

// Load all plugins
func (l *Loader) Load(b core.Bus) {
	// try to load dynamic plugins
	plugins, err := loadDynPlugins()
	if err != nil {
		zap.L().Fatal(err.Error())
	}

	// load static plugins
	for _, p := range StaticPlugins() {
		id := p.ID()

		// skip if already loaded
		if _, ok := l.plugins[id]; ok {
			zap.L().Warn(assets.L.Get(
				"Plugin %s already loaded - static version skipped", id))
			continue
		}

		zap.L().Info(assets.L.Get("Loaded static plugin %s in Version %s",
			id, p.Version()))

		plugins[id] = p
	}

	// initialize the plugins
	l.plugins = make(map[string]*pluginInfo, len(plugins))
	for id, p := range plugins {
		info := pluginInfo{plugin: p}

		zap.L().Debug(assets.L.Get("Init for plugin %s", id))
		err := info.init(b)
		if err != nil {
			zap.S().Fatalf("Plugin %s: %v", id, err)
		}

		l.plugins[id] = &info
	}
}

// constants for possible handler functions
type handlerFunction int

const (
	handlerConfigGet handlerFunction = iota + 1
	handlerConfigChange
	handlerEntryList
	handlerEntryGet
	handlerEntryAdd
	handlerEntryChange
	handlerEntryDelete
)

// pluginHandler creates handler based on selected function
func (l *Loader) pluginHandler(function handlerFunction) core.SimplePathEntryHandler {
	return func(param map[string]interface{}) (map[string]interface{}, error) {
		info, ok := l.plugins[cast.ToString(param["plugin_id"])]
		if !ok {
			return nil, errors.New(assets.L.Get("no plugin with ID %v", param["plugin_id"]))
		}
		delete(param, "plugin_id")

		switch function {
		case handlerConfigGet:
			return info.handlerConfigGet(param)
		case handlerConfigChange:
			return info.handlerConfigChange(param)
		case handlerEntryList:
			return info.handlerEntryList(param)
		case handlerEntryGet:
			return info.handlerEntryGet(param)
		case handlerEntryAdd:
			return info.handlerEntryAdd(param)
		case handlerEntryChange:
			return info.handlerEntryChange(param)
		case handlerEntryDelete:
			return info.handlerEntryDelete(param)
		default:
			return nil, fmt.Errorf("unknown function %d", function)
		}
	}
}

// handlerPluginList lists all plugins
func (l *Loader) handlerPluginList(param map[string]interface{}) (map[string]interface{}, error) {
	pluginList := make(map[string]interface{}, len(l.plugins))

	for id, info := range l.plugins {
		pluginList[id] = info.info()
	}
	return pluginList, nil
}

// handlerPluginGet returns plugin information
func (l *Loader) handlerPluginGet(param map[string]interface{}) (map[string]interface{}, error) {
	info, ok := l.plugins[cast.ToString(param["plugin_id"])]
	if !ok {
		return nil, errors.New(assets.L.Get("no plugin with ID %s", param["plugin_id"]))
	}

	return info.info(), nil
}

// Handlers for this service
func (l *Loader) Handlers() map[string]core.RequestHandler {
	pluginData := api.FieldMap{
		"id": {
			Type:        api.String,
			Description: assets.L.Get("ID of plugin"),
			Required:    true,
		},
		"version": {
			Type:        api.String,
			Description: assets.L.Get("Version of plugin"),
			Required:    true,
		},
		"config": {
			Description: assets.L.Get("Configuration structure of plugin"),
		},
	}

	return map[string]core.RequestHandler{
		"list": {
			Func: l.handlerPluginList,
			Def: api.SimpleHandler{
				Description: assets.L.Get("List existing plugins"),
				Response: api.FieldMap{
					"{id}": {
						Description: assets.L.Get("Plugin information"),
						Type:        pluginData,
					},
				},
			},
		},
		"get_info": {
			Func: l.handlerPluginGet,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Plugin information"),
				Parameters: api.FieldMap{
					"plugin_id": {
						Type:        api.String,
						Description: assets.L.Get("Id of plugin"),
						Required:    true,
					},
				},
				Response: pluginData,
			},
		},

		"config_get": {
			Func: l.pluginHandler(handlerConfigGet),
			Def: api.SimpleHandler{
				Description: assets.L.Get("Get plugin configuration"),
				Parameters: api.FieldMap{
					"plugin_id": {
						Type:        api.String,
						Description: assets.L.Get("Id of plugin"),
						Required:    true,
					},
				},
				Response: api.FieldMap{
					"{name}": {
						Description: assets.L.Get("Config values"),
						Required:    true,
					},
				},
			},
		},
		"config_change": {
			Func: l.pluginHandler(handlerConfigChange),
			Def: api.SimpleHandler{
				Description: assets.L.Get("Get plugin configuration"),
				Parameters: api.FieldMap{
					"plugin_id": {
						Type:        api.String,
						Description: assets.L.Get("Id of plugin"),
						Required:    true,
					},
					"{name}": {
						Description: assets.L.Get("Config values"),
						Required:    true,
					},
				},
			},
		},

		"config_entry_list": {
			Func: l.pluginHandler(handlerEntryList),
			Def: api.SimpleHandler{
				Description: assets.L.Get("List config entries"),
				Parameters: api.FieldMap{
					"plugin_id": {
						Type:        api.String,
						Description: assets.L.Get("Id of plugin"),
						Required:    true,
					},
					"entry_type": {
						Type:        api.String,
						Description: assets.L.Get("Name of entry type"),
						Required:    true,
					},
				},
				Response: api.FieldMap{
					"{id}": {
						Description: assets.L.Get("Config of entry"),
						Required:    true,
						Type: api.FieldMap{
							"{name}": {
								Description: assets.L.Get("Config values"),
								Required:    true,
							},
						},
					},
				},
			},
		},
		"config_entry_get": {
			Func: l.pluginHandler(handlerEntryGet),
			Def: api.SimpleHandler{
				Description: assets.L.Get("Get config of entry"),
				Parameters: api.FieldMap{
					"plugin_id": {
						Type:        api.String,
						Description: assets.L.Get("Id of plugin"),
						Required:    true,
					},
					"entry_type": {
						Type:        api.String,
						Description: assets.L.Get("Name of entry type"),
						Required:    true,
					},
					"entry_key": {
						Type:        api.String,
						Description: assets.L.Get("Key of entry"),
						Required:    true,
					},
				},
				Response: api.FieldMap{
					"{name}": {
						Description: assets.L.Get("Config values"),
						Required:    true,
					},
				},
			},
		},
		"config_entry_add": {
			Func: l.pluginHandler(handlerEntryAdd),
			Def: api.SimpleHandler{
				Description: assets.L.Get("Add entry"),
				Parameters: api.FieldMap{
					"plugin_id": {
						Type:        api.String,
						Description: assets.L.Get("Id of plugin"),
						Required:    true,
					},
					"entry_type": {
						Type:        api.String,
						Description: assets.L.Get("Name of entry type"),
						Required:    true,
					},
					"entry_key": {
						Type:        api.String,
						Description: assets.L.Get("Key of entry"),
						Required:    true,
					},
					"{name}": {
						Description: assets.L.Get("Config values"),
						Required:    true,
					},
				},
			},
		},
		"config_entry_change": {
			Func: l.pluginHandler(handlerEntryChange),
			Def: api.SimpleHandler{
				Description: assets.L.Get("Change entry config"),
				Parameters: api.FieldMap{
					"plugin_id": {
						Type:        api.String,
						Description: assets.L.Get("Id of plugin"),
						Required:    true,
					},
					"entry_type": {
						Type:        api.String,
						Description: assets.L.Get("Name of entry type"),
						Required:    true,
					},
					"entry_key": {
						Type:        api.String,
						Description: assets.L.Get("Key of entry"),
						Required:    true,
					},
					"{name}": {
						Description: assets.L.Get("Config values"),
						Required:    true,
					},
				},
			},
		},
		"config_entry_remove": {
			Func: l.pluginHandler(handlerEntryDelete),
			Def: api.SimpleHandler{
				Description: assets.L.Get("Remove entry"),
				Parameters: api.FieldMap{
					"plugin_id": {
						Type:        api.String,
						Description: assets.L.Get("Id of plugin"),
						Required:    true,
					},
					"entry_type": {
						Type:        api.String,
						Description: assets.L.Get("Name of entry type"),
						Required:    true,
					},
					"entry_key": {
						Type:        api.String,
						Description: assets.L.Get("Key of entry"),
						Required:    true,
					},
				},
			},
		},
	}
}

// APIEndpoints returns API endpoints of plugins
func (l *Loader) APIEndpoints() []core.PathEntry {
	pluginEndpoints := make([]core.PathEntry, 0, len(l.plugins))

	// plugin specific endpoints
	for id, info := range l.plugins {
		entry := core.PathEntry{
			Def: api.ExtendedHandler{
				Description: assets.L.Get("Get information about %s plugin", id),
				Responses: map[string]api.Response{
					"200": {
						Description: assets.L.Get("Plugin information"),
						Data: api.FieldMap{
							"id": {
								Type:        api.String,
								Description: assets.L.Get("ID of plugin"),
								Required:    true,
							},
							"version": {
								Type:        api.String,
								Description: assets.L.Get("Version of plugin"),
								Required:    true,
							},
						},
					},
				},
			},
			Path:    "/" + id + "/",
			Handler: l.apiGet(info),
			Group:   "Plugin " + id,
		}

		entry.SubEntries = info.configEndpoints()
		if apiPlugin, ok := info.plugin.(core.APIPlugin); ok {
			entry.SubEntries = append(entry.SubEntries,
				apiPlugin.APIEndpoints()...)
		}
		entry.SubEntries = setPluginEndpointGroup(id, entry.SubEntries)

		pluginEndpoints = append(pluginEndpoints, entry)
	}

	return []core.PathEntry{{
		Def: api.ExtendedHandler{
			Description: assets.L.Get("List existing plugins"),
			Responses: map[string]api.Response{
				"200": {
					Description: assets.L.Get("Map with plugin information"),
					Data: api.FieldMap{
						"{id}": {
							Description: assets.L.Get("Plugin information"),
							Type: api.FieldMap{
								"id": {
									Type:        api.String,
									Description: assets.L.Get("ID of plugin"),
									Required:    true,
								},
								"version": {
									Type:        api.String,
									Description: assets.L.Get("Version of plugin"),
									Required:    true,
								},
							},
						},
					},
				},
			},
		},
		Path:       "/plugins/",
		Handler:    l.apiList,
		SubEntries: pluginEndpoints,
		Group:      "Core",
	}}
}

// WebEndpoints returns web endpoints of plugins
func (l *Loader) WebEndpoints() []core.PathEntry {
	endpoints := make([]core.PathEntry, 0)

	// plugin specific endpoints
	for _, info := range l.plugins {
		webPlugin, ok := info.plugin.(core.WebPlugin)
		if !ok {
			continue
		}

		endpoints = append(endpoints, webPlugin.WebEndpoints()...)
	}
	return endpoints
}

// Shutdown plugins
func (l *Loader) Shutdown() {
	for key, info := range l.plugins {
		plugin, ok := info.plugin.(core.ShutdownPlugin)
		if ok {
			zap.L().Debug(assets.L.Get("Stopping plugin %s ...", key))
			plugin.Shutdown()
			zap.L().Debug(assets.L.Get("Plugin %s stopped.", key))
		}
	}
}

// apiList handle plugin list request
func (l *Loader) apiList(ctx core.HTTPContext) {
	data, _ := l.handlerPluginList(nil)
	ctx.JSON(http.StatusOK, data)
}

// apiGet returns handler plugin get request
func (l *Loader) apiGet(info *pluginInfo) func(ctx core.HTTPContext) {
	return func(ctx core.HTTPContext) {
		ctx.JSON(http.StatusOK, info.info())
	}
}

// setPluginEndpointGroup sets plugin group for all entries and sub entries
func setPluginEndpointGroup(plugin string, entries []core.PathEntry) []core.PathEntry {
	for idx := range entries {
		if entries[idx].Group == "" {
			entries[idx].Group = "Plugin " + plugin
		} else {
			entries[idx].Group = "Plugin " + plugin + " - " + entries[idx].Group
		}
		if entries[idx].SubEntries != nil {
			entries[idx].SubEntries = setPluginEndpointGroup(plugin, entries[idx].SubEntries)
		}
	}
	return entries
}
