// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package plugin

import (
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

type testPlugin struct {
	err    bool
	config *core.PluginConfigDef
}

func (p *testPlugin) ID() string {
	return "test"
}

func (p *testPlugin) Version() string {
	return "1.2.3"
}

func (p *testPlugin) Init(b core.Bus) error {
	if p.err {
		return errors.New("test")
	}
	return nil
}

func (p *testPlugin) Config() *core.PluginConfigDef {
	if p.config != nil {
		return p.config
	}
	return &core.PluginConfigDef{
		Config: api.FieldMap{},
		Entries: core.PluginConfigEntryDefMap{
			"aaa": {},
		},
	}
}

func TestPluginInfo_init(t *testing.T) {
	ass := assert.New(t)

	bus := core.CreateBus()
	defer bus.Shutdown()

	plugin := &testPlugin{
		err: true,
		config: &core.PluginConfigDef{
			Config: api.FieldMap{
				"aaa": {},
			},
		},
	}
	info := &pluginInfo{
		plugin: plugin,
	}

	err := info.init(bus)
	ass.EqualError(err, "test")

	plugin.err = false
	err = info.init(bus)
	ass.EqualError(err, "missing LoadFunc")

	plugin.config = &core.PluginConfigDef{}
	err = info.init(bus)

	// no config service-> load failed
	ass.EqualError(err, "no service for core.config")
	// TODO: check if load is working -> requires config service
}

func TestPluginInfo_changedValues(t *testing.T) {
	ass := assert.New(t)

	bus := core.CreateBus()
	defer bus.Shutdown()

	info := &pluginInfo{
		bus:    bus,
		plugin: &testPlugin{},
		config: &core.PluginConfig{},
	}

	fields := api.FieldMap{
		"aaa": {
			Type:     api.Int,
			Required: true,
		},
	}

	config := map[string]interface{}{
		"aaa": 42,
	}

	param := map[string]interface{}{
		"aaa": "test",
	}
	err := info.changedValues(fields, config, param, nil)
	ass.EqualError(err, "invalid value type for field \"aaa\"")

	param = map[string]interface{}{}
	err = info.changedValues(fields, config, param, nil)
	ass.EqualError(err, "no values changed")

	param = map[string]interface{}{
		"aaa": 32,
	}
	err = info.changedValues(fields, config, param, func(changes map[string]interface{}) error {
		ass.Equal(param, changes)
		return errors.New("test")
	})
	ass.EqualError(err, "test")

	err = info.changedValues(fields, config, param, func(changes map[string]interface{}) error {
		ass.Equal(param, changes)
		return nil
	})
	ass.NoError(err)

	time.Sleep(time.Millisecond * 100)
}
func TestPluginInfo_handlerConfigGet(t *testing.T) {
	ass := assert.New(t)

	info := &pluginInfo{
		config: &core.PluginConfig{
			Config: map[string]interface{}{
				"aaa": 42,
			},
		},
	}

	data, err := info.handlerConfigGet(nil)
	ass.NoError(err)
	ass.Equal(info.config.Config, data)
}
func TestPluginInfo_handlerConfigChange(t *testing.T) {
	ass := assert.New(t)

	info := &pluginInfo{
		config: &core.PluginConfig{
			Config: map[string]interface{}{
				"aaa": 42,
			},
		},
		configDef: &core.PluginConfigDef{
			Config: api.FieldMap{
				"aaa": {
					Type:     api.Int,
					Required: true,
				},
			},
		},
	}

	_, err := info.handlerConfigChange(map[string]interface{}{"aaa": 42})
	ass.EqualError(err, "no values changed")
}
func TestPluginInfo_handlerEntryAdd(t *testing.T) {
	ass := assert.New(t)

	bus := core.CreateBus()
	defer bus.Shutdown()

	info := pluginInfo{
		bus:    bus,
		plugin: &testPlugin{},
		config: &core.PluginConfig{
			Entries: map[string]map[string]map[string]interface{}{
				"a": {
					"aaa": {},
				},
			},
		},
		configDef: &core.PluginConfigDef{
			Entries: core.PluginConfigEntryDefMap{
				"a": {},
			},
		},
	}

	_, err := info.handlerEntryAdd(map[string]interface{}{
		"entry_type": "b",
		"entry_key":  "aaa",
		"aaa":        42,
	})
	ass.EqualError(err, "invalid entry type for plugin test")

	_, err = info.handlerEntryAdd(map[string]interface{}{
		"entry_type": "a",
		"entry_key":  "aaa",
		"aaa":        42,
	})
	ass.EqualError(err, "entry with key aaa already exist")

	info.config.Entries = map[string]map[string]map[string]interface{}{
		"a": {},
	}
	info.configDef.Entries = core.PluginConfigEntryDefMap{
		"a": {
			AddEntryFunc: func(key string, config map[string]interface{}) error {
				ass.Equal("aaa", key)
				ass.Equal(map[string]interface{}{
					"aaa": 42,
				}, config)
				return errors.New("test")
			},
		},
	}

	_, err = info.handlerEntryAdd(map[string]interface{}{
		"entry_type": "a",
		"entry_key":  "aaa",
		"aaa":        42,
	})
	ass.EqualError(err, "test")

	info.configDef.Entries = core.PluginConfigEntryDefMap{
		"a": {
			AddEntryFunc: func(key string, config map[string]interface{}) error {
				ass.Equal("aaa", key)
				ass.Equal(map[string]interface{}{
					"aaa": 42,
				}, config)
				return nil
			},
		},
	}
	_, err = info.handlerEntryAdd(map[string]interface{}{
		"entry_type": "a",
		"entry_key":  "aaa",
		"aaa":        42,
	})
	ass.NoError(err)

}

func TestPluginInfo_handlerEntryList(t *testing.T) {
	ass := assert.New(t)

	info := pluginInfo{
		plugin: &testPlugin{},
		config: &core.PluginConfig{
			Entries: map[string]map[string]map[string]interface{}{
				"a": {
					"aaa": {
						"test": 42,
					},
				},
			},
		},
		configDef: &core.PluginConfigDef{
			Entries: core.PluginConfigEntryDefMap{
				"a": {},
			},
		},
	}

	_, err := info.handlerEntryList(map[string]interface{}{
		"entry_type": "b",
	})
	ass.EqualError(err, "invalid entry type for plugin test")

	data, err := info.handlerEntryList(map[string]interface{}{
		"entry_type": "a",
	})
	ass.NoError(err)
	ass.Equal(map[string]interface{}{
		"aaa": map[string]interface{}{
			"test": 42,
		},
	}, data)
}

func TestPluginInfo_handlerEntryGet(t *testing.T) {
	ass := assert.New(t)

	info := pluginInfo{
		plugin: &testPlugin{},
		config: &core.PluginConfig{
			Entries: map[string]map[string]map[string]interface{}{
				"a": {
					"aaa": {
						"test": 42,
					},
				},
			},
		},
		configDef: &core.PluginConfigDef{
			Entries: core.PluginConfigEntryDefMap{
				"a": {},
			},
		},
	}

	_, err := info.handlerEntryGet(map[string]interface{}{
		"entry_type": "b",
		"entry_key":  "aaa",
	})
	ass.EqualError(err, "invalid entry type for plugin test")

	_, err = info.handlerEntryGet(map[string]interface{}{
		"entry_type": "a",
		"entry_key":  "bbb",
	})
	ass.EqualError(err, "entry not found for plugin test")

	data, err := info.handlerEntryGet(map[string]interface{}{
		"entry_type": "a",
		"entry_key":  "aaa",
	})
	ass.NoError(err)
	ass.Equal(map[string]interface{}{
		"test": 42,
	}, data)
}

func TestPluginInfo_handlerEntryDelete(t *testing.T) {
	ass := assert.New(t)

	bus := core.CreateBus()
	defer bus.Shutdown()

	info := pluginInfo{
		bus:    bus,
		plugin: &testPlugin{},
		config: &core.PluginConfig{
			Entries: map[string]map[string]map[string]interface{}{
				"a": {
					"aaa": {
						"test": 42,
					},
				},
			},
		},
		configDef: &core.PluginConfigDef{
			Entries: core.PluginConfigEntryDefMap{
				"a": {},
			},
		},
	}

	_, err := info.handlerEntryDelete(map[string]interface{}{
		"entry_type": "b",
		"entry_key":  "aaa",
	})
	ass.EqualError(err, "invalid entry type for plugin test")

	_, err = info.handlerEntryDelete(map[string]interface{}{
		"entry_type": "a",
		"entry_key":  "bbb",
	})
	ass.EqualError(err, "entry with key bbb does not exist")

	info.configDef.Entries = core.PluginConfigEntryDefMap{
		"a": {
			RemoveEntryFunc: func(key string) error {
				ass.Equal("aaa", key)
				return errors.New("test")
			},
		},
	}
	_, err = info.handlerEntryDelete(map[string]interface{}{
		"entry_type": "a",
		"entry_key":  "aaa",
	})
	ass.EqualError(err, "test")

	info.configDef.Entries = core.PluginConfigEntryDefMap{
		"a": {
			RemoveEntryFunc: func(key string) error {
				ass.Equal("aaa", key)
				return nil
			},
		},
	}
	_, err = info.handlerEntryDelete(map[string]interface{}{
		"entry_type": "a",
		"entry_key":  "aaa",
	})
	ass.NoError(err)
}

func TestPluginInfo_handlerEntryChange(t *testing.T) {
	ass := assert.New(t)

	bus := core.CreateBus()
	defer bus.Shutdown()

	info := pluginInfo{
		bus:    bus,
		plugin: &testPlugin{},
		config: &core.PluginConfig{
			Entries: map[string]map[string]map[string]interface{}{
				"a": {
					"aaa": {
						"test": 42,
					},
				},
			},
		},
		configDef: &core.PluginConfigDef{
			Entries: core.PluginConfigEntryDefMap{
				"a": {
					Config: api.FieldMap{
						"test": {},
					},
				},
			},
		},
	}

	_, err := info.handlerEntryChange(map[string]interface{}{
		"entry_type": "b",
		"entry_key":  "aaa",
	})
	ass.EqualError(err, "invalid entry type for plugin test")

	_, err = info.handlerEntryChange(map[string]interface{}{
		"entry_type": "a",
		"entry_key":  "bbb",
	})
	ass.EqualError(err, "entry with key bbb does not exist")

	info.configDef.Entries = core.PluginConfigEntryDefMap{
		"a": {
			Config: api.FieldMap{
				"test": {},
			},
			ChangeEntryFunc: func(key string, config map[string]interface{}) error {
				ass.Equal("aaa", key)
				ass.Equal(map[string]interface{}{
					"test": 11,
				}, config)
				return errors.New("test")
			},
		},
	}
	_, err = info.handlerEntryChange(map[string]interface{}{
		"entry_type": "a",
		"entry_key":  "aaa",
		"test":       11,
	})
	ass.EqualError(err, "test")

	info.configDef.Entries = core.PluginConfigEntryDefMap{
		"a": {
			Config: api.FieldMap{
				"test": {},
			},
			ChangeEntryFunc: func(key string, config map[string]interface{}) error {
				ass.Equal("aaa", key)
				ass.Equal(map[string]interface{}{
					"test": 11,
				}, config)
				return nil
			},
		},
	}
	_, err = info.handlerEntryChange(map[string]interface{}{
		"entry_type": "a",
		"entry_key":  "aaa",
		"test":       11,
	})
	ass.NoError(err)
}

func Test_appendParentFields(t *testing.T) {
	ass := assert.New(t)

	fields := appendEntryKey(api.FieldMap{"config": {}})
	ass.Contains(fields, "config")
	ass.Contains(fields, "entry_key")
	ass.Len(fields, 2)
}

func TestPluginInfo_configEndpoints(t *testing.T) {
	ass := assert.New(t)

	info := &pluginInfo{
		plugin: new(testPlugin),
		config: &core.PluginConfig{
			Entries: map[string]map[string]map[string]interface{}{},
		},
		configDef: &core.PluginConfigDef{
			Config: api.FieldMap{
				"aaa": {},
			},
			Entries: core.PluginConfigEntryDefMap{
				"aaa": {
					ChangeEntryFunc: func(string, map[string]interface{}) error {
						return nil
					},
				},
			},
		},
	}

	endpoints := info.configEndpoints()
	ass.Len(endpoints, 1)
	ass.Equal("/config/", endpoints[0].Path)
	ass.Len(endpoints[0].SubEntries, 7)
}

func TestPluginInfo_configEntriesEndpoints(t *testing.T) {
	ass := assert.New(t)

	info := &pluginInfo{
		plugin: new(testPlugin),
		config: &core.PluginConfig{
			Entries: map[string]map[string]map[string]interface{}{},
		},
		configDef: &core.PluginConfigDef{
			Entries: core.PluginConfigEntryDefMap{
				"aaa": {
					ChangeEntryFunc: func(string, map[string]interface{}) error {
						return nil
					},
				},
			},
		},
	}
	ass.Len(info.configEntriesEndpoints(), 5)
}

func Test_entryWrapper(t *testing.T) {
	ass := assert.New(t)

	handler := func(param map[string]interface{}) (map[string]interface{}, error) {
		ass.Equal(map[string]interface{}{
			"test":       42,
			"entry_type": "aaa",
		}, param)
		return nil, errors.New("test")
	}

	_, err := entryWrapper("aaa", handler)(map[string]interface{}{
		"test": 42,
	})
	ass.EqualError(err, "test")

	handler = func(param map[string]interface{}) (map[string]interface{}, error) {
		ass.Equal(map[string]interface{}{
			"test":       42,
			"entry_type": "aaa",
		}, param)
		return nil, nil
	}

	_, err = entryWrapper("aaa", handler)(map[string]interface{}{
		"test": 42,
	})
	ass.NoError(err)
}
