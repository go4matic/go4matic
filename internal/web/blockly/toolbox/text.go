// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package toolbox

// Text block
func Text() Block {
	return Block{
		Type: "text",
		Fields: []Field{{
			Name: "TEXT",
		}},
	}
}

// TextJoin block
func TextJoin() Block {
	return Block{
		Type: "text_join",
		Mutations: []Mutation{{
			Attributes: map[string]interface{}{
				"items": 2,
			},
		}},
	}
}

// TextAppend block
func TextAppend() Block {
	return Block{
		Type: "text_append",
		Fields: []Field{{
			Name: "VAR",
			Data: "item",
		}},
		Values: []Value{{
			Name: "TEXT",
			Shadow: &Shadow{
				Type: "text",
				Field: Field{
					Name: "TEXT",
				},
			},
		}},
	}
}

// TextLength block
func TextLength() Block {
	return Block{
		Type: "text_length",
		Values: []Value{{
			Name: "VALUE",
			Shadow: &Shadow{
				Type: "text",
				Field: Field{
					Name: "TEXT",
				},
			},
		}},
	}
}

// TextIsEmpty block
func TextIsEmpty() Block {
	return Block{
		Type: "text_isEmpty",
		Values: []Value{{
			Name: "VALUE",
			Shadow: &Shadow{
				Type: "text",
				Field: Field{
					Name: "TEXT",
				},
			},
		}},
	}
}

// TextIndexOf block
func TextIndexOf() Block {
	return Block{
		Type: "text_indexOf",
		Fields: []Field{{
			Name: "END",
			Data: "FIRST",
		}},
		Values: []Value{{
			Name: "VALUE",
			Block: &Block{
				Type: "variables_get",
				Fields: []Field{{
					Name: "VAR",
					Data: "text",
				}},
			},
		}, {
			Name: "FIND",
			Shadow: &Shadow{
				Type: "text",
				Field: Field{
					Name: "TEXT",
				},
			},
		}},
	}
}

// TextCharAt block
func TextCharAt() Block {
	return Block{
		Type: "text_charAt",
		Mutations: []Mutation{{
			Attributes: map[string]interface{}{
				"at": true,
			},
		}},
		Fields: []Field{{
			Name: "WHERE",
			Data: "FROM_START",
		}},
		Values: []Value{{
			Name: "VALUE",
			Block: &Block{
				Type: "variables_get",
				Fields: []Field{{
					Name: "VAR",
					Data: "text",
				}},
			},
		}},
	}
}

// TextGetSubstring block
func TextGetSubstring() Block {
	return Block{
		Type: "text_getSubstring",
		Mutations: []Mutation{{
			Attributes: map[string]interface{}{
				"at1": true,
				"at2": true,
			},
		}},
		Fields: []Field{{
			Name: "WHERE1",
			Data: "FROM_START",
		}, {
			Name: "WHERE2",
			Data: "FROM_START",
		}},
		Values: []Value{{
			Name: "STRING",
			Block: &Block{
				Type: "variables_get",
				Fields: []Field{{
					Name: "VAR",
					Data: "text",
				}},
			},
		}},
	}
}

// TextChangeCase block
func TextChangeCase() Block {
	return Block{
		Type: "text_changeCase",
		Fields: []Field{{
			Name: "CASE",
			Data: "UPPERCASE",
		}},
		Values: []Value{{
			Name: "TEXT",
			Shadow: &Shadow{
				Type: "text",
				Field: Field{
					Name: "TEXT",
				},
			},
		}},
	}
}

// TextTrim block
func TextTrim() Block {
	return Block{
		Type: "text_trim",
		Fields: []Field{{
			Name: "MODE",
			Data: "BOTH",
		}},
		Values: []Value{{
			Name: "TEXT",
			Shadow: &Shadow{
				Type: "text",
				Field: Field{
					Name: "TEXT",
				},
			},
		}},
	}
}

// TextPrint block
func TextPrint() Block {
	return Block{
		Type: "text_print",
		Values: []Value{{
			Name: "TEXT",
			Shadow: &Shadow{
				Type: "text",
				Field: Field{
					Name: "TEXT",
				},
			},
		}},
	}
}
