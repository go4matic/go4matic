// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package toolbox

import (
	"encoding/xml"
	"sort"

	"github.com/spf13/cast"
)

// XML root object
type XML struct {
	XMLName xml.Name `xml:"xml"`

	// List of categories and separators
	Entries []interface{}
}

// Separator for category list
type Separator struct {
	XMLName xml.Name `xml:"sep"`
}

// Category groups items in toolbar
type Category struct {
	XMLName xml.Name `xml:"category"`

	// attributes
	Name   string `xml:"name,attr"`
	Color  string `xml:"colour,attr"`
	Custom string `xml:"custom,attr,omitempty"`

	// List of categories, separators and blocks
	Entries []interface{}
}

// Block entry for toolbox
type Block struct {
	XMLName xml.Name `xml:"block"`
	Type    string   `xml:"type,attr"`

	Mutations []Mutation `xml:",omitempty"`
	Fields    []Field    `xml:",omitempty"`
	Values    []Value    `xml:",omitempty"`
}

// Shadow block for pre attached block
type Shadow struct {
	XMLName xml.Name `xml:"shadow"`
	Type    string   `xml:"type,attr"`

	Field Field
}

// Field of a block
type Field struct {
	XMLName xml.Name `xml:"field"`
	Name    string   `xml:"name,attr"`

	Data string `xml:",chardata"`
}

// Value of a block
type Value struct {
	XMLName xml.Name `xml:"value"`
	Name    string   `xml:"name,attr"`

	Shadow *Shadow `xml:",omitempty"`
	Block  *Block  `xml:",omitempty"`
}

// Mutation of a block
type Mutation struct {
	Attributes map[string]interface{}
}

// MarshalXML encodes object to XML
func (m Mutation) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	start.Name.Local = "mutation"

	// sort keys
	keys := make([]string, 0, len(m.Attributes))
	for key := range m.Attributes {
		keys = append(keys, key)
	}
	sort.Strings(keys)

	// add attributes
	start.Attr = make([]xml.Attr, 0, len(m.Attributes))
	for _, key := range keys {
		start.Attr = append(start.Attr, xml.Attr{
			Name: xml.Name{
				Local: key,
			},
			Value: cast.ToString(m.Attributes[key]),
		})
	}

	err := e.EncodeToken(start)
	if err != nil {
		return err
	}
	return e.EncodeToken(xml.EndElement{
		Name: start.Name,
	})
}
