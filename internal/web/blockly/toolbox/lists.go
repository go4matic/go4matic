// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package toolbox

// ListCreateWith block
func ListCreateWith(size int) Block {
	return Block{
		Type: "lists_create_with",
		Mutations: []Mutation{{
			Attributes: map[string]interface{}{
				"items": size,
			},
		}},
	}
}

// ListRepeat block
func ListRepeat() Block {
	return Block{
		Type: "lists_repeat",
		Values: []Value{{
			Name: "NUM",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "5",
				},
			},
		}},
	}
}

// ListLength block
func ListLength() Block {
	return Block{
		Type: "lists_length",
	}
}

// ListIsEmpty block
func ListIsEmpty() Block {
	return Block{
		Type: "lists_isEmpty",
	}
}

// ListIndexOf block
func ListIndexOf() Block {
	return Block{
		Type: "lists_indexOf",
		Fields: []Field{{
			Name: "END",
			Data: "FIRST",
		}},
		Values: []Value{{
			Name: "VALUE",
			Block: &Block{
				Type: "variables_get",
				Fields: []Field{{
					Name: "VAR",
					Data: "list",
				}},
			},
		}},
	}
}

// ListGetIndex block
func ListGetIndex() Block {
	return Block{
		Type: "lists_getIndex",
		Mutations: []Mutation{{
			Attributes: map[string]interface{}{
				"statement": false,
				"at":        true,
			},
		}},
		Fields: []Field{{
			Name: "MODE",
			Data: "GET",
		}, {
			Name: "WHERE",
			Data: "FROM_START",
		}},
		Values: []Value{{
			Name: "VALUE",
			Block: &Block{
				Type: "variables_get",
				Fields: []Field{{
					Name: "VAR",
					Data: "list",
				}},
			},
		}},
	}
}

// ListSetIndex block
func ListSetIndex() Block {
	return Block{
		Type: "lists_setIndex",
		Mutations: []Mutation{{
			Attributes: map[string]interface{}{
				"at": true,
			},
		}},
		Fields: []Field{{
			Name: "MODE",
			Data: "SET",
		}, {
			Name: "WHERE",
			Data: "FROM_START",
		}},
		Values: []Value{{
			Name: "LIST",
			Block: &Block{
				Type: "variables_get",
				Fields: []Field{{
					Name: "VAR",
					Data: "list",
				}},
			},
		}},
	}
}

// ListGetSublist block
func ListGetSublist() Block {
	return Block{
		Type: "lists_getSublist",
		Mutations: []Mutation{{
			Attributes: map[string]interface{}{
				"at1": true,
				"at2": true,
			},
		}},
		Fields: []Field{{
			Name: "WHERE1",
			Data: "FROM_START",
		}, {
			Name: "WHERE2",
			Data: "FROM_START",
		}},
		Values: []Value{{
			Name: "LIST",
			Block: &Block{
				Type: "variables_get",
				Fields: []Field{{
					Name: "VAR",
					Data: "list",
				}},
			},
		}},
	}
}

// ListSplit block
func ListSplit() Block {
	return Block{
		Type: "lists_split",
		Mutations: []Mutation{{
			Attributes: map[string]interface{}{
				"mode": "SPLIT",
			},
		}},
		Fields: []Field{{
			Name: "MODE",
			Data: "SPLIT",
		}},
		Values: []Value{{
			Name: "DELIM",
			Shadow: &Shadow{
				Type: "text",
				Field: Field{
					Name: "TEXT",
					Data: ",",
				},
			},
		}},
	}
}

// ListSort block
func ListSort() Block {
	return Block{
		Type: "lists_sort",
		Fields: []Field{{
			Name: "TYPE",
			Data: "NUMERIC",
		}, {
			Name: "DIRECTION",
			Data: "1",
		}},
	}
}
