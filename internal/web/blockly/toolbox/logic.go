// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package toolbox

// ControlsIf block
func ControlsIf() Block {
	return Block{
		Type: "controls_if",
	}
}

// LogicCompare block
func LogicCompare() Block {
	return Block{
		Type: "logic_compare",
		Fields: []Field{{
			Name: "OP",
			Data: "EQ",
		}},
	}
}

// LogicOperation block
func LogicOperation() Block {
	return Block{
		Type: "logic_operation",
		Fields: []Field{{
			Name: "OP",
			Data: "AND",
		}},
	}
}

// LogicNegate block
func LogicNegate() Block {
	return Block{
		Type: "logic_negate",
	}
}

// LogicBoolean block
func LogicBoolean() Block {
	return Block{
		Type: "logic_boolean",
		Fields: []Field{{
			Name: "BOOL",
			Data: "TRUE",
		}},
	}
}

// LogicNull block
func LogicNull() Block {
	return Block{
		Type: "logic_null",
	}
}

// LogicTernary block
func LogicTernary() Block {
	return Block{
		Type: "logic_ternary",
	}
}
