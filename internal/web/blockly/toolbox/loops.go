// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package toolbox

// ControlsRepeatExt block
func ControlsRepeatExt() Block {
	return Block{
		Type: "controls_repeat_ext",
		Values: []Value{{
			Name: "TIMES",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "10",
				},
			},
		}},
	}
}

// ControlsWhileUntil block
func ControlsWhileUntil() Block {
	return Block{
		Type: "controls_whileUntil",
		Fields: []Field{{
			Name: "MODE",
			Data: "WHILE",
		}},
	}
}

// ControlsFor block
func ControlsFor() Block {
	return Block{
		Type: "controls_for",
		Fields: []Field{{
			Name: "VAR",
			Data: "i",
		}},
		Values: []Value{{
			Name: "FROM",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "1",
				},
			},
		}, {
			Name: "TO",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "10",
				},
			},
		}, {
			Name: "BY",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "1",
				},
			},
		}},
	}
}

// ControlsForEach block
func ControlsForEach() Block {
	return Block{
		Type: "controls_forEach",
		Fields: []Field{{
			Name: "VAR",
			Data: "j",
		}},
	}
}

// ControlsFlowStatements block
func ControlsFlowStatements() Block {
	return Block{
		Type: "controls_flow_statements",
		Fields: []Field{{
			Name: "FLOW",
			Data: "BREAK",
		}},
	}
}
