// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package toolbox

// CategoryLogic category with logic blocks
func CategoryLogic() Category {
	return Category{
		Name:  "Logic",
		Color: "#5b80a5",
		Entries: []interface{}{
			ControlsIf(),
			LogicCompare(),
			LogicOperation(),
			LogicNegate(),
			LogicBoolean(),
			LogicNull(),
			LogicTernary(),
		},
	}
}

// CategoryLoops category with loop blocks
func CategoryLoops() Category {
	return Category{
		Name:  "Loops",
		Color: "#5ba55b",
		Entries: []interface{}{
			ControlsRepeatExt(),
			ControlsWhileUntil(),
			ControlsFor(),
			ControlsForEach(),
			ControlsFlowStatements(),
		},
	}
}

// CategoryMath category with math blocks
func CategoryMath() Category {
	return Category{
		Name:  "Math",
		Color: "#5b67a5",
		Entries: []interface{}{
			MathNumber(),
			MathArithmetic(),
			MathSingle(),
			MathTrig(),
			MathConstant(),
			MathNumberProperty(),
			MathRound(),
			MathOnList(),
			MathModulo(),
			MathConstrain(),
			MathRandomInt(),
			MathRandomFloat(),
		},
	}
}

// CategoryText category with text blocks
func CategoryText() Category {
	return Category{
		Name:  "Text",
		Color: "#5ba58c",
		Entries: []interface{}{
			Text(),
			TextJoin(),
			TextAppend(),
			TextLength(),
			TextIsEmpty(),
			TextIndexOf(),
			TextCharAt(),
			TextGetSubstring(),
			TextChangeCase(),
			TextTrim(),
			TextPrint(),
		},
	}
}

// CategoryLists category with list blocks
func CategoryLists() Category {
	return Category{
		Name:  "Lists",
		Color: "#745ba5",
		Entries: []interface{}{
			ListCreateWith(0),
			ListCreateWith(3),
			ListRepeat(),
			ListLength(),
			ListIsEmpty(),
			ListIndexOf(),
			ListGetIndex(),
			ListSetIndex(),
			ListGetSublist(),
			ListSplit(),
			ListSort(),
		},
	}
}

// CategoryColour category with colour blocks
func CategoryColour() Category {
	return Category{
		Name:  "Colour",
		Color: "#a5745b",
		Entries: []interface{}{
			ColourPicker(),
			ColourRandom(),
			ColourRGB(),
			ColourBlend(),
		},
	}
}

// CategoryVariables dynamic category with variables blocks
func CategoryVariables() Category {
	return Category{
		Name:   "Variables",
		Color:  "#a55b80",
		Custom: "VARIABLE",
	}
}

// CategoryFunctions dynamic category with function blocks
func CategoryFunctions() Category {
	return Category{
		Name:   "Functions",
		Color:  "#995ba5",
		Custom: "PROCEDURE",
	}
}

// DefaultToolbox from blockly
func DefaultToolbox() XML {
	return XML{
		Entries: []interface{}{
			CategoryLogic(),
			CategoryLoops(),
			CategoryMath(),
			CategoryText(),
			CategoryLists(),
			CategoryColour(),
			new(Separator),
			CategoryVariables(),
			CategoryFunctions(),
		},
	}
}
