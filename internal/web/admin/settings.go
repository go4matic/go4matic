// Copyright 2020 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package admin

import (
	"time"

	"gitlab.com/go4matic/go4matic/internal/web/helper"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// render settings page
func (w *Web) adminSettings(ctx core.HTTPContext) {
	ctx.Set("tpl", "admin/settings")
	ctx.Set("title", "Settings/Admin - Go4Matic")

	configResponse := w.Bus.HandleRequestWait(core.Request{
		ID: "core.config#global_get",
	}, time.Second)

	if configResponse.Error != nil {
		helper.AddMessage(ctx, "danger", configResponse.Error.Error())
		ctx.Set("tpl", "admin")
		return
	}

	form, err := helper.FieldForm(core.Config(), configResponse.Data)
	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		ctx.Set("tpl", "admin")
		return
	}
	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["form"] = form
}

// handle post on settings page
func (w *Web) adminSettingsPost(ctx core.HTTPContext) {
	config, err := helper.FieldFormValues(ctx, core.Config())

	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		return
	}

	config, err = core.Config().Validate(config)
	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		return
	}

	response := w.Bus.HandleRequestWait(core.Request{
		ID:         "core.config#global_set",
		Parameters: config,
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
	}
}
