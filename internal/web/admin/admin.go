// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package admin

import (
	"errors"
	"time"

	"github.com/spf13/cast"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/internal/web/blockly"
	"gitlab.com/go4matic/go4matic/internal/web/blockly/toolbox"
	"gitlab.com/go4matic/go4matic/internal/web/dashboard"
	"gitlab.com/go4matic/go4matic/internal/web/helper"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// Web handler for admin
type Web struct {
	Bus core.Bus

	blocklyJs      string
	blocklyToolbox toolbox.XML
}

// Key that identifies this service
func (w *Web) Key() string {
	return "core.admin.web"
}

// Start prepares admin web pages
func (w *Web) Start(bus core.Bus) error {
	resp := bus.HandleRequestWait(core.Request{
		ID: "core.scripts#list_modules",
	}, time.Second*2)

	if resp.Error != nil {
		return errors.New(assets.L.Get("failed to load script modules: %s", resp.Error))
	}

	modules := make(blockly.ModuleDefList, 0, len(resp.Data))
	for moduleName, infoRaw := range resp.Data {
		info := cast.ToStringMap(infoRaw)

		module := blockly.NewModule(moduleName, info["functions"].(map[string]api.Function))
		modules.Add(module)
	}

	var err error
	w.blocklyJs, err = modules.BlockJS()
	if err != nil {
		return errors.New(assets.L.Get("failed to get Blockly JS: %s", err))
	}
	w.blocklyToolbox = modules.ExtendedDefaultToolbox()
	return nil
}

// AdminEndpoints returns admin path entries
func (w *Web) AdminEndpoints() []core.PathEntry {
	pages := []core.PathEntry{{
		Path:    "/",
		Handler: w.adminHome,
	}, {
		Path: "dashboards",
		SubEntries: []core.PathEntry{{
			Handler: w.adminDashboards,
		}, {
			Method:     "POST",
			Middleware: []func(ctx core.HTTPContext){w.adminDashboardsPost},
			Handler:    w.adminDashboards,
		}, {
			Path: ":name",
			SubEntries: []core.PathEntry{{
				Handler: w.adminDashboard,
			}, {
				Method:     "POST",
				Middleware: []func(ctx core.HTTPContext){w.adminDashboardPost},
				Handler:    w.adminDashboard,
			}, {
				Path: "add_widget/:group/:widget_type",
				SubEntries: []core.PathEntry{{
					Handler: w.adminDashboardNewWidget,
				}, {
					Method:     "POST",
					Middleware: []func(ctx core.HTTPContext){w.adminDashboardNewWidgetPost},
					Handler:    w.adminDashboardNewWidget,
				}},
			}, {
				Path: "edit_widget/:group/:widget",
				SubEntries: []core.PathEntry{{
					Handler: w.adminDashboardEditWidget,
				}, {
					Method:     "POST",
					Middleware: []func(ctx core.HTTPContext){w.adminDashboardEditWidgetPost},
					Handler:    w.adminDashboardEditWidget,
				}},
			}},
		}},
	}, {
		Path: "timers",
		SubEntries: []core.PathEntry{{
			Handler: w.adminTimers,
		}, {
			Method:     "POST",
			Middleware: []func(ctx core.HTTPContext){w.adminTimersPost},
			Handler:    w.adminTimers,
		}, {
			Path: ":key",
			SubEntries: []core.PathEntry{{
				Handler: w.adminTimer,
			}, {
				Method:     "POST",
				Middleware: []func(ctx core.HTTPContext){w.adminTimerPost},
				Handler:    w.adminTimer,
			}},
		}},
	}, {
		Path: "items",
		SubEntries: []core.PathEntry{{
			Handler: w.adminItems,
		}, {
			Path: ":id",
			SubEntries: []core.PathEntry{{
				Handler: w.adminItem,
			}, {
				Method:     "POST",
				Middleware: []func(ctx core.HTTPContext){w.adminItemPost},
				Handler:    w.adminItem,
			}},
		}},
	}, {
		Path: "scripts",
		SubEntries: []core.PathEntry{{
			Handler: w.adminScripts,
		}, {
			Method:     "POST",
			Middleware: []func(ctx core.HTTPContext){w.adminScriptsPost},
			Handler:    w.adminScripts,
		}, {
			Path: ":id",
			SubEntries: []core.PathEntry{{
				Handler: w.adminScript,
			}, {
				Method:     "POST",
				Middleware: []func(ctx core.HTTPContext){w.adminScriptPost},
				Handler:    w.adminScript,
			}, {
				Path:    "toolbox.xml",
				Handler: w.adminScriptToolboxXML,
			}, {
				Path:    "blocks.js",
				Handler: w.adminScriptBlocksJs,
			}},
		}},
	}, {
		Path: "plugins",
		SubEntries: []core.PathEntry{{
			Path: ":id",
			SubEntries: []core.PathEntry{{
				Handler: w.adminPlugin,
			}, {
				Method:     "POST",
				Middleware: []func(ctx core.HTTPContext){w.adminPluginPost},
				Handler:    w.adminPlugin,
			}, {
				Path:    "add/:entry_type",
				Handler: w.adminPluginAdd,
			}, {
				Method:     "POST",
				Path:       "add/:entry_type",
				Middleware: []func(ctx core.HTTPContext){w.adminPluginAddPost},
				Handler:    w.adminPluginAdd,
			}, {
				Method:     "POST",
				Path:       "edit/:entry_type/:key",
				Middleware: []func(ctx core.HTTPContext){w.adminPluginEditPost},
				Handler:    w.adminPluginEdit,
			}, {
				Path:    "edit/:entry_type/:key",
				Handler: w.adminPluginEdit,
			}},
		}},
	}, {
		Path: "settings",
		SubEntries: []core.PathEntry{{
			Handler: w.adminSettings,
		}, {
			Method:     "POST",
			Middleware: []func(ctx core.HTTPContext){w.adminSettingsPost},
			Handler:    w.adminSettings,
		}},
	}}

	return w.adminRaspberryPages(pages)
}

// TODO improve admin function + remove code duplication

// dummy/empty admin page
func (w *Web) adminHome(ctx core.HTTPContext) {
	ctx.Set("tpl", "admin/index")
	ctx.Set("title", "Admin - Go4Matic")

	// plugins
	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.plugins#list",
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}
	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["plugins"] = response.Data

	// list dash boards
	response = w.Bus.HandleRequestWait(core.Request{
		ID: "core.dashboards#dashboard_list",
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}
	vars["dashboards"] = len(response.Data["dashboards"].([]dashboard.Dashboard))

	// other list actions for counter
	counters := map[string]string{
		"items":   "core.items#list",
		"timers":  "core.timers#list",
		"scripts": "core.scripts#list",
	}

	for key, action := range counters {
		response = w.Bus.HandleRequestWait(core.Request{ID: action}, time.Second)

		if response.Error != nil {
			helper.AddMessage(ctx, "danger", response.Error.Error())
			return
		}
		vars[key] = len(response.Data)
	}
}
