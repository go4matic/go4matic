// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package admin

import (
	"fmt"
	"time"

	"github.com/spf13/cast"

	"gitlab.com/go4matic/go4matic/internal/web/helper"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

func (w *Web) adminItems(ctx core.HTTPContext) {
	ctx.Set("tpl", "admin/items")
	ctx.Set("title", "Items/Admin - Go4Matic")

	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.items#list",
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}
	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["items"] = response.Data
}

func (w *Web) adminItem(ctx core.HTTPContext) {
	ctx.Set("tpl", "admin/item")
	id := ctx.Param("id")
	ctx.Set("title", fmt.Sprintf("%s/Items/Admin - Go4Matic", id))

	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.items#get_info",
		Parameters: map[string]interface{}{
			"id": id,
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}

	item := response.Data
	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["item"] = item
	vars["id"] = id

	var values map[string]interface{}
	if cast.ToBool(item["is_sensor"]) {
		response := w.Bus.HandleRequestWait(core.Request{
			ID: "core.items#get_values",
			Parameters: map[string]interface{}{
				"id": id,
			},
		}, time.Second)

		if response.Error != nil {
			helper.AddMessage(ctx, "danger", response.Error.Error())
			return
		}

		vars["sensor_values"] = response.Data
		values = response.Data
	}

	if cast.ToBool(item["is_actor"]) {
		actorForm, err := helper.FieldForm(item["values"].(api.FieldMap), values)

		if err != nil {
			helper.AddMessage(ctx, "danger", err.Error())
			ctx.Set("tpl", "admin")
			return
		}

		vars["actor_form"] = actorForm
	}
}

func (w *Web) adminItemPost(ctx core.HTTPContext) {
	id := ctx.Param("id")
	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.items#get_info",
		Parameters: map[string]interface{}{
			"id": id,
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}

	if !cast.ToBool(response.Data["is_actor"]) {
		// no actor -> no value set
		return
	}

	values, err := helper.FieldFormValues(ctx, response.Data["values"].(api.FieldMap))
	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		return
	}

	response = w.Bus.HandleRequestWait(core.Request{
		ID: "core.items#set_values",
		Parameters: map[string]interface{}{
			"id":     id,
			"values": values,
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}
}
