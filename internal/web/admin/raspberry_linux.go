// Copyright 2020 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package admin

import (
	"crypto/sha256"
	"encoding/hex"
	"flag"
	"io"
	"io/ioutil"
	"net"
	"os"
	"strings"
	"time"

	"gitlab.com/bboehmke/raspi-alpine-builder"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/internal/web/helper"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// RaspberryEnabled is true if the raspberry image integration is enabled
var RaspberryEnabled bool

// files used for update progress
const updateImageFile = "/data/update/image.gz"
const updateImageNameFile = "/data/update/filename"

func init() {
	flag.BoolVar(&RaspberryEnabled, "raspberry", false,
		"Enable raspberry image integration")
}

// UBootResetCounter to mark system as booted
func UBootResetCounter() {
	if RaspberryEnabled {
		err := alpine_builder.UBootResetCounter()
		if err != nil {
			zap.L().Error(assets.L.Get("Failed to reset uboot counter: %v", err))
		}
	}
}

// add raspberry admin pages if option is enabled
func (w *Web) adminRaspberryPages(pages []core.PathEntry) []core.PathEntry {
	if !RaspberryEnabled {
		return pages
	}

	return append(pages, core.PathEntry{
		Path: "raspberry",
		SubEntries: []core.PathEntry{{
			Handler: w.adminRaspberry,
		}, {
			Method:     "POST",
			Middleware: []func(ctx core.HTTPContext){w.adminRaspberryPost},
			Handler:    w.adminRaspberry,
		}, {
			Path:    "update",
			Handler: w.adminRaspberryUpdate,
		}, {
			Path:       "update",
			Method:     "POST",
			Middleware: []func(ctx core.HTTPContext){w.adminRaspberryUpdatePost},
			Handler:    w.adminRaspberryUpdate,
		}, {
			Path:    "reboot",
			Handler: w.adminRaspberryReboot,
		}},
	})
}

// raspberry main admin page
func (w *Web) adminRaspberry(ctx core.HTTPContext) {
	ctx.Set("tpl", "admin/raspberry")

	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["ssh_enabled"] = false
	vars["network"] = &alpine_builder.NetworkInfo{}
	vars["timezones"] = []string{}
	vars["timezone"] = ""

	var err error
	vars["timezones"], err = alpine_builder.SystemListTimeZones()
	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		return
	}

	vars["timezone"], err = alpine_builder.SystemGetTimeZone()
	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		return
	}

	vars["network"], err = alpine_builder.GetNetworkInfo()
	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		return
	}

	vars["ssh_enabled"], err = alpine_builder.SystemSSHEnabled()
	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		return
	}
}

// handle post requests on raspberry main admin page
func (w *Web) adminRaspberryPost(ctx core.HTTPContext) {
	switch ctx.PostForm("action") {
	case "network":
		// enable dhcp?
		if ctx.PostForm("network_type") == "dhcp" {
			err := alpine_builder.NetworkEnableDHCP()
			if err != nil {
				helper.AddMessage(ctx, "danger", err.Error())
				return
			}
			helper.AddMessage(ctx, "success",
				assets.L.Get("Updated network config (DHCP)"))
			return
		}

		if net.ParseIP(ctx.PostForm("network_address")) == nil {
			helper.AddMessage(ctx, "danger",
				assets.L.Get("invalid IP address"))
			return
		}
		if net.ParseIP(ctx.PostForm("network_netmask")) == nil {
			helper.AddMessage(ctx, "danger",
				assets.L.Get("invalid netmask"))
			return
		}
		if net.ParseIP(ctx.PostForm("network_gateway")) == nil {
			helper.AddMessage(ctx, "danger",
				assets.L.Get("invalid gateway"))
			return
		}

		err := alpine_builder.NetworkSetStatic(
			ctx.PostForm("network_address"),
			ctx.PostForm("network_netmask"),
			ctx.PostForm("network_gateway"))
		if err != nil {
			helper.AddMessage(ctx, "danger", err.Error())
			return
		}
		helper.AddMessage(ctx, "success",
			assets.L.Get("Updated network config (static)"))

	case "system":
		// SSH server
		var err error
		if ctx.PostForm("system_ssh") == "enabled" {
			err = alpine_builder.SystemEnableSSH()
		} else {
			err = alpine_builder.SystemDisableSSH()
		}
		if err != nil {
			helper.AddMessage(ctx, "danger", err.Error())
			return
		}

		// timezone
		timezone := strings.TrimSpace(ctx.PostForm("system_timezone"))
		err = alpine_builder.SystemSetTimeZone(timezone)
		if err != nil {
			helper.AddMessage(ctx, "danger", err.Error())
			return
		}

		// root password
		pw1 := strings.TrimSpace(ctx.PostForm("system_root_pw1"))
		pw2 := strings.TrimSpace(ctx.PostForm("system_root_pw2"))
		if pw1 != "" || pw2 != "" {
			if len(pw1) < 6 {
				helper.AddMessage(ctx, "danger",
					assets.L.Get("Minimum root password length is 6"))
				return
			}

			if pw1 != pw2 {
				helper.AddMessage(ctx, "danger",
					assets.L.Get("Passwords does not match"))
				return
			}

			err = alpine_builder.SystemSetRootPassword(pw1)
			if err != nil {
				helper.AddMessage(ctx, "danger", err.Error())
				return
			}
		}

	case "reboot":
		err := alpine_builder.SystemReboot()
		if err != nil {
			helper.AddMessage(ctx, "danger", err.Error())
			return
		}

		ctx.Redirect(302, "raspberry/reboot")
		ctx.Abort()

	case "shutdown":
		err := alpine_builder.SystemShutdown()
		if err != nil {
			helper.AddMessage(ctx, "danger", err.Error())
			return
		}

	case "update_manual":
		// TODO more validation/checksum
		uploadFile, fileHeader, err := ctx.Request().FormFile("update_upload")
		if err != nil {
			helper.AddMessage(ctx, "danger", err.Error())
			return
		}
		defer uploadFile.Close()

		_ = os.MkdirAll("/data/update", os.ModePerm)
		_ = os.Remove(updateImageNameFile)

		imgFile, err := os.Create(updateImageFile)
		if err != nil {
			helper.AddMessage(ctx, "danger", err.Error())
			return
		}
		defer imgFile.Close()

		_, err = io.Copy(imgFile, uploadFile)
		if err != nil {
			helper.AddMessage(ctx, "danger", err.Error())
			return
		}

		_ = ioutil.WriteFile(updateImageNameFile, []byte(fileHeader.Filename), os.ModePerm)

		// TODO sync multiple requests
		ctx.Redirect(302, "raspberry/update")
		ctx.Abort()
	}
}

// page to validate update file
func (w *Web) adminRaspberryUpdate(ctx core.HTTPContext) {
	ctx.Set("tpl", "admin/raspberry_update")

	if _, err := os.Stat(updateImageFile); os.IsNotExist(err) {
		ctx.Redirect(302, "raspberry")
		ctx.Abort()
		return
	}

	fileNameRaw, err := ioutil.ReadFile(updateImageNameFile)
	if err != nil {
		ctx.Redirect(302, "raspberry")
		ctx.Abort()
		return
	}

	imgFile, err := os.Open(updateImageFile)
	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		return
	}

	hasher := sha256.New()
	_, err = io.Copy(hasher, imgFile)
	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		return
	}

	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["checksum"] = hex.EncodeToString(hasher.Sum(nil))
	vars["filename"] = string(fileNameRaw)
}

// start update on post request
func (w *Web) adminRaspberryUpdatePost(ctx core.HTTPContext) {
	err := alpine_builder.UpdateSystem(updateImageFile)
	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		return
	}

	ctx.Redirect(302, "reboot")
	ctx.Abort()

	go func() {
		time.Sleep(time.Millisecond * 500)
		err := alpine_builder.SystemReboot()
		if err != nil {
			zap.L().Error(assets.L.Get("Reboot failed: %v", err))
			return
		}
	}()
}

// show reboot wait page
func (w *Web) adminRaspberryReboot(ctx core.HTTPContext) {
	ctx.Set("tpl", "admin/raspberry_reboot")
}
