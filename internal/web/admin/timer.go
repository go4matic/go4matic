// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package admin

import (
	"fmt"
	"strings"
	"time"

	"github.com/spf13/cast"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/internal/web/helper"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

func (w *Web) adminTimers(ctx core.HTTPContext) {
	ctx.Set("tpl", "admin/timers")
	ctx.Set("title", "Timers/Admin - Go4Matic")

	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.timers#list",
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}

	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["timers"] = response.Data
}

func (w *Web) adminTimersPost(ctx core.HTTPContext) {
	var request core.Request
	action := ctx.PostForm("action")
	switch action {
	case "add":
		request = core.Request{
			ID: "core.timers#add",
			Parameters: map[string]interface{}{
				"key": ctx.PostForm("key"),
			},
		}
	case "remove":
		request = core.Request{
			ID: "core.timers#remove",
			Parameters: map[string]interface{}{
				"key": ctx.PostForm("key"),
			},
		}
	default:
		helper.AddMessage(ctx, "danger",
			assets.L.Get("unknown action %s", action))
		return
	}

	response := w.Bus.HandleRequestWait(request, time.Second)
	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
	}
}

func (w *Web) adminTimer(ctx core.HTTPContext) {
	ctx.Set("tpl", "admin/timer")
	key := ctx.Param("key")
	ctx.Set("title", fmt.Sprintf("%s/Timers/Admin - Go4Matic", key))

	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.timers#get",
		Parameters: map[string]interface{}{
			"key": key,
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}

	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["key"] = key
	vars["timer"] = response.Data

	response = w.Bus.HandleRequestWait(core.Request{
		ID: "core.items#list",
	}, time.Second)

	data := make(map[interface{}]string, len(response.Data))
	for id, item := range response.Data {
		itemData := cast.ToStringMap(item)
		fields, _ := itemData["values"].(api.FieldMap)
		for name := range fields {
			data[id+"#"+name] = fmt.Sprintf("%s - %s", itemData["name"], name)
		}
	}
	vars["possible_item_values"] = data

	vars["secToTime"] = func(sec int) string {
		return time.Date(0, 0, 0,
			0, 0, sec, 0, time.UTC).Format("15:04")
	}
	vars["repeatStr"] = func(repeat int) string {
		if repeat == 0 {
			return "No"
		}
		list := make([]string, 0, 7)
		if repeat&(0x02) > 0 {
			list = append(list, assets.L.Get("Mo"))
		}
		if repeat&(0x04) > 0 {
			list = append(list, assets.L.Get("Tu"))
		}
		if repeat&(0x08) > 0 {
			list = append(list, assets.L.Get("We"))
		}
		if repeat&(0x10) > 0 {
			list = append(list, assets.L.Get("Th"))
		}
		if repeat&(0x20) > 0 {
			list = append(list, assets.L.Get("Fr"))
		}
		if repeat&(0x40) > 0 {
			list = append(list, assets.L.Get("Sa"))
		}
		if repeat&(0x01) > 0 {
			list = append(list, assets.L.Get("Su"))
		}
		return strings.Join(list, ", ")
	}
}

func (w *Web) adminTimerPost(ctx core.HTTPContext) {
	key := ctx.Param("key")

	var request core.Request
	action := ctx.PostForm("action")
	switch action {
	case "add_item":
		request = core.Request{
			ID: "core.timers#add_item_value",
			Parameters: map[string]interface{}{
				"key":        key,
				"item_value": ctx.PostForm("item_value"),
			},
		}
	case "remove_item":
		request = core.Request{
			ID: "core.timers#remove_item_value",
			Parameters: map[string]interface{}{
				"key":        key,
				"item_value": ctx.PostForm("item_value"),
			},
		}

	case "add_time":
		request = core.Request{
			ID: "core.timers#add_time",
			Parameters: map[string]interface{}{
				"key":    key,
				"active": ctx.PostForm("active") != "",
				"time":   ctx.PostForm("time"),
				"repeat": ctx.PostForm("repeat"),
				"state":  ctx.PostForm("state"),
			},
		}

	case "edit_time":
		request = core.Request{
			ID: "core.timers#edit_time",
			Parameters: map[string]interface{}{
				"key":    key,
				"idx":    ctx.PostForm("idx"),
				"active": ctx.PostForm("active") != "",
				"time":   ctx.PostForm("time"),
				"repeat": ctx.PostForm("repeat"),
				"state":  ctx.PostForm("state"),
			},
		}

	case "remove_time":
		request = core.Request{
			ID: "core.timers#remove_time",
			Parameters: map[string]interface{}{
				"key": key,
				"idx": ctx.PostForm("idx"),
			},
		}

	default:
		helper.AddMessage(ctx, "danger",
			assets.L.Get("unknown action %s", action))
		return
	}

	response := w.Bus.HandleRequestWait(request, time.Second)
	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
	}
}
