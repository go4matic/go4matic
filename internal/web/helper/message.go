// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package helper

import "gitlab.com/go4matic/go4matic/pkg/core"

// Message to show on web page (error, warning, ...)
type Message struct {
	// Type of message
	//  valid values: danger, info, warning, success
	Type string
	// Content of message
	Content string
}

// AddMessage to context
func AddMessage(ctx core.HTTPContext, messageType, content string) {
	messages := ctx.MustGet("messages").([]Message)
	messages = append(messages, Message{
		Type:    messageType,
		Content: content,
	})
	ctx.Set("messages", messages)
}
