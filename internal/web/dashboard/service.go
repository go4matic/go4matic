// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dashboard

import (
	"errors"
	"fmt"
	"strconv"
	"sync"
	"sync/atomic"

	"github.com/spf13/cast"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/internal/web/dashboard/widgets"
	"gitlab.com/go4matic/go4matic/internal/web/helper"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// Dashboard instance
type Dashboard struct {
	Name       string
	Hide       bool
	Icon       string // font-awesome
	MaxColumns int
	Groups     []Group
}

// Dump dashboard to data map
func (d *Dashboard) Dump() map[string]interface{} {
	groups := make([]interface{}, 0, len(d.Groups))
	for _, group := range d.Groups {
		groups = append(groups, group.Dump())
	}

	return map[string]interface{}{
		"name":        d.Name,
		"hide":        d.Hide,
		"icon":        d.Icon,
		"max_columns": d.MaxColumns,
		"groups":      groups,
	}
}

// Group of widgets
type Group struct {
	Name    string
	Widgets []WidgetWrapper
}

// Dump group to data map
func (g *Group) Dump() map[string]interface{} {
	widgetList := make([]interface{}, 0, len(g.Widgets))
	for _, widget := range g.Widgets {
		widgetList = append(widgetList, widget.Config)
	}

	return map[string]interface{}{
		"name":    g.Name,
		"widgets": widgetList,
	}
}

// widgetIDCounter holds the last used id
var widgetIDCounter uint64

// ID returns a runtime unique identifier for widgets
func ID() string {
	return strconv.FormatUint(atomic.AddUint64(&widgetIDCounter, 1), 10)
}

// WidgetWrapper contains config and instance of widget
type WidgetWrapper struct {
	Config map[string]interface{}
	Widget widgets.Widget
	ID     string
}

// Name of widget
func (w *WidgetWrapper) Name() string {
	if title, ok := w.Config["title"]; ok {
		return fmt.Sprintf("%s - %s ", w.TypeID(), title)
	}
	return w.TypeID()
}

// TypeID identifies widget type
func (w *WidgetWrapper) TypeID() string {
	if w.Widget != nil {
		return w.Widget.TypeID()
	}
	return cast.ToString(w.Config["type"])
}

// HTML returns the rendered widget
func (w *WidgetWrapper) HTML() string {
	if w.Widget == nil {
		return assets.L.Get("<strong>Unknown widget type: %s</strong>", w.TypeID())
	}

	// copy config before modify
	config := make(map[string]interface{}, len(w.Config)+1)
	for key, value := range w.Config {
		config[key] = value
	}
	config["id"] = w.ID

	varWidget, ok := w.Widget.(widgets.RenderVariablesWidget)
	if ok {
		vars, err := varWidget.RenderVariables(config)
		if err != nil {
			zap.L().Error(err.Error())
			return assets.L.Get("<strong class=\"col-md-6\">Failed to get widget render variables</strong>")
		}
		for key, value := range vars {
			config[key] = value
		}
	}

	data, err := helper.RenderString(
		fmt.Sprintf("widgets/%s", w.Widget.TypeID()), config)
	if err != nil {
		zap.L().Error(err.Error())
		return assets.L.Get("<strong class=\"col-md-6\">Failed to render template</strong>")
	}
	return data
}

// Service manages all dashboard instance
type Service struct {
	bus core.Bus

	// list of registered dashboards
	dashboards []Dashboard

	// map of registered widgets
	widgetTypes map[string]widgets.Widget

	// mutex for dashboards & widget types
	mutex sync.RWMutex

	// trigger functions for events
	trigger core.EventTrigger
}

// Key that identifies this service
func (s *Service) Key() string {
	return "core.dashboards"
}

// Init load the service
func (s *Service) Init(bus core.Bus) error {
	s.bus = bus
	s.dashboards = make([]Dashboard, 0)
	s.widgetTypes = make(map[string]widgets.Widget)

	s.registerWidget(&widgets.SwitchWidget{
		Bus: bus,
	})
	s.registerWidget(&widgets.SolarWidget{
		Bus: bus,
	})
	s.registerWidget(&widgets.TimerWidget{
		Bus: bus,
	})
	s.registerWidget(&widgets.IFrameWidget{})
	s.registerWidget(&widgets.ValueBoolWidget{
		Bus: bus,
	})
	s.registerWidget(&widgets.ValueNumberWidget{
		Bus: bus,
	})
	s.registerWidget(&widgets.ValueStringWidget{
		Bus: bus,
	})
	return nil
}

// registerWidget to service
func (s *Service) registerWidget(widget widgets.Widget) {
	s.widgetTypes[widget.TypeID()] = widget
}

// Start the service
func (s *Service) Start(bus core.Bus) error {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	data, err := core.ConfigRead(bus, "core.dashboards")
	if err != nil {
		return err
	}

	// dashboards
	for _, dashboardRaw := range cast.ToSlice(data) {
		dashboard := cast.ToStringMap(dashboardRaw)
		groups := make([]Group, 0)

		// groups
		for _, groupRaw := range cast.ToSlice(dashboard["groups"]) {
			group := cast.ToStringMap(groupRaw)
			widgetList := make([]WidgetWrapper, 0)

			// widgets
			for _, widgetRaw := range cast.ToSlice(group["widgets"]) {
				config := cast.ToStringMap(widgetRaw)
				widgetType := cast.ToString(config["type"])

				widget, ok := s.widgetTypes[widgetType]
				if !ok {
					zap.L().Warn(assets.L.Get("Unknown widget loaded %s", widgetType))

					widgetList = append(widgetList, WidgetWrapper{
						Config: config,
					})
				} else {
					fields := widget.ConfigDef()
					fields["{}"] = api.Field{}
					config2, err := fields.Validate(config)
					if err != nil {
						zap.L().Warn(assets.L.Get("Failed to loaded %s widget: %v", widgetType, err))
						widgetList = append(widgetList, WidgetWrapper{
							Config: config,
						})
					} else {
						widgetList = append(widgetList, WidgetWrapper{
							Config: config2,
							Widget: widget,
							ID:     ID(),
						})
					}
				}
			}

			groups = append(groups, Group{
				Name:    cast.ToString(group["name"]),
				Widgets: widgetList,
			})
		}

		maxColumns := cast.ToInt(dashboard["max_columns"])
		if maxColumns > 3 || maxColumns < 1 {
			maxColumns = 3
		}

		s.dashboards = append(s.dashboards, Dashboard{
			Name:       cast.ToString(dashboard["name"]),
			Hide:       cast.ToBool(dashboard["hide"]),
			Icon:       cast.ToString(dashboard["icon"]),
			MaxColumns: maxColumns,
			Groups:     groups,
		})
	}

	return nil
}

// save widgets
func (s *Service) save() {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	data := make([]interface{}, len(s.dashboards))
	for idx, dashboard := range s.dashboards {
		data[idx] = dashboard.Dump()
	}

	err := core.ConfigWrite(s.bus, "core.dashboards", data)
	if err != nil {
		zap.L().Error(err.Error())
	}

	s.trigger("core.dashboards#changed", nil)
}

// Handlers for this service
func (s *Service) Handlers() map[string]core.RequestHandler {
	// TODO add missing handler
	return map[string]core.RequestHandler{
		"dashboard_list": {
			Func: s.dashboardList,
			Def: api.SimpleHandler{
				Description: assets.L.Get("List existing dashboards"),
				Response: api.FieldMap{
					"dashboards": {
						Description: assets.L.Get("List with dashboard objects"),
						Required:    true,
						Type:        api.Slice(api.Any),
					},
				},
			},
		},
		"dashboard_get": {
			Func: s.dashboardGet,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Get dashboard information"),
				Parameters: api.FieldMap{
					"dashboard_name": {
						Description: assets.L.Get("Name of dashboard"),
						Required:    true,
						Type:        api.String,
					},
				},
				Response: api.FieldMap{
					"dashboard": {
						Description: assets.L.Get("Dashboard object"),
						Required:    true,
						Type:        api.Any,
					},
					"dashboard_idx": {
						Description: assets.L.Get("Index of dashboard"),
						Required:    true,
						Type:        api.Int,
					},
				},
			},
		},
		"dashboard_add": {
			Func: s.dashboardAdd,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Add dashboard"),
				Parameters: api.FieldMap{
					"dashboard_name": {
						Description: assets.L.Get("Name of dashboard"),
						Type:        api.String,
						Required:    true,
					},
					"hide": {
						Description:  assets.L.Get("True if dashboard hidden from menu"),
						Type:         api.Bool,
						DefaultValue: false,
					},
					"icon": {
						Description:  assets.L.Get("Icon of dashboard (font-awesome)"),
						Type:         api.String,
						DefaultValue: "",
					},
					"max_columns": {
						Description:  assets.L.Get("Maximal amount of columns"),
						Type:         api.Int,
						DefaultValue: 3,
					},
				},
			},
		},
		"dashboard_change": {
			Func: s.dashboardChange,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Change dashboard settings"),
				Parameters: api.FieldMap{
					"dashboard_name": {
						Description: assets.L.Get("Name of dashboard"),
						Type:        api.String,
						Required:    true,
					},
					"new_name": {
						Description: assets.L.Get("New name of dashboard"),
						Type:        api.String,
					},
					"hide": {
						Description: assets.L.Get("True if dashboard hidden from menu"),
						Type:        api.Bool,
					},
					"icon": {
						Description: assets.L.Get("Icon of dashboard (font-awesome)"),
						Type:        api.String,
					},
					"max_columns": {
						Description: assets.L.Get("Maximal amount of columns"),
						Type:        api.Int,
					},
					"new_position": {
						Description: assets.L.Get("New position of dashboard"),
						Type:        api.Int,
					},
				},
			},
		},
		"dashboard_remove": {
			Func: s.dashboardRemove,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Remove dashboard"),
				Parameters: api.FieldMap{
					"dashboard_name": {
						Description: assets.L.Get("Name of dashboard"),
						Required:    true,
						Type:        api.String,
					},
				},
			},
		},

		/*"group_list": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("List widget groups of dashboard"),
				Parameters: api.FieldMap{
					"dashboard_name": {
						Description: assets.L.Get("Name of dashboard"),
						Required:    true,
						Type:        api.String,
					},
				},
				Response: groupList,
			},
		},
		"group_get": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("Get widget group of dashboard"),
				Parameters: api.FieldMap{
					"dashboard_name": {
						Description: assets.L.Get("Name of dashboard"),
						Required:    true,
						Type:        api.String,
					},
					"group_idx": {
						Description: assets.L.Get("Index of widget group"),
						Required:    true,
						Type:        api.Int,
					},
				},
				Response: groupInfo,
			},
		},*/
		"group_add": {
			Func: s.groupAdd,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Add widget group to dashboard"),
				Parameters: api.FieldMap{
					"dashboard_name": {
						Description: assets.L.Get("Name of dashboard"),
						Required:    true,
						Type:        api.String,
					},
					"group_name": {
						Description: assets.L.Get("Name of widget group"),
						Type:        api.String,
					},
				},
			},
		},
		"group_change": {
			Func: s.groupChange,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Change widget group settings"),
				Parameters: api.FieldMap{
					"dashboard_name": {
						Description: assets.L.Get("Name of dashboard"),
						Required:    true,
						Type:        api.String,
					},
					"group_idx": {
						Description: assets.L.Get("Index of widget group"),
						Required:    true,
						Type:        api.Int,
					},
					"group_name": {
						Description: assets.L.Get("New name of widget group"),
						Type:        api.String,
					},
					"new_position": {
						Description: assets.L.Get("New position of group"),
						Type:        api.Int,
					},
				},
			},
		},
		"group_remove": {
			Func: s.groupRemove,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Remove widget group from dashboard"),
				Parameters: api.FieldMap{
					"dashboard_name": {
						Description: assets.L.Get("Name of dashboard"),
						Required:    true,
						Type:        api.String,
					},
					"group_idx": {
						Description: assets.L.Get("Index of widget group"),
						Required:    true,
						Type:        api.Int,
					},
				},
			},
		},

		/*"widget_list": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("List widgets of group"),
				Parameters: api.FieldMap{
					"dashboard_name": {
						Description: assets.L.Get("Name of dashboard"),
						Required:    true,
						Type:        api.String,
					},
					"group_name": {
						Description: assets.L.Get("Name of widget group"),
						Required:    true,
						Type:        api.String,
					},
				},
				Response: widgetList,
			},
		},
		"widget_get": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("Get widget of group"),
				Parameters: api.FieldMap{
					"dashboard_name": {
						Description: assets.L.Get("Name of dashboard"),
						Required:    true,
						Type:        api.String,
					},
					"group_name": {
						Description: assets.L.Get("Name of widget group"),
						Required:    true,
						Type:        api.String,
					},
					"widget_idx": {
						Description: assets.L.Get("Index of widget in group"),
						Required:    true,
						Type:        api.Int,
					},
				},
				Response: widgetInfo,
			},
		},*/
		"widget_add": {
			Func: s.widgetAdd,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Add a widget to group"),
				Parameters: api.FieldMap{
					"dashboard_name": {
						Description: assets.L.Get("Name of dashboard"),
						Required:    true,
						Type:        api.String,
					},
					"group_idx": {
						Description: assets.L.Get("Index of widget group"),
						Required:    true,
						Type:        api.Int,
					},
					"widget_type": {
						Description: assets.L.Get("Type of widget to add"),
						Required:    true,
						Type:        api.String,
					},
					"config": {
						Description: assets.L.Get("Configuration of widget"),
						Required:    true,
						Type:        api.Map(api.String, api.Any),
					},
				},
			},
		},
		"widget_change": {
			Func: s.widgetChange,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Change widget settings"),
				Parameters: api.FieldMap{
					"dashboard_name": {
						Description: assets.L.Get("Name of dashboard"),
						Required:    true,
						Type:        api.String,
					},
					"group_idx": {
						Description: assets.L.Get("Index of widget group"),
						Required:    true,
						Type:        api.Int,
					},
					"widget_idx": {
						Description: assets.L.Get("Index of widget"),
						Required:    true,
						Type:        api.Int,
					},
					"new_position": {
						Description: assets.L.Get("New position of widget"),
						Required:    true,
						Type:        api.Int,
					},
					"config": {
						Description: assets.L.Get("Changed configuration of widget"),
						Required:    true,
						Type:        api.Map(api.String, api.Any),
					},
				},
			},
		},
		"widget_remove": {
			Func: s.widgetRemove,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Remove widget from group"),
				Parameters: api.FieldMap{
					"dashboard_name": {
						Description: assets.L.Get("Name of dashboard"),
						Required:    true,
						Type:        api.String,
					},
					"group_idx": {
						Description: assets.L.Get("Index of widget group"),
						Required:    true,
						Type:        api.Int,
					},
					"widget_idx": {
						Description: assets.L.Get("Index of widget"),
						Required:    true,
						Type:        api.Int,
					},
				},
			},
		},

		"widget_type_list": {
			Func: s.widgetTypeList,
			Def: api.SimpleHandler{
				Description: assets.L.Get("List widget types"),
				Response: api.FieldMap{
					"{id}": {
						Description: assets.L.Get("Config definition of widget type"),
						Required:    true,
					},
				},
			},
		},
		"widget_type_add": {
			Func: s.widgetTypeAdd,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Register widget type"),
				Response: api.FieldMap{
					"widget_type": {
						Description: assets.L.Get("Widget type to register"),
						Required:    true,
					},
				},
			},
		},
	}
}

// Events returns all events that can be emitted
func (s *Service) Events() map[string]api.SimpleHandler {
	return map[string]api.SimpleHandler{
		"core.dashboards#changed": {
			Description: assets.L.Get("Emitted a dashboard changed changed"),
		},
	}
}

// SetEventTrigger for service
func (s *Service) SetEventTrigger(trigger core.EventTrigger) {
	s.trigger = trigger
}

// dashboardList returns copy of dashboard list
func (s *Service) dashboardList(param map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	// copy object to prevent external changes
	dashboards := make([]Dashboard, len(s.dashboards))
	copy(dashboards, s.dashboards)
	for i := range s.dashboards {
		dashboards[i].Groups = make([]Group, len(s.dashboards[i].Groups))
		for j := range s.dashboards[i].Groups {
			dashboards[i].Groups[j] = Group{
				Name:    s.dashboards[i].Groups[j].Name,
				Widgets: make([]WidgetWrapper, len(s.dashboards[i].Groups[j].Widgets)),
			}
			copy(dashboards[i].Groups[j].Widgets, s.dashboards[i].Groups[j].Widgets)
		}
	}

	return map[string]interface{}{
		"dashboards": dashboards,
	}, nil
}

// dashboardGet returns copy of dashboard
func (s *Service) dashboardGet(param map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	dashboardName := cast.ToString(param["dashboard_name"])
	for idx, dashboard := range s.dashboards {
		if dashboard.Name != dashboardName {
			continue
		}
		dash := dashboard

		dash.Groups = make([]Group, len(dashboard.Groups))
		for j := range dashboard.Groups {
			dash.Groups[j] = Group{
				Name:    dashboard.Groups[j].Name,
				Widgets: make([]WidgetWrapper, len(dashboard.Groups[j].Widgets)),
			}
			copy(dash.Groups[j].Widgets, dashboard.Groups[j].Widgets)
		}
		return map[string]interface{}{
			"dashboard":     dash,
			"dashboard_idx": idx,
		}, nil
	}

	return nil, errors.New(assets.L.Get("no dashboard with name \"%s\"", dashboardName))
}

// dashboardAdd adds a new dashboard
func (s *Service) dashboardAdd(param map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	dashboardName := cast.ToString(param["dashboard_name"])
	for _, dashboard := range s.dashboards {
		if dashboard.Name == dashboardName {
			return nil, errors.New(assets.L.Get("dashboard with name \"%s\" already exist", dashboardName))
		}
	}

	maxColumns := cast.ToInt(param["max_columns"])
	if maxColumns > 3 || maxColumns < 1 {
		maxColumns = 3
	}

	s.dashboards = append(s.dashboards,
		Dashboard{
			Name:       dashboardName,
			Hide:       cast.ToBool(param["hide"]),
			Icon:       cast.ToString(param["icon"]),
			MaxColumns: maxColumns,
			Groups:     make([]Group, 0),
		})

	go s.save()
	return nil, nil
}

// dashboardChange changes dashboard
func (s *Service) dashboardChange(param map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	dashboardName := cast.ToString(param["dashboard_name"])
	for srcIdx, dashboard := range s.dashboards {
		if dashboard.Name != dashboardName {
			continue
		}

		if value, ok := param["new_name"]; ok {
			newName := cast.ToString(value)
			if newName != dashboard.Name && newName != "" {
				for _, dash := range s.dashboards {
					if dash.Name == newName {
						return nil, errors.New(assets.L.Get("dashboard with name \"%s\" already exist", dashboardName))
					}
				}
				dashboard.Name = newName
			}
		}
		if value, ok := param["hide"]; ok {
			dashboard.Hide = cast.ToBool(value)
		}
		if value, ok := param["icon"]; ok {
			dashboard.Icon = cast.ToString(value)
		}
		if value, ok := param["max_columns"]; ok {
			dashboard.MaxColumns = cast.ToInt(value)
			if dashboard.MaxColumns > 3 || dashboard.MaxColumns < 1 {
				dashboard.MaxColumns = 3
			}
		}
		s.dashboards[srcIdx] = dashboard

		if value, ok := param["new_position"]; ok {
			dstIdx := cast.ToInt(value)
			if dstIdx < 0 {
				dstIdx = 0
			} else if dstIdx >= len(s.dashboards) {
				dstIdx = len(s.dashboards) - 1
			}
			if dstIdx != srcIdx {
				dashboards := append(s.dashboards[:srcIdx], s.dashboards[srcIdx+1:]...)
				s.dashboards = append(dashboards[:dstIdx],
					append([]Dashboard{dashboard}, dashboards[dstIdx:]...)...)
			}
		}
		go s.save()
		return nil, nil
	}

	return nil, errors.New(assets.L.Get("no dashboard with name \"%s\"", dashboardName))
}

// dashboardRemove removes a dashboard
func (s *Service) dashboardRemove(param map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	dashboardName := cast.ToString(param["dashboard_name"])
	for srcIdx, dashboard := range s.dashboards {
		if dashboard.Name != dashboardName {
			continue
		}

		dashboards := make([]Dashboard, 0, len(s.dashboards))
		for idx, dash := range s.dashboards {
			if idx != srcIdx {
				dashboards = append(dashboards, dash)
			}
		}
		s.dashboards = dashboards

		go s.save()
		return nil, nil
	}

	return nil, errors.New(assets.L.Get("no dashboard with name \"%s\"", dashboardName))
}

// groupAdd adds a new group to dashboard
func (s *Service) groupAdd(param map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	dashboardName := cast.ToString(param["dashboard_name"])
	for idx, dashboard := range s.dashboards {
		if dashboard.Name == dashboardName {
			dashboard.Groups = append(dashboard.Groups,
				Group{
					Name:    cast.ToString(param["group_name"]),
					Widgets: make([]WidgetWrapper, 0),
				})
			s.dashboards[idx] = dashboard

			go s.save()
			return nil, nil
		}
	}
	return nil, errors.New(assets.L.Get("no dashboard with name \"%s\"", dashboardName))
}

// groupChange changes a widget group
func (s *Service) groupChange(param map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	dashboardName := cast.ToString(param["dashboard_name"])
	for idx, dashboard := range s.dashboards {
		if dashboard.Name == dashboardName {
			groupIdx := cast.ToInt(param["group_idx"])
			if groupIdx < 0 || groupIdx >= len(dashboard.Groups) {
				return nil, errors.New(assets.L.Get("%d is not a valid group index", groupIdx))
			}

			if value, ok := param["group_name"]; ok {
				dashboard.Groups[groupIdx].Name = cast.ToString(value)
			}

			if value, ok := param["new_position"]; ok {
				dstIdx := cast.ToInt(value)
				if dstIdx < 0 {
					dstIdx = 0
				} else if dstIdx >= len(dashboard.Groups) {
					dstIdx = len(dashboard.Groups) - 1
				}
				if dstIdx != groupIdx {
					group := dashboard.Groups[groupIdx]

					groups := append(dashboard.Groups[:groupIdx],
						dashboard.Groups[groupIdx+1:]...)

					dashboard.Groups = append(groups[:dstIdx],
						append([]Group{group}, groups[dstIdx:]...)...)
				}
			}
			s.dashboards[idx] = dashboard

			go s.save()
			return nil, nil
		}
	}
	return nil, errors.New(assets.L.Get("no dashboard with name \"%s\"", dashboardName))
}

// groupRemove remove a widget group
func (s *Service) groupRemove(param map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	dashboardName := cast.ToString(param["dashboard_name"])
	for idx, dashboard := range s.dashboards {
		if dashboard.Name == dashboardName {
			groupIdx := cast.ToInt(param["group_idx"])
			if groupIdx < 0 || groupIdx >= len(dashboard.Groups) {
				return nil, errors.New(assets.L.Get("%d is not a valid group index", groupIdx))
			}

			dashboard.Groups = append(dashboard.Groups[:groupIdx],
				dashboard.Groups[groupIdx+1:]...)

			s.dashboards[idx] = dashboard

			go s.save()
			return nil, nil
		}
	}
	return nil, errors.New(assets.L.Get("no dashboard with name \"%s\"", dashboardName))
}

// widgetAdd adds a new widget
func (s *Service) widgetAdd(param map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	// get widget instance
	widgetType := cast.ToString(param["widget_type"])
	widget, ok := s.widgetTypes[widgetType]
	if !ok {
		return nil, errors.New(assets.L.Get("unknown widget type %s", widgetType))
	}

	// validate parameters
	fields := widget.ConfigDef()
	fields["{}"] = api.Field{}
	config, err := fields.Validate(cast.ToStringMap(param["config"]))
	if err != nil {
		return nil, errors.New(assets.L.Get("invalid widget configuration: %w", err))
	}

	confWidget, ok := widget.(widgets.CheckConfigWidget)
	if ok {
		err = confWidget.CheckConfig(config)
		if err != nil {
			return nil, errors.New(assets.L.Get("invalid widget configuration: %w", err))
		}
	}
	config["type"] = widget.TypeID()

	// add widget
	dashboardName := cast.ToString(param["dashboard_name"])
	for idx, dashboard := range s.dashboards {
		if dashboard.Name == dashboardName {
			groupIdx := cast.ToInt(param["group_idx"])
			if groupIdx < 0 || groupIdx >= len(dashboard.Groups) {
				return nil, errors.New(assets.L.Get("%d is not a valid group index", groupIdx))
			}

			dashboard.Groups[groupIdx].Widgets =
				append(dashboard.Groups[groupIdx].Widgets, WidgetWrapper{
					Config: config,
					Widget: widget,
					ID:     ID(),
				})

			s.dashboards[idx] = dashboard

			go s.save()
			return nil, nil
		}
	}
	return nil, errors.New(assets.L.Get("no dashboard with name \"%s\"", dashboardName))
}

// widgetChange changes a widget
func (s *Service) widgetChange(param map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	dashboardName := cast.ToString(param["dashboard_name"])
	for idx, dashboard := range s.dashboards {
		if dashboard.Name == dashboardName {
			groupIdx := cast.ToInt(param["group_idx"])
			if groupIdx < 0 || groupIdx >= len(dashboard.Groups) {
				return nil, errors.New(assets.L.Get("%d is not a valid group index", groupIdx))
			}

			widgetIdx := cast.ToInt(param["widget_idx"])
			if widgetIdx < 0 || widgetIdx >= len(dashboard.Groups[groupIdx].Widgets) {
				return nil, errors.New(assets.L.Get("%d is not a valid widget index", widgetIdx))
			}

			if value, ok := param["config"]; ok {
				wrapper := dashboard.Groups[groupIdx].Widgets[widgetIdx]

				if wrapper.Widget == nil {
					return nil, errors.New(assets.L.Get("unknown widgets cannot be changed"))
				}

				changedConfig := cast.ToStringMap(value)
				config := wrapper.Config
				for key := range wrapper.Widget.ConfigDef() {
					value, ok := changedConfig[key]
					if ok {
						config[key] = value
					}
				}

				// validate parameters
				fields := wrapper.Widget.ConfigDef()
				fields["{}"] = api.Field{}
				config, err := fields.Validate(config)
				if err != nil {
					return nil, errors.New(assets.L.Get("invalid widget configuration: %w", err))
				}

				confWidget, ok := wrapper.Widget.(widgets.CheckConfigWidget)
				if ok {
					err = confWidget.CheckConfig(config)
					if err != nil {
						return nil, errors.New(assets.L.Get("invalid widget configuration: %w", err))
					}
				}
				wrapper.Config = config

				dashboard.Groups[groupIdx].Widgets[widgetIdx] = wrapper
			}

			if value, ok := param["new_position"]; ok {
				dstIdx := cast.ToInt(value)
				if dstIdx < 0 {
					dstIdx = 0
				} else if dstIdx >= len(dashboard.Groups[groupIdx].Widgets) {
					dstIdx = len(dashboard.Groups[groupIdx].Widgets) - 1
				}
				if dstIdx != widgetIdx {
					wrapper := dashboard.Groups[groupIdx].Widgets[widgetIdx]

					widgetList := append(dashboard.Groups[groupIdx].Widgets[:widgetIdx],
						dashboard.Groups[groupIdx].Widgets[widgetIdx+1:]...)

					dashboard.Groups[groupIdx].Widgets = append(widgetList[:dstIdx],
						append([]WidgetWrapper{wrapper}, widgetList[dstIdx:]...)...)
				}
			}

			s.dashboards[idx] = dashboard

			go s.save()
			return nil, nil
		}
	}
	return nil, errors.New(assets.L.Get("no dashboard with name \"%s\"", dashboardName))
}

// widgetRemove removes a widget
func (s *Service) widgetRemove(param map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	dashboardName := cast.ToString(param["dashboard_name"])
	for idx, dashboard := range s.dashboards {
		if dashboard.Name == dashboardName {
			groupIdx := cast.ToInt(param["group_idx"])
			if groupIdx < 0 || groupIdx >= len(dashboard.Groups) {
				return nil, errors.New(assets.L.Get("%d is not a valid group index", groupIdx))
			}

			widgetIdx := cast.ToInt(param["widget_idx"])
			if widgetIdx < 0 || widgetIdx >= len(dashboard.Groups[groupIdx].Widgets) {
				return nil, errors.New(assets.L.Get("%d is not a valid widget index", widgetIdx))
			}

			dashboard.Groups[groupIdx].Widgets = append(
				dashboard.Groups[groupIdx].Widgets[:widgetIdx],
				dashboard.Groups[groupIdx].Widgets[widgetIdx+1:]...)

			s.dashboards[idx] = dashboard

			go s.save()
			return nil, nil
		}
	}
	return nil, errors.New(assets.L.Get("no dashboard with name \"%s\"", dashboardName))
}

// widgetTypeList returns list of known widget types
func (s *Service) widgetTypeList(param map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	data := make(map[string]interface{}, len(s.widgetTypes))
	for id, widget := range s.widgetTypes {
		data[id] = widget.ConfigDef()
	}
	return data, nil
}

// widgetTypeAdd register new widget type
func (s *Service) widgetTypeAdd(param map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	widgetType, ok := param["widget_type"].(widgets.Widget)
	if !ok {
		return nil, errors.New(assets.L.Get("invalid widget type instance given"))
	}

	_, ok = s.widgetTypes[widgetType.TypeID()]
	if ok {
		return nil, errors.New(assets.L.Get("widget type with ID %s already exist", widgetType.TypeID()))
	}
	s.widgetTypes[widgetType.TypeID()] = widgetType
	return nil, nil
}
