// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dashboard

import (
	"fmt"
	"net/http"

	"gitlab.com/go4matic/go4matic/pkg/core"
)

// Web handler for dashboard
type Web struct {
	Bus core.Bus
}

// DashboardEndpoints returns dashboard path entries
func (w *Web) DashboardEndpoints() []core.PathEntry {
	return []core.PathEntry{{
		Handler: func(ctx core.HTTPContext) {
			dashIndex, ok := ctx.Get("dash_index")
			if ok {
				ctx.Redirect(302, "dash/"+dashIndex.(string))
			} else {
				ctx.Redirect(302, "admin")
			}
		},
	}, {
		Path:       ":name",
		Middleware: []func(ctx core.HTTPContext){w.dashboardCheck},
		SubEntries: []core.PathEntry{{
			Handler: w.dashboardWeb,
		}},
	}}
}

// dashboardCheck check if dash board exist
func (w *Web) dashboardCheck(ctx core.HTTPContext) {
	if _, ok := ctx.Get("dashboard"); !ok {
		ctx.Abort()
		ctx.Status(http.StatusNotFound)
		return
	}
}

// dashboardWeb renders dashboard
func (w *Web) dashboardWeb(ctx core.HTTPContext) {
	ctx.Set("tpl", "dashboard")

	dash := ctx.MustGet("dashboard").(*Dashboard)
	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["groups"] = dash.Groups
	vars["max_columns"] = dash.MaxColumns
	ctx.Set("title", fmt.Sprintf("%s - Go4Matic", dash.Name))
}
