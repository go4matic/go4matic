// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package widgets

import (
	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// ValueNumberWidget defines widget to show numeric values
type ValueNumberWidget struct {
	Bus core.Bus
}

// TypeID identifies widget type
func (w *ValueNumberWidget) TypeID() string {
	return "value_number"
}

// ConfigDef defines configuration of widget
func (w *ValueNumberWidget) ConfigDef() api.FieldMap {
	return api.FieldMap{
		"title": {
			Description: assets.L.Get("Title for Value"),
			Type:        api.String,
		},
		"item_value": {
			Description: assets.L.Get("Value to display"),
			Type:        core.SensorItemValueType,
			Required:    true,
			PossibleValues: func() map[interface{}]string {
				return core.SensorItemValues(w.Bus, api.Numbers...)
			},
		},
		"correction_factor": {
			Description:  assets.L.Get("Correction factor for value"),
			Type:         api.Float64,
			DefaultValue: 1,
		},
		"unit": {
			Description: assets.L.Get("Unit of value"),
			Type:        api.String,
		},
		"decimal_digits": {
			Description:  assets.L.Get("Amount of decimal digits"),
			Type:         api.Uint,
			DefaultValue: 1,
		},
	}
}
