// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package widgets

import (
	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
)

// IFrameWidget defines IFrame widget
type IFrameWidget struct{}

// TypeID identifies widget type
func (w *IFrameWidget) TypeID() string {
	return "iframe"
}

// ConfigDef defines configuration of widget
func (w *IFrameWidget) ConfigDef() api.FieldMap {
	return api.FieldMap{
		"title": {
			Description: assets.L.Get("Title for widget"),
			Type:        api.String,
		},
		"url": {
			Description: assets.L.Get("URL to embed"),
			Type:        api.String,
			Required:    true,
		},
		"ratio": {
			Description:  assets.L.Get("Aspect ratios"),
			Type:         api.String,
			DefaultValue: "1by1",
			PossibleValues: func() map[interface{}]string {
				return map[interface{}]string{
					"21by9": "21:9",
					"16by9": "16:9",
					"4by3":  "4:3",
					"1by1":  "1:1",
				}
			},
		},
	}
}
