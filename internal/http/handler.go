// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package http

import (
	"context"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// Handler for HTTP request
type Handler struct {
	apiHandler []core.PathEntry

	baseRouter gin.IRouter
	apiGroup   gin.IRouter
	webGroup   gin.IRouter

	server *http.Server
}

// CreateHandler for HTTP request
func CreateHandler(addr string) *Handler {
	// create router instance
	router := gin.New()

	// check for debug mode
	if strings.ToLower(os.Getenv("DEBUG")) != "true" {
		gin.SetMode(gin.ReleaseMode)

	} else {
		router.Use(gin.Logger())
	}

	router.Use(gin.Recovery())

	// start web server
	srv := &http.Server{
		Addr:    addr,
		Handler: router,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			zap.S().Fatalf("http listen failed: %s", err)
		}
	}()

	handler := &Handler{
		apiHandler: make([]core.PathEntry, 0),

		baseRouter: router,
		apiGroup:   router.Group("/api"),
		webGroup:   router.Group("/web"),

		server: srv,
	}

	registerAPISpec(handler)

	return handler
}

// AddAPIEndpoints to HTTP handler
func (h *Handler) AddAPIEndpoints(entries []core.PathEntry) {
	h.apiHandler = append(h.apiHandler, entries...)
	addEntries(h.apiGroup, entries)
}

// AddWebEndpoints to HTTP handler
func (h *Handler) AddWebEndpoints(entries []core.PathEntry) {
	addEntries(h.webGroup, entries)
}

// AddBaseEndpoints to HTTP handler
func (h *Handler) AddBaseEndpoints(entries []core.PathEntry) {
	addEntries(h.baseRouter, entries)
}

// Shutdown http interface
func (h *Handler) Shutdown() {
	// wait up to 2 seconds for shutdown
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
	defer cancel()

	err := h.server.Shutdown(ctx)
	if err != nil {
		zap.L().Error(assets.L.Get("Failed to stop API"))
	} else {
		zap.L().Info(assets.L.Get("API stopped"))
	}
}

// WrapContext wraps gin.Context into core.HTTPContext
func WrapContext(ctx *gin.Context) core.HTTPContext {
	return &contextWrapper{Context: ctx}
}

// contextWrapper wraps a gin context to a HTTPContext
type contextWrapper struct {
	*gin.Context
	body map[string]interface{}
}

// Request from HTTP client
func (c *contextWrapper) Request() *http.Request {
	return c.Context.Request
}

// Writer returns HTTP response object
func (c *contextWrapper) Writer() http.ResponseWriter {
	return c.Context.Writer
}

// BodyValue returns value from body.
func (c *contextWrapper) BodyValue(key string) interface{} {
	if c.body == nil {
		c.body = make(map[string]interface{})
		err := c.ShouldBind(&c.body)
		if err != nil {
			c.Context.AbortWithStatusJSON(http.StatusBadRequest, map[string]interface{}{
				"error": err.Error(),
			})
			return nil
		}
	}
	return c.body[key]
}

// AbortWithStatusJSON calls `Abort()` and then `JSON` internally.
func (c *contextWrapper) AbortWithStatusJSON(code int, jsonObj interface{}) {
	c.Context.Abort()
	c.Context.IndentedJSON(code, jsonObj)
}

// JSON redirects to IndentedJSON fore better JSON output
func (c *contextWrapper) JSON(code int, obj interface{}) {
	c.Context.IndentedJSON(code, obj)
}

// BusResponse writes a bus response to http context
func (c *contextWrapper) BusResponse(response core.Response) {
	if response.Error != nil {
		c.Context.IndentedJSON(http.StatusBadRequest, map[string]interface{}{
			"error": response.Error.Error(),
		})
	} else {
		c.Context.IndentedJSON(http.StatusOK, response.Data)
	}
}

// wrapCtxFunc wraps a HTTPContext function to a gin.HandlerFunc
func wrapCtxFunc(f func(core.HTTPContext)) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		f(WrapContext(ctx))
	}
}

// addEntries to router
func addEntries(group gin.IRouter, entries []core.PathEntry) {
	for _, entry := range entries {
		if entry.Method == "" {
			entry.Method = "GET"
		}

		g := group.Group(entry.Path)

		if entry.Middleware != nil {
			for _, m := range entry.Middleware {
				g.Use(wrapCtxFunc(m))
			}
		}

		if entry.FileSystem != nil {
			if entry.Handler != nil || len(entry.SubEntries) > 0 {
				zap.S().Fatal("FileSystem is not compatible with Handler & SubEntries: %s", g.BasePath())
			}
			g.StaticFS("/", entry.FileSystem)
		}

		if entry.Handler != nil {
			g.Handle(entry.Method, "", wrapCtxFunc(entry.Handler))
		}

		if len(entry.SubEntries) > 0 {
			addEntries(g, entry.SubEntries)
		}
	}
}
