// Copyright 2020 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package http

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/pkg/core"
)

// runID identifies the actual application run and should be different on every restart
var runID = rand.New(rand.NewSource(time.Now().UnixNano())).Uint64()

// WSRequest send through web socket
type WSRequest struct {
	// Type of request (event, widget_action, ...)
	Type string `json:"type"`

	// Id of action (event id, widget id, ...
	ID string `json:"id"`

	// Data of request
	Data map[string]interface{} `json:"data"`

	// Reference to another request (request_response -> original request)
	Reference *WSRequest `json:"reference"`
}

// WsClient communicates with dashboard in the browser
type WsClient struct {
	conn net.Conn

	bus              core.Bus
	eventBuffer      chan core.Event
	registeredEvents map[string]bool
	mutex            sync.Mutex

	reader *wsutil.Reader
	writer *wsutil.Writer

	encoder *json.Encoder
	decoder *json.Decoder
}

// UpgradeHttp request to websocket and handle communication
func UpgradeHttp(r *http.Request, w http.ResponseWriter, bus core.Bus) {
	// upgrade to websocket
	conn, _, _, err := ws.UpgradeHTTP(r, w)
	if err != nil {
		zap.L().Error(err.Error())
		return
	}
	defer conn.Close()

	// handler websocket
	NewWsClient(conn, bus).Run()
}

// NewWsClient creates client for websocket communication
func NewWsClient(conn net.Conn, bus core.Bus) *WsClient {
	client := WsClient{
		conn: conn,

		bus:              bus,
		eventBuffer:      make(chan core.Event, 5),
		registeredEvents: make(map[string]bool),

		reader: wsutil.NewReader(conn, ws.StateServerSide),
		writer: wsutil.NewWriter(conn, ws.StateServerSide, ws.OpText),
	}

	client.decoder = json.NewDecoder(client.reader)
	client.encoder = json.NewEncoder(client.writer)

	// send initial packet with run ID
	err := client.sendRequest(&WSRequest{
		Type: "init",
		ID:   strconv.FormatUint(runID, 10),
	})
	if err != nil {
		zap.L().Error(err.Error())
	}

	return &client
}

// Run communication handling
func (c *WsClient) Run() {
	go c.wsReader()
	c.eventHandler()
}

// wsReader reads received messages and handles requests
func (c *WsClient) wsReader() {
	for {
		hdr, err := c.reader.NextFrame()
		if err != nil {
			zap.L().Error(err.Error())
			break
		}
		if hdr.OpCode == ws.OpClose {
			break
		}
		var req WSRequest
		if err := c.decoder.Decode(&req); err != nil {
			zap.L().Error(err.Error())
			continue
		}
		if err := c.handleRequest(&req); err != nil {
			zap.L().Error(err.Error())
			_ = c.sendRequest(&WSRequest{
				Type: "error",
				Data: map[string]interface{}{
					"msg": err.Error(),
				},
			})
			continue
		}
	}
	c.eventBuffer <- core.Event{}
}

// handleRequest received from socket
func (c *WsClient) handleRequest(request *WSRequest) error {
	switch request.Type {
	case "request":
		response := c.bus.HandleRequestWait(core.Request{
			ID:         request.ID,
			Parameters: request.Data,
		}, time.Second*2)

		if response.Error != nil {
			return response.Error
		}

		return c.sendRequest(&WSRequest{
			Type:      "request_response",
			ID:        request.ID,
			Data:      response.Data,
			Reference: request,
		})

	case "register_event":
		err := c.bus.RegisterEventListener(request.ID, c.eventBuffer)
		if err != nil {
			return err
		}
		c.registeredEvents[request.ID] = true

		return nil
	default:
		return fmt.Errorf("unknown request type %s", request.Type)
	}
}

// eventHandler forwards received bus events to web socket
func (c *WsClient) eventHandler() {
	defer func() {
		for id := range c.registeredEvents {
			c.bus.UnregisterEventListener(id, c.eventBuffer)
		}
	}()

	for event := range c.eventBuffer {
		if event.ID == "" {
			return
		}

		request := WSRequest{
			Type: "event",
			ID:   event.ID,
			Data: event.Data,
		}
		if err := c.sendRequest(&request); err != nil {
			if _, ok := err.(*net.OpError); ok {
				continue
			}
			zap.L().Error(err.Error())
			continue
		}
	}
}

// sendRequest to web socket
func (c *WsClient) sendRequest(request *WSRequest) error {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	if err := c.encoder.Encode(request); err != nil {
		return err
	}
	if err := c.writer.Flush(); err != nil {
		return err
	}
	return nil
}
