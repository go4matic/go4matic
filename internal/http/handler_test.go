// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package http

import (
	"context"
	"errors"
	"fmt"
	"net"
	"net/http"
	"strings"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	testifyHttp "github.com/stretchr/testify/http"

	"gitlab.com/go4matic/go4matic/pkg/core"
)

func TestHandler_CreateShutdown(t *testing.T) {
	ass := assert.New(t)

	// get a free port
	ln, err := net.Listen("tcp", ":0")
	ass.NoError(err)
	ass.NoError(ln.Close())
	port := ln.Addr().(*net.TCPAddr).Port

	// start first handler
	handler1 := CreateHandler(fmt.Sprintf(":%d", port))
	time.Sleep(time.Millisecond)

	ass.Equal(gin.ReleaseMode, gin.Mode())

	response, err := http.Get(fmt.Sprintf("http://127.0.0.1:%d/", port))
	ass.NoError(err)
	ass.Equal(404, response.StatusCode)

	// stop first handler
	handler1.Shutdown()
	ass.NoError(handler1.server.Shutdown(context.Background()))
}

func dummyHandler(_ core.HTTPContext) {}

type RoutesInfo gin.RoutesInfo

func (i RoutesInfo) getRoute(path string) gin.RouteInfo {
	for _, info := range i {
		if info.Path == path {
			return info
		}
	}
	return gin.RouteInfo{}
}
func (i RoutesInfo) getRoutes(path string) RoutesInfo {
	infos := make(RoutesInfo, 0)
	for _, info := range i {
		if info.Path == path {
			infos = append(infos, info)
		}
	}
	return infos
}

func Test_addEntries(t *testing.T) {
	ass := assert.New(t)

	router := gin.New()
	addEntries(router, []core.PathEntry{
		{
			Path:    "/aaa",
			Method:  "PUT",
			Handler: dummyHandler,
		},
		{
			Path:    "/bbb",
			Handler: dummyHandler,
		},
		{
			Path:   "/ccc",
			Method: "PUT",
			Middleware: []func(ctx core.HTTPContext){
				dummyHandler, dummyHandler,
			},
			Handler: dummyHandler,
		},
		{
			Path:       "/fs",
			FileSystem: http.Dir("."),
		},
		{
			Path: "/sub1",
			Middleware: []func(ctx core.HTTPContext){
				dummyHandler, dummyHandler,
			},
			SubEntries: []core.PathEntry{
				{
					Path:    "/aaa",
					Handler: dummyHandler,
				},
				{
					Path:    "/bbb",
					Handler: dummyHandler,
				},
			},
		},
		{
			Path:    "/sub2",
			Handler: dummyHandler,
			SubEntries: []core.PathEntry{
				{
					Path:    "/aaa",
					Handler: dummyHandler,
				},
				{
					Path:    "/bbb",
					Handler: dummyHandler,
				},
			},
		},
	})

	routes := RoutesInfo(router.Routes())
	ass.Len(routes, 10)
	ass.Equal("PUT", routes.getRoute("/aaa").Method)
	ass.Equal("GET", routes.getRoute("/bbb").Method)
	ass.Equal("PUT", routes.getRoute("/ccc").Method)
	ass.Equal("GET", routes.getRoute("/sub1/aaa").Method)
	ass.Equal("GET", routes.getRoute("/sub1/bbb").Method)
	ass.Equal("GET", routes.getRoute("/sub2/aaa").Method)
	ass.Equal("GET", routes.getRoute("/sub2/aaa").Method)
	ass.Equal("GET", routes.getRoute("/sub2").Method)

	fsRoutes := routes.getRoutes("/fs/*filepath")
	ass.Equal("GET", fsRoutes[0].Method)
	ass.Equal("HEAD", fsRoutes[1].Method)
}

func TestHandler_AddAPIEndpoints(t *testing.T) {
	router := gin.New()
	handler := Handler{
		apiHandler: make([]core.PathEntry, 0),
		apiGroup:   router,
	}

	entries := []core.PathEntry{
		{
			Path:    "/aaa",
			Method:  "GET",
			Handler: dummyHandler,
		},
		{
			Path:    "/bbb",
			Handler: dummyHandler,
		},
	}

	handler.AddAPIEndpoints(entries)

	ass := assert.New(t)
	ass.Equal(entries[0].Path, handler.apiHandler[0].Path)
	ass.Equal(entries[1].Path, handler.apiHandler[1].Path)
	ass.Len(router.Routes(), 2)
}

func TestHandler_AddWebEndpoints(t *testing.T) {
	router := gin.New()
	handler := Handler{
		webGroup: router,
	}

	entries := []core.PathEntry{
		{
			Path:    "/aaa",
			Handler: dummyHandler,
		},
		{
			Path:    "/bbb",
			Handler: dummyHandler,
		},
	}

	handler.AddWebEndpoints(entries)

	ass := assert.New(t)
	ass.Len(router.Routes(), 2)
}

func TestHandler_AddBaseEndpoints(t *testing.T) {
	router := gin.New()
	handler := Handler{
		baseRouter: router,
	}

	entries := []core.PathEntry{
		{
			Path:    "/aaa",
			Handler: dummyHandler,
		},
		{
			Path:    "/bbb",
			Handler: dummyHandler,
		},
	}

	handler.AddBaseEndpoints(entries)

	ass := assert.New(t)
	ass.Len(router.Routes(), 2)
}

func Test_contextWrapper(t *testing.T) {
	ass := assert.New(t)

	gin.SetMode(gin.TestMode)
	writer := testifyHttp.TestResponseWriter{}
	ctx, _ := gin.CreateTestContext(&writer)
	ctxWrapper := contextWrapper{Context: ctx}

	ass.Equal(ctx.Request, ctxWrapper.Request())
	ass.Equal(ctx.Writer, ctxWrapper.Writer())

	// JSON
	ctxWrapper.JSON(200, map[string]interface{}{
		"test": 42,
	})
	ass.Equal("{\n    \"test\": 42\n}", writer.Output)
	writer.Output = ""

	// AbortWithStatusJSON
	ass.False(ctx.IsAborted())
	ctxWrapper.AbortWithStatusJSON(400, map[string]interface{}{
		"test": 42,
	})
	ass.Equal("{\n    \"test\": 42\n}", writer.Output)
	ass.True(ctx.IsAborted())

	// BusResponse
	writer.Output = ""
	ctxWrapper.BusResponse(core.Response{
		Data: map[string]interface{}{
			"aaa": "test",
		},
	})
	ass.JSONEq(`{"aaa": "test"}`, writer.Output)

	writer.Output = ""
	ctxWrapper.BusResponse(core.Response{
		Error: errors.New("test"),
	})
	ass.JSONEq(`{"error": "test"}`, writer.Output)

	// BodyValue
	request, _ := http.NewRequest("POST",
		"http://127.0.0.1:10001/",
		strings.NewReader(`{"aaa": "test"}`))
	request.Header.Set("Content-Type", gin.MIMEJSON)
	ctx.Request = request
	ass.Equal("test", ctxWrapper.BodyValue("aaa"))

	writer.Output = ""
	ctx, _ = gin.CreateTestContext(&writer)
	ctxWrapper = contextWrapper{Context: ctx}
	request, _ = http.NewRequest("POST",
		"http://127.0.0.1:10001/",
		strings.NewReader(`test`))
	request.Header.Set("Content-Type", gin.MIMEJSON)
	ctx.Request = request
	ass.Equal(nil, ctxWrapper.BodyValue("aaa"))
	ass.Equal(400, writer.StatusCode)
	ass.Equal("{\"error\":\"invalid character 'e' in literal true (expecting 'r')\"}", strings.TrimSpace(writer.Output))
}

func Test_wrapCtx(t *testing.T) {
	writer := testifyHttp.TestResponseWriter{}
	ctx, _ := gin.CreateTestContext(&writer)

	wrapCtxFunc(func(ctx core.HTTPContext) {
		ctx.JSON(200, map[string]interface{}{
			"test": 42,
		})
	})(ctx)

	assert.Equal(t, "{\n    \"test\": 42\n}", writer.Output)
}
