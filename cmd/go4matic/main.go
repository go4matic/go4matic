// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/yaml.v2"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/internal/http"
	"gitlab.com/go4matic/go4matic/internal/lua"
	"gitlab.com/go4matic/go4matic/internal/plugin"
	"gitlab.com/go4matic/go4matic/internal/service"
	"gitlab.com/go4matic/go4matic/internal/version"
	"gitlab.com/go4matic/go4matic/internal/web"
	"gitlab.com/go4matic/go4matic/internal/web/admin"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// CLI parameter
var port = flag.Int("port", 8080, "Port of API and web interface")
var configFile = flag.String("config", "config.yml", "Path to configuration file")
var logConfigFile = flag.String("log_config", "log_config.yml", "Path to log configuration file")

func createLogger() *zap.Logger {
	// define default config
	config := zap.NewProductionConfig()
	config.Level = zap.NewAtomicLevelAt(zap.DebugLevel)
	config.OutputPaths = []string{"stdout"}
	config.Encoding = "console"
	config.DisableCaller = true
	config.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	config.EncoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder

	// try to load log config
	f, err := os.Open(*logConfigFile)
	if err == nil {
		err = yaml.NewDecoder(f).Decode(&config)
		if err != nil {
			fmt.Println(err)
			fmt.Println("Ignoring log config")
		}
	}

	// create logger
	logger, err := config.Build()
	if err != nil {
		panic(err)
	}

	return logger
}

func checkError(err error) {
	if err != nil {
		zap.L().Fatal(err.Error())
	}
}

func main() {
	// parse CLI
	flag.Parse()

	// wait for valid time
	for time.Now().Year() == 1970 {
		time.Sleep(time.Second)
	}

	logger := createLogger()
	zap.ReplaceGlobals(logger)
	defer fmt.Print(logger.Sync())

	zap.S().Infof("Starting Go4Matic %s", version.Version)
	bus := core.CreateBus()
	httpHandler := http.CreateHandler(fmt.Sprintf(":%d", *port))

	// load core services
	checkError(bus.RegisterService(
		service.NewConfigService(*configFile)))

	itemService := service.NewItemService()
	checkError(bus.RegisterService(itemService))
	httpHandler.AddAPIEndpoints(itemService.APIEndpoints(bus))

	scriptService := new(service.ScriptService)
	checkError(bus.RegisterService(scriptService))
	httpHandler.AddAPIEndpoints(scriptService.APIEndpoints(bus))

	timerService := new(service.TimerService)
	checkError(bus.RegisterService(timerService))
	httpHandler.AddAPIEndpoints(timerService.APIEndpoints(bus))

	// initialize web interface and dashboard
	checkError(web.InitializeWeb(bus, httpHandler))

	// add core scripts
	checkError(scriptService.AddModule(lua.CreateCoreModule(bus)))
	checkError(scriptService.AddModule(lua.CreateItemsModule(bus)))
	checkError(scriptService.AddModule(new(lua.TimeModule)))

	// load plugins
	pluginLoader := new(plugin.Loader)
	pluginLoader.Load(bus)
	checkError(bus.RegisterService(pluginLoader))
	httpHandler.AddAPIEndpoints(pluginLoader.APIEndpoints())
	httpHandler.AddWebEndpoints(pluginLoader.WebEndpoints())

	checkError(bus.Start())

	// make bus accessible via websocket
	httpHandler.AddBaseEndpoints([]core.PathEntry{{
		Path: "/bus",
		Handler: func(ctx core.HTTPContext) {
			http.UpgradeHttp(ctx.Request(), ctx.Writer(), bus)
		},
	}})
	zap.L().Info(assets.L.Get("Go4Matic core initialized"))

	// handle boot counter reset
	admin.UBootResetCounter()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c

	zap.L().Info(assets.L.Get("Go4Matic core shutdown started ..."))
	pluginLoader.Shutdown()
	httpHandler.Shutdown()
	bus.Shutdown()

	zap.L().Info(assets.L.Get("Go4Matic core shutdown finished"))
	os.Exit(0)
}
