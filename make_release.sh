#!/bin/sh
set -e

curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: ${BOT_TOKEN}" \
     --data "{ \"name\": \"${CI_COMMIT_REF_NAME}\", \"tag_name\": \"${CI_COMMIT_REF_NAME}\", \"description\": \"no description\" }" \
     --request POST ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases

cd out/
for file in *; do
    curl --request POST \
        --header "PRIVATE-TOKEN: ${BOT_TOKEN}" \
        --data name="$file" \
        --data url="https://${MC_HOST}/${MC_BUCKET}/${CI_COMMIT_REF_NAME}/$file" \
        ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases/${CI_COMMIT_REF_NAME}/assets/links
done


